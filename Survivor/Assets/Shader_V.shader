﻿Shader "Custom/Shader_V"
{
  Properties {
   
	_MainTex ("Texture", 2D) = "white" {}
	_Color ("Cor", Color) = (0,0,0,0) 
  }
  SubShader {
    Tags { "RenderType" = "Opaque" }

    ColorMask RGB

    CGPROGRAM
    #pragma surface surf Lambert

    sampler2D _MainTex;
	float4 _Color;

    struct Input {
        float4 color : COLOR;
        float2 uv_MainTex;
    };

    void surf (Input IN, inout SurfaceOutput o) {
        fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
        o.Albedo = c.rgb;
    }
    ENDCG
  }

  Fallback "Diffuse"
}
