﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine;

public class NewSobrevivente : MonoBehaviour
{
    public Transform target;
    [Space]

    public int vida;
    public int dano;

    public Text dist;
    public bool resgatado;
    public bool esperando;
    public bool concluido;
    [Space]

    public float distancia;

    NavMeshAgent agent;
    public GameObject Jogador;
    Animator anim;

    public void Resgatado(bool rego)
    {
        resgatado = rego;
    }

    // Start is called before the first frame update
    void Start()
    {
        Jogador = CG_Vitto.Instance.gameObject;
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();

        vida = 100;
        target = Jogador.transform;

        dist.text = "";
        anim.SetBool("Andando", false);
    }

    // Update is called once per frame
    void Update()
    {
       // if (distancia < 6 && Input.GetKeyDown(Mover.Instance.Interagir) && !resgatado) resgatado = true;

        if (resgatado)
        {

            distancia = Vector3.Distance(transform.position, new Vector3(Jogador.transform.position.x, Jogador.transform.position.y - 1, Jogador.transform.position.z));

            if (concluido) dist.enabled = false;
            else
            {
                dist.enabled = true;
                if (distancia < 20 && distancia > 7) dist.transform.localScale = Vector3.Lerp(dist.transform.localScale, new Vector3(0.5f, 0.5f, 0.5f), Time.deltaTime * 2.5f);
                if (distancia > 20 && distancia < 30) dist.transform.localScale = Vector3.Lerp(dist.transform.localScale, new Vector3(1, 1, 1), Time.deltaTime * 2.5f);
                if (distancia > 30 && distancia < 40) dist.transform.localScale = Vector3.Lerp(dist.transform.localScale, new Vector3(1.5f, 1.5f, 1.5f), Time.deltaTime * 2.5f);
            }

            if (distancia >= 7)
            {
                agent.speed = 10;
                dist.text = "" + distancia.ToString("f2") + "m";

            }
            if (distancia < 7 && distancia > agent.stoppingDistance)
            {
                agent.speed = 6.5f;
                dist.text = "";

            }

            if (distancia > agent.stoppingDistance)
            {
                anim.SetBool("Andando", true);
                agent.acceleration = 10;

            }
            if (distancia <= agent.stoppingDistance)
            {
                anim.SetBool("Andando", false);
                agent.acceleration = 70;
                agent.speed = 2;
            }

            anim.SetFloat("Speed", agent.speed);

            Esperar();
            OlharPlayer();
        }
    }

    void Velocidade()
    {
      /*  if (Mover.Instance.FrenteTras != 0 || Mover.Instance.DireitaEsquerda != 0)
        {
            if (Mover.Instance.speed == Mover.Instance.runningSpeed) agent.speed = 12;
            if (Mover.Instance.speed == Mover.Instance.walkingSpeed) agent.speed = 6;
            agent.acceleration = 10;

        }*/

       /* if (Mover.Instance.FrenteTras == 0 && Mover.Instance.DireitaEsquerda == 0)
        {
            agent.stoppingDistance = distancia;
        }*/

    }

    void Esperar()
    {
        if (Input.GetKeyDown(Mover.Instance.Espera)) esperando = !esperando;

        if (esperando)
        {
            agent.isStopped = true;
            if (distancia > agent.stoppingDistance) anim.SetBool("Andando", false);
        }


        if (!esperando)
        {
            agent.isStopped = false;

            SeguePlayer();
            Velocidade();
        }

    }

    void SeguePlayer()
    {
        agent.destination = target.transform.position;
    }

    void OlharPlayer()
    {
        if (distancia > agent.stoppingDistance)
        {
            var JogadorRotation = Quaternion.LookRotation(new Vector3(Jogador.transform.position.x, transform.position.y, Jogador.transform.position.z) - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, JogadorRotation, 5 * Time.deltaTime);
        }
    }


}
