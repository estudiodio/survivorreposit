﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.AI;
using UnityEngine;

public class IA_Sobrevivente : MonoBehaviour
{

    /*public int vida;
    public int dano;
    [Space]

    public Transform target;
    public Text dis;
    public float distancia;
    float distanciaZumbi;
    public LayerMask layer_Zumbi;
    [Space]

    public GameObject CuboDeDano;
    public GameObject inimigoTemporario;
    public GameObject impactBlood;
    public GameObject pistola;
    public GameObject bastao;
    public ParticleSystem muzzleParticles;
    public Light muzzleflashLight;

    [Space]

    public bool ativado;
    public bool resgatado;
    public bool sobAtaque;
    public bool esperando;
    public bool armaDeFogo;
    public bool temArma;
    bool batendo;
    [Space]

    public float rotationSpeed;
    [Space]
    public float fireRate;
    public float currentRate;
    int T_Raio;

    NavMeshAgent agent;
    GameObject Jogador;
    Animator anim;

    // Start is called before the first frame update

    public void Resgatado(bool rego)
    {
        resgatado = rego;
    }
    
    void Start()
    {
        Jogador = GameObject.FindWithTag("Player");
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();

        vida = 100;
        
        CuboDeDano.SetActive(false);

        pistola.SetActive(false);
        bastao.SetActive(false);

        if (armaDeFogo) {T_Raio = 15;
        }else T_Raio = 9;


    }

    // Update is called once per frame
    void Update()
    {
        if (ativado)
        {
            distancia = Vector3.Distance(transform.position, Jogador.transform.position);



            if (distancia < 6 && Input.GetKeyDown(Mover.Instance.Interagir) && !resgatado) resgatado = true;

            if (resgatado)
            {

                if (distancia > agent.stoppingDistance) anim.SetBool("Andando", true);
                if (distancia <= agent.stoppingDistance) anim.SetBool("Andando", false);

                if (temArma)
                {
                    if (armaDeFogo)
                    {
                        pistola.SetActive(true);
                        bastao.SetActive(false);
                    }
                    else
                    {
                        pistola.SetActive(false);
                        bastao.SetActive(true);
                    }
                }
                else
                {
                    pistola.SetActive(false);
                    bastao.SetActive(false);
                }


                if (!sobAtaque)
                {
                    OlharPlayer();
                    agent.stoppingDistance = 8;

                }

                if (sobAtaque && inimigoTemporario && temArma)
                {
                        distanciaZumbi = Vector3.Distance(transform.position, inimigoTemporario.transform.position);

                        OlharZumbie();
                        Ataca();

                        if (!esperando && distancia <= 5 && !armaDeFogo)
                        {
                             target = inimigoTemporario.transform;
                        }

                        agent.stoppingDistance = 4;
                }


                Collider[] colliderSob = Physics.OverlapSphere(transform.position, T_Raio, layer_Zumbi);
                foreach (Collider hitS in colliderSob)
                {
                    if (hitS.GetComponent<NewZombie>()) inimigoTemporario = colliderSob[0].gameObject;
                }

                CuboDeDano.SetActive(batendo);

                if (inimigoTemporario) sobAtaque = true;
                else sobAtaque = false;



                Esperar();


            }
        }
    }

    
    void Velocidade()
    {
        if (Mover.Instance.speed == Mover.Instance.runningSpeed && distancia > agent.stoppingDistance) agent.speed = 12;
        else agent.speed = 6;
    }

    void Esperar()
    {
        if (Input.GetKeyDown(Mover.Instance.Espera)) esperando = !esperando;

        if (esperando)
        {
            agent.speed = 0;
            if (distancia > agent.stoppingDistance) anim.SetBool("Andando", false);

        }


        if (!esperando)
        {
            SeguePlayer();
            Velocidade();
        }

    }

    void SeguePlayer()
    {
        if (sobAtaque && distancia > 5 || !sobAtaque)   target = Jogador.transform;
        agent.destination = target.transform.position;
      //if (distancia > agent.stoppingDistance) anim.SetBool("Andando", true);
      // else anim.SetBool("Andando", true);
    }

    void OlharPlayer()
    {
        if (distancia < agent.stoppingDistance)
        {
            //var JogadorRotation = Quaternion.LookRotation(Jogador.transform.position - transform.position);
            var JogadorRotation = Quaternion.LookRotation(new Vector3(Jogador.transform.position.x, transform.position.y, Jogador.transform.position.z) - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, JogadorRotation, rotationSpeed * Time.deltaTime);
        }
    }

    void OlharZumbie()
    {
        if (distanciaZumbi < T_Raio)
        {
           // var ZumbiRotation = Quaternion.LookRotation(inimigoTemporario.transform.position - transform.position);
            var ZumbiRotation = Quaternion.LookRotation(new Vector3(inimigoTemporario.transform.position.x, transform.position.y, inimigoTemporario.transform.position.z) - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, ZumbiRotation, 5 * Time.deltaTime);
        }
    }

    void Ataca()
    {
        currentRate += Time.deltaTime;

        if (!armaDeFogo)
        {
            if (distanciaZumbi < 5 && currentRate >= fireRate)
            {
                anim.Play("Ataque", 0, 0);
                currentRate = 0;
            }
        }

        if (armaDeFogo)
        {

            if (currentRate >= 0.4f)
            {
                anim.Play("Atira", 0, 0);
                muzzleParticles.Emit(1);
                StartCoroutine(MuzzleFlashLight());
                currentRate = 0;

                if (Physics.Raycast(pistola.transform.position, pistola.transform.forward, out RaycastHit hit, 20, layer_Zumbi))
                {
                    if (hit.transform.CompareTag("Zombie"))
                    {
                        hit.transform.GetComponent<NewZombie>().TomaDano(dano);
                        hit.transform.GetComponent<Rigidbody>().velocity = transform.forward * 3.5f;
                        Instantiate(impactBlood, hit.point, Quaternion.LookRotation(hit.normal), hit.transform.parent);
                    }

                    Debug.Log(hit.transform);
                }
            }

        }
    }

    public void TomaDano(int dano)
    {
        vida -= dano;
        anim.Play("Hit", 0, 0f);
        if (vida <= 0) Morre();
    }

    void Morre()
    {
        Destroy(gameObject);
    }

    public void Bater()
    {
        batendo = true;
    }

    public void Desbater()
    {
        batendo = false;
    }

    private IEnumerator MuzzleFlashLight()
    {
        muzzleflashLight.enabled = true;
        yield return new WaitForSeconds(0.02f);
        muzzleflashLight.enabled = false;
    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, T_Raio);
    }*/
}
