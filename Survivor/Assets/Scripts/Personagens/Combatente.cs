﻿
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public enum estadoCombate { idle, ataque, esquiva }


[RequireComponent(typeof(Animator))]
public class Combatente : Destrutivel   //recebe e da pauladas
{
    protected FerramentaNome ferramenta;
    public Ferramenta ferramentaValores;
    public GameObject ferramentaParent;


    [HideInInspector] public Material materialTesteColisores;
    int xp;

    [Range(0,10)]
    [SerializeField] public int forca;
    [Range(0, 10)]
    [SerializeField] public int agilidade;


    protected override void Awake()
    {
        base.Awake();
    }

    protected void Iniciar()
    {
        RecuperarVida(VidaMax());

        AtualizarAtributosSet(forca, agilidade);
    }

    public virtual void AtualizarAtributosAdicionar(int forcaMais, int agilidadeMais)
    {
        forca = Mathf.Clamp(forca += forcaMais, 0, 100);
        agilidade = Mathf.Clamp(agilidade += agilidadeMais, 0, 100);
    }
    public virtual void AtualizarAtributosSet(int forcaMais, int agilidadeMais)
    {
        forca = Mathf.Clamp(forcaMais, 0, 100);
        agilidade = Mathf.Clamp(agilidadeMais, 0, 100);
    }

    /*
    public virtual void AtualizarFerramenta(FerramentaNome ferramentaNovo) // o item que ta sendo segurado
    {
        if (Itens.Instance == null)
        {
            Debug.LogError("ATUALIZE O DICIONARIO PRIMEIRO");
            return;
        }

        //objeto 3D
        ferramentaParent.transform.GetChild((int)ferramenta).gameObject.SetActive(false); //disabilita usado previamente

        ferramenta = ferramentaNovo;
        ferramentaValores = Itens.Instance.ferramentasDicionario[(int)ferramentaNovo];

        ferramentaParent.transform.GetChild((int)ferramentaNovo).gameObject.SetActive(true); //habilita ferramenta nova
    }
    public virtual void AtualizarFerramenta(Ferramenta ferramentaNovo) // o item que ta sendo segurado
    {
        if (Itens.Instance == null)
        {
            Debug.LogError("ATUALIZE O DICIONARIO PRIMEIRO");
            return;
        }

        //objeto 3D
        ferramentaParent.transform.GetChild(ferramentaValores.IDLocal).gameObject.SetActive(false); //disabilita usado previamente

        ferramenta = (FerramentaNome)ferramentaNovo.IDLocal;
        ferramentaValores = ferramentaNovo;

        ferramentaParent.transform.GetChild(ferramentaNovo.IDLocal).gameObject.SetActive(false); //disabilita usado previamente
    } 
    */

    /*#region Colisores Ataque
    public void ColisorArmaTodosOn()
    {
        Collider armaAtual = ferramentaParent.transform.GetChild((int)ferramenta).GetComponent<Collider>();
        armaAtual.GetComponent<Collider>().enabled = true;
        armaAtual.GetComponent<MeshRenderer>().material.color = Color.red;
    }
    public void ColisorArmaTodosOff()
    {
        Collider armaAtual = ferramentaParent.transform.GetChild((int)ferramenta).GetComponent<Collider>();
        armaAtual.GetComponent<Collider>().enabled = false;
        armaAtual.GetComponent<MeshRenderer>().material.color = Color.white;
    }
    public void ColisorArmaOn(int numColisor)
    {
        Collider armaAtual = ferramentaParent.transform.GetChild(numColisor).GetComponent<Collider>();
        armaAtual.GetComponent<Collider>().enabled = true;
        armaAtual.GetComponent<MeshRenderer>().material.color = Color.red;
    }
    public void ColisorArmaOff(int numColisor)
    {
        Collider armaAtual = ferramentaParent.transform.GetChild(numColisor).GetComponent<Collider>();
        armaAtual.GetComponent<Collider>().enabled = false;
        armaAtual.GetComponent<MeshRenderer>().material.color = Color.white;
    }
    public void MostrarEsconderColisores()
    {

            for (int j = 0; j < ferramentaParent.transform.childCount; j++)
            {
                MeshRenderer meshR = ferramentaParent.transform.GetChild(j).gameObject.GetComponent<MeshRenderer>();
                meshR.enabled = (!meshR.enabled);
            }
    } //mesh renderer

    public void MostrarColisores()
    {
        for (int j = 0; j < ferramentaParent.transform.childCount; j++)
        {
            MeshRenderer meshR = ferramentaParent.transform.GetChild(j).gameObject.GetComponent<MeshRenderer>();
            meshR.enabled = (meshR.enabled);
        }
    } //mesh renderer
    #endregion*/
}