﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Destrutivel : MonoBehaviour //recebe pauladas, mas não dá pauladas. Esse seria o combatente.
{
    [SerializeField] float vida;// { get { return vida; } protected set { vida = value; } }
    [SerializeField] float vidaMax;// { get { return vidaMax; } protected set { vidaMax = value; } }
    [SerializeField] protected bool imortal = false;
    public bool vivo { get { return vida > 0; } }

    public UnityEvent morte;

    [Header("Feedback")]
    public Image vidaImgFill;
    public Text vidaText;
    public Text vidaMaxText;

    [Header("Animator")]
    protected Animator anim;
    [SerializeField] string triggerDano = "";
    [SerializeField] string triggerMorte = "";

    protected virtual void Awake()
    {
        anim = GetComponent<Animator>();
        AtualizarCanvas();
    }

    public void ReceberDano(float dano)
    {
        if (imortal) return;
        if (!vivo) return;

        vida = Mathf.Clamp(vida - dano, 0, vidaMax);

       /* if (anim && !GetComponent<IA_Zumbi>())
            anim.SetTrigger(triggerDano);
       
        if (anim && GetComponent<IA_Zumbi>())
            anim.Play("Hit", 0, 0f);*/



        AtualizarCanvas();

        if (vida <= 0)
        {
            Morrer();

            /*if (GetComponent<IA_Zumbi>())
            {
                GetComponent<IA_Zumbi>().Morreer();
                Destroy(gameObject.GetComponent<IA_Zumbi>());
            }*/

            if (GetComponent<ArmadilhaEstacas>())
            {
                GetComponent<ArmadilhaEstacas>().Destruida();
            }
        } // if morte
    }

    void AtualizarCanvas()
    {
        if (vidaImgFill) vidaImgFill.fillAmount = (float)vida / (float)vidaMax;
        if (vidaText) vidaText.text = vida.ToString();
        if (vidaMaxText) vidaMaxText.text = vidaMax.ToString();
    }

    public float VidaPorcentagem()
    {
        return (float)vida / (float)vidaMax;
    }
    public float Vida()
    {
        return (float)vida;
    }

    public float VidaMax()
    {
        return (float)vidaMax;
    }

    public void RecuperarVida(float vidaRecuperar)
    {
        if (!vivo) return;

        vida = Mathf.Clamp(vida + vidaRecuperar, 0, vidaMax);

        AtualizarCanvas();
    } //clampar a vida pra nao subir mais do que o vigor

    protected virtual void Morrer()
    {
        /*if (anim)
            anim.SetTrigger(triggerMorte);
        morte.Invoke();*/
    }

    public void DestroyObject(GameObject toDestroy)
    {
        Destroy(toDestroy);
    }
    /*private void FixedUpdate()
    {
        //RecuperarVida();
    }*/

    /*protected void RecuperarVida()
    {
        if (vida < vidaVigor)
        {
            cooldownRecover += (0.02f * velocidadeRecuperarVidaVigor);
            if (cooldownRecover >= 1)
            {
                RecuperarVida(1);
                cooldownRecover = 0;
            }
        }
    }*/
}
