﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomPooling : MonoBehaviour {

    public GameObject parent;
    public int numEnabledObjsAtSameTime;
    public Vector2 randomEnableWaitingTime;
    public Vector2 randomDisableWaitingTime;
    public float updateRateCheckSecs;
    [Tooltip("It'll disable objects to enable others instead of waiting for them to disable themselves.")]
    public bool forcedPooling = false;
    public bool onEnable = true;
    bool pooling;
    int numObjsTotal;
    List<int> enabledObjs = new List<int>();
    List<int> disabledObjs = new List<int>();

    public GameObject pontosSpawnParent;
    public Transform[] pontosSpawn;

    private void Start()
    {
         
    }

    void OnEnable () {
        if (parent == null) parent = gameObject;
        numObjsTotal = parent.transform.childCount;

        for (int i = 0; i < numObjsTotal; i++)
        {
            parent.transform.GetChild(i).gameObject.SetActive(false);
        }

        pontosSpawn = new Transform[pontosSpawnParent.transform.childCount];
        for (int i = 0; i < pontosSpawnParent.transform.childCount; i++)
        {
            pontosSpawn[i] = pontosSpawnParent.transform.GetChild(i).transform;
        }

        if (checkForError()) return;

        UpdateEnabledObjsList();

        if (onEnable)
            StartCoroutine(RandomPoolingStart());
    }

    void UpdateEnabledObjsList()
    {
        enabledObjs.Clear();
        disabledObjs.Clear();
        for (int i = 0; i < numObjsTotal; i++)
        {
            if (parent.transform.GetChild(i).gameObject.activeSelf)
                enabledObjs.Add(i);
            else
                disabledObjs.Add(i);
        }
    }

    bool checkForError()
    {
        if (numEnabledObjsAtSameTime > numObjsTotal)
        {
            print("pooling error: umEnabledObjsAtSameTime greater than numObjsTotal (" + numEnabledObjsAtSameTime + " > " + numObjsTotal + ")");
            return true;
        }
        return false;
    }

    IEnumerator RandomPoolingStart()
    {
        if (checkForError())
            yield break;

        pooling = true;

        while (pooling)
        {
            UpdateEnabledObjsList();

            if (forcedPooling)
                ForcedPooling();
            else
                NotForcedPooling();


            yield return new WaitForSeconds(updateRateCheckSecs);
        }

        yield break;
    }

    void ForcedPooling()
    {
        //print("POOLING  >>> ENABLED " + enabledObjs.Count + " & TOTAL " +  numObjsTotal);
        if (disabledObjs.Count <= 0) //there are objs not active = prefer activating the ones not active first
            return;

            List<int> canEnable = new List<int>();
            canEnable.Clear();
            for (int i = 0; i < disabledObjs.Count; i++)
            {
                canEnable.Add(disabledObjs[i]);
            }

            List<int> canDisable = new List<int>();
            canDisable.Clear();
            for (int i = 0; i < enabledObjs.Count; i++)
            {
                canDisable.Add(enabledObjs[i]);
            }

            int numChanges = Mathf.Clamp(canEnable.Count, 0, numEnabledObjsAtSameTime);

            for (int i = 0; i < numChanges; i++)// num of disabled objs that you can chose from
            {
                int chosenOne = Random.Range(0, canEnable.Count);
                //print(" [" + i + "]  CHOSEN NUM " + chosenOne + "/" + canEnable.Count + "   ITEM " + canEnable[chosenOne]);
                StartCoroutine(ObjSetActiveTiming(true, parent.transform.GetChild(canEnable[chosenOne]).gameObject));
                canEnable.Remove(canEnable[chosenOne]);
            }

            if(enabledObjs.Count >= numChanges)
            for (int i = 0; i < numChanges; i++) //num of enabled objs that will be disabled
            {
                 int chosenOne = Random.Range(0, canDisable.Count);
                 StartCoroutine(ObjSetActiveTiming(false, parent.transform.GetChild(canDisable[chosenOne]).gameObject));
                 canDisable.Remove(canDisable[chosenOne]);
            }
    }

    void NotForcedPooling()
    {
        if (numEnabledObjsAtSameTime == enabledObjs.Count) //do nothing cause to maintain the numEnabledObjsAtSameTime would mean to disable objs
            return;

        List<int> canEnable = new List<int>();
        canEnable.Clear();
        for (int i = 0; i < disabledObjs.Count; i++)
        {
            canEnable.Add(disabledObjs[i]);
        }

        for (int i = 0; i < numEnabledObjsAtSameTime - enabledObjs.Count; i++)
        {
            int chosenOne = Random.Range(0, canEnable.Count);
            StartCoroutine(ObjSetActiveTiming(true, parent.transform.GetChild(canEnable[chosenOne]).gameObject));
            canEnable.Remove(canEnable[chosenOne]);
        }
    }

    IEnumerator ObjSetActiveTiming(bool active, GameObject obj)
    {
        if (active)
            yield return new WaitForSeconds(Random.Range(randomEnableWaitingTime.x, randomEnableWaitingTime.y));
        else
            yield return new WaitForSeconds(Random.Range(randomDisableWaitingTime.x, randomDisableWaitingTime.y));

        if (!pooling)
            yield break;

        obj.SetActive(active);
        obj.transform.position = pontosSpawn[Random.Range(0, pontosSpawn.Length)].position;
    }

    public void PoolingStart()
    {
        if (!pooling)
            StartCoroutine(RandomPoolingStart());
    }
    public void PoolingStop()
    {
        pooling = false;
    }
    public void DisableAllObjs()
    {
        StartCoroutine(DisableAll(randomDisableWaitingTime));
    }
    IEnumerator DisableAll(Vector2 randomWaitingTimeBetweenObjs)
    {
        for (int i = 0; i < numObjsTotal; i++)
        {
            parent.transform.GetChild(i).gameObject.SetActive(false);
            yield return new WaitForSeconds(Random.Range(randomWaitingTimeBetweenObjs.x, randomWaitingTimeBetweenObjs.y));
        }
    }
}
