﻿using UnityEngine;
using UnityEngine.UI;

public class Player : Destrutivel
{
    public static Player Instance;
    CG_Vitto playerV;
    [SerializeField] public Vitais vitais;
    public AtributoCanvas[] atributosVitais; //vida, energia, fome, sede; ATRIBUTOS DO PLAYER sao 4
    public int raycastDistancia;
    public LayerMask myLayerMask;
    [Range(0,5)]
    [SerializeField] float tempoInteragirMesaCraft;
    [Range(0, 5)]
    [SerializeField] float tempoInteragirItem;
    [Range(0, 5)]
    [SerializeField] float tempoInteragirLootBox;
    [SerializeField] float tempoInteragindo;
    [SerializeField] public Image tempoInteragindoFill;
    /*[SerializeField]*/ GameObject objInteragindo;
    /*[HideInInspector] */public GameObject iconeInteracao;

    protected override void Awake()
    {
        base.Awake();
        #region Instance
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);
        #endregion
        playerV = gameObject.GetComponent<CG_Vitto>();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        if (Construir.Enabled()) return; //se tiver em construcao (posicionando obj) ele nao muda de arma, nao abre inventario nem checa interacao de obj no chao

        #region Abrir Inventário Rápido - Input 12345
        if (Input.GetKeyDown(KeyCode.Alpha1))
            InventarioRapido.Instance.SelecionarRapido(0, false);

        if (Input.GetKeyDown(KeyCode.Alpha2))
            InventarioRapido.Instance.SelecionarRapido(1, false);

        if (Input.GetKeyDown(KeyCode.Alpha3))
            InventarioRapido.Instance.SelecionarRapido(2, false);

        if (Input.GetKeyDown(KeyCode.Alpha4))
            InventarioRapido.Instance.SelecionarRapido(3, false);

        if (Input.GetKeyDown(KeyCode.Alpha5))
            InventarioRapido.Instance.SelecionarRapido(4, false);
        #endregion

        if (Input.GetKeyDown(KeyCode.Tab)) // abrir mochila
        {
            InventarioMochila.Instance.SwitchEnable();

            if (!InventarioMochila.Instance.Enabled()) // se mochila foi desligada
            {
                InventarioLoot.Instance.Enable(false);// desliga a de loot tb
                InventarioCraft.Instance.Enable(false);
            }
            else
            {
                InventarioRapido.Instance.Enable(true); //se mochila ligada, liga o invent primario
            }
        }
    }

    private void FixedUpdate()
    {
        if (!InventarioMochila.Instance.Enabled() && !InventarioLoot.Instance.Enabled() && !Construir.Instance.gameObject.activeInHierarchy)
            ChecarInteracao(); //ver se ta em distancia pra interagir com objetos
        else
        {
            InteracaoIcone.Instance.SetInteracao(null, transform.position);
            objInteragindo = null;
            iconeInteracao.SetActive(false);
            tempoInteragindo = 0;
            tempoInteragindoFill.fillAmount = 0;
        }
    }
    public void VitaisAdicionar(Vitais vitaisNovo)
    {
        //if ()
        playerV.vida = (int)Mathf.Clamp(playerV.vida + vitaisNovo.vida, 0, 100);
        vitais.energia = Mathf.Clamp(vitais.energia + vitaisNovo.energia, 0, 100);
        vitais.fome = Mathf.Clamp(vitais.fome + vitaisNovo.fome, 0, 100);
        vitais.sede = Mathf.Clamp(vitais.sede + vitaisNovo.sede, 0, 100);

        //if (Vida() <= 0) Morrer();
        if (vitais.fome <= 0) playerV.morte.SetActive(true);
        if (vitais.sede <= 0) playerV.morte.SetActive(true);
        /*vitais.vida += vitaisNovo.vida;
        vitais.energia += vitaisNovo.energia;
        vitais.fome += vitaisNovo.fome;
        vitais.sede += vitaisNovo.sede;*/

        HUD.Instance.AtualizarVitais();
    }

    public void VitaisSetar(Vitais vitaisNovo)
    {
        playerV.vida = (int)vitaisNovo.vida;
        vitais.energia = vitaisNovo.energia;
        vitais.fome = vitaisNovo.fome;
        vitais.sede = vitaisNovo.sede;

        HUD.Instance.AtualizarVitais();
    }

    public void ChecarInteracao()
    {
        RaycastHit obj;
        Ray distAcao = new Ray(Camera.main.transform.position, Camera.main.transform.forward);
        //print(Camera.main.gameObject.name + " / " + Camera.main.gameObject.transform.parent.name);
        if (Physics.Raycast(distAcao, out obj, raycastDistancia, myLayerMask, QueryTriggerInteraction.Ignore)) //se tiver objeto no meio
        {
            GameObject interagindoAgora = obj.transform.gameObject;
            if (interagindoAgora != objInteragindo)
            {
                tempoInteragindo = 0;
                tempoInteragindoFill.fillAmount = 0;
            }
            objInteragindo = interagindoAgora;

            Interativo interativo = obj.transform.gameObject.GetComponent<Interativo>(); //se for obj interativo
            if (interativo)
            if (interativo.PodeInteragir())
            {
                iconeInteracao.gameObject.SetActive(true);
                tempoInteragindoFill.fillAmount = interativo.Progresso();
                InteracaoIcone.Instance.SetInteracao(Itens.Instance.iconeInteracaoItem, obj.transform.position);
                if (Input.GetKey(Mover.Instance.PegarItem)) interativo.Interagindo();
                else
                {
                    interativo.interagirProgresso = 0;
                    tempoInteragindoFill.fillAmount = interativo.Progresso();
                }
                return;
            }

            ObraConstrucaoGame obraConstrucaoGame = null;
            if (obj.transform.gameObject.GetComponent<ConstrucaoGame>()) obraConstrucaoGame = obj.transform.gameObject.GetComponent<ConstrucaoGame>().obraConstrucaoGame;
            if (obj.transform.gameObject.GetComponent<ObraGame>()) obraConstrucaoGame = obj.transform.gameObject.GetComponent<ObraGame>().obraConstrucaoGame;
            
            if (obraConstrucaoGame)
            {
                tempoInteragindo = 0;
                iconeInteracao.gameObject.SetActive(true);
                tempoInteragindoFill.fillAmount = obraConstrucaoGame.Progresso();
                InteracaoIcone.Instance.SetInteracao(Itens.Instance.iconeInteracaoItem, obj.transform.position);
                if (Input.GetKey(Mover.Instance.PegarItem)) obraConstrucaoGame.Interagindo();
                /*else
                {
                    tempoInteragindo = 0;
                    tempoInteragindoFill.fillAmount = 0;
                }
                if (tempoInteragindo >= tempoInteragirMesaCraft)
                    InteragirMesaCraft(obj, mesaCraft);*/
                return;
            }

            MesaCraft mesaCraft = obj.transform.gameObject.GetComponent<MesaCraft>(); //se for loot box no chao
            if (mesaCraft)
            {
                iconeInteracao.gameObject.SetActive(true);
                tempoInteragindoFill.fillAmount = tempoInteragindo/ tempoInteragirMesaCraft;
                InteracaoIcone.Instance.SetInteracao(Itens.Instance.iconeInteracaoCraft, obj.transform.position);
                if (Input.GetKey(Mover.Instance.InteragirLootBox)) tempoInteragindo += 0.02f;
                else
                {
                    tempoInteragindo = 0;
                    tempoInteragindoFill.fillAmount = 0;
                }
                if (tempoInteragindo >= tempoInteragirMesaCraft)
                InteragirMesaCraft(obj, mesaCraft);
                return;
            }

            ItemGame itemGame = obj.transform.gameObject.GetComponent<ItemGame>(); //se for item de inventario
            if (itemGame)
            {
                iconeInteracao.gameObject.SetActive(true);
                tempoInteragindoFill.fillAmount = tempoInteragindo / tempoInteragirItem;
                InteracaoIcone.Instance.SetInteracao(Itens.Instance.iconeInteracaoItem, obj.transform.position);
                if (Input.GetKey(Mover.Instance.PegarItem)) tempoInteragindo += 0.02f;
                else
                {
                    tempoInteragindo = 0;
                    tempoInteragindoFill.fillAmount = 0;
                }
                if (tempoInteragindo >= tempoInteragirItem)
                    InteragirItemGame(obj, itemGame);
                return;
            }

            //se for Granada no chao
            if (obj.transform.gameObject.tag == "Granada")
            {
                iconeInteracao.gameObject.SetActive(true);
                tempoInteragindoFill.fillAmount = tempoInteragindo / tempoInteragirItem;
                InteracaoIcone.Instance.SetInteracao(Itens.Instance.iconeInteracaoItem, obj.transform.gameObject.transform.position);
                if (Input.GetKey(Mover.Instance.PegarItem)) tempoInteragindo += 0.02f;
                else
                {
                    tempoInteragindo = 0;
                    tempoInteragindoFill.fillAmount = 0;
                }
                if (tempoInteragindo >= tempoInteragirItem)
                {
                    CG_Vitto.Instance.N_Granadas++;
                    Destroy(obj.transform.gameObject);
                    tempoInteragindo = 0;
                }
                return;
            }

            LootBox inventarioBox = obj.transform.gameObject.GetComponent<LootBox>(); //se for loot box no chao
            if (inventarioBox)
            {
                iconeInteracao.gameObject.SetActive(true);
                tempoInteragindoFill.fillAmount = tempoInteragindo / tempoInteragirLootBox;
                InteracaoIcone.Instance.SetInteracao(Itens.Instance.iconeInteracaoLootbox, obj.transform.position);
                if (Input.GetKey(Mover.Instance.InteragirMesaCraft)) tempoInteragindo += 0.02f;
                else
                {
                    tempoInteragindo = 0;
                    tempoInteragindoFill.fillAmount = 0;
                }
                    if (tempoInteragindo >= tempoInteragirLootBox)
                    InteragirInventarioBox(obj, inventarioBox);
                return;
            }




            InteracaoIcone.Instance.SetInteracao(null, transform.position);
            objInteragindo = null;
            tempoInteragindo = 0;
            tempoInteragindoFill.fillAmount = 0;
            iconeInteracao.SetActive(false);
        }
        else
        {
            InteracaoIcone.Instance.SetInteracao(null, transform.position);
            iconeInteracao.SetActive(false);
            objInteragindo = null;
            tempoInteragindo = 0;
            tempoInteragindoFill.fillAmount = 0;
        }
    }

    void InteragirInventarioBox(RaycastHit obj, LootBox inventarioBox)
    {

            InventarioLoot.Instance.SetInventario(inventarioBox.GerarItens()); //seta inventario do chao pra mostrar os itens q tem na lootbox
            InventarioLoot.atual = inventarioBox;
            InventarioLoot.Instance.Enable(true);
            InventarioMochila.Instance.Enable(true);
            InventarioRapido.Instance.Enable(true);
        tempoInteragindo = 0;
    }

    void InteragirItemGame(RaycastHit obj, ItemGame itemGame)
    {
            itemGame.TentarAdicionarInventario();
        tempoInteragindo = 0;
    }

    void InteragirMesaCraft(RaycastHit obj, MesaCraft mesaCraft)
    {
            InventarioCraft.Instance.Enable(true); //seta inventario do chao pra mostrar os itens q tem na lootbox
            InventarioMochila.Instance.Enable(true);
            InventarioRapido.Instance.Enable(true);
        tempoInteragindo = 0;
    }
}
