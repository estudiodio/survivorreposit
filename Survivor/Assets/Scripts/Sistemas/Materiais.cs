using UnityEngine;

[System.Serializable]
public class Materiais
{
	[SerializeField] public int madeira;
	[SerializeField] public int ferro;
	[SerializeField] public int lingote;
	[SerializeField] public int molacomum;
	[SerializeField] public int molatornado;
	[SerializeField] public int metal;
	[SerializeField] public int chumbo;
	[SerializeField] public int cano;
	[SerializeField] public int parafuso;
	[SerializeField] public int tecido;
	[SerializeField] public int prego;
}
