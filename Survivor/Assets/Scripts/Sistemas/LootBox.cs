﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LootBox : MonoBehaviour
{
    [SerializeField] public List<ItemNome> itensNome;
    Item[] itens = new Item[0];
    [HideInInspector] public bool podeUpdatear; // é pra ser usada com lootboxeseditor mas to com preguiça vo fazer algo de util

    public Item[] GerarItens()
    {
        if (itens.Length == 0) //se é a primeira vez abrindo essa caixa
        {
            itens = new Item[InventarioLoot.Instance.objetosInventario.Length];
            for (int i = 0; i < itensNome.Count; i++)
            {
                itens[i] = (Itens.Instance.Item(itensNome[i]));
            }
        }
        return itens;
    }

    public void SetItens(Item[] itensNovos)
    {
        itens = itensNovos;
    }
}
