﻿using UnityEngine;

//[RequireComponent(typeof(OnMouseOverCollider))]
[System.Serializable]
public class ItemGame : MonoBehaviour
{
    [SerializeField] protected ItemNome? itemNullable = null; // unity nao aparece nullable int, dai n da pra usar essa classe pra inventario, por exemplo, onde um slot estaria vazio
    [SerializeField] protected ItemNome item;
    [SerializeField] protected int quantidade;
    Player player;

    private void Awake()
    {
        itemNullable = item;
        player = Player.Instance;

        if (Itens.Instance.Item(item).tipoItem == TipoItem.Ferramenta)
            switch (Itens.Instance.Item(item).IDLocal)
            {
                case 0: //Desert
                    quantidade = Mathf.Clamp(quantidade, 0, Pistola_V.Instance.maxDeMunicaoPente);
                    break;
                case 1: //USPS
                    quantidade = Mathf.Clamp(quantidade, 0, Pistola_V.Instance.maxDeMunicaoPente);
                    break;
                case 2://Ak_47
                    quantidade = Mathf.Clamp(quantidade, 0, Rifle_V.Instance.magAKmax);
                    break;
                case 3://M4A4
                    quantidade = Mathf.Clamp(quantidade, 0, Rifle_V.Instance.magM4max);
                    break;
                case 4://G35G
                    quantidade = Mathf.Clamp(quantidade, 0, Rifle_V.Instance.magG3Smax);
                    break;
                case 5://Bastao_1
                    quantidade = 0;
                    break;
                case 6://Bastao_2
                    quantidade = 0;
                    break;
                case 7://Bastao_3
                    quantidade = 0;
                    break;

                default:
                    break;
            }
    }

    public void ChecarInteracao()
    {
        player.ChecarInteracao();
    }

    public void SetQuantidade(int qnt)
    {
        quantidade = qnt;
    }

    public void TentarAdicionarInventario()
    {
        //print(item + " " + Itens.Instance.Item(item).tipoItem);
        //print("TIPO MUNICAO " + Itens.Instance.Item(item).tipoMunicao);
        bool colocarInventarioRapido = false;

        Item itemBase = Itens.Instance.Item(item);
        Item itemNovo = new Item();
        //print("TIPO MUNICAO BASE " + itemBase.tipoMunicao);

        //(itemBase.tipoItem == TipoItem.Recurso || itemBase.tipoItem == TipoItem.Ferramenta)
        //(quantidade != itemBase.quantidade)

        if (itemBase.tipoItem == TipoItem.Recurso || itemBase.tipoItem == TipoItem.Ferramenta)
        {
            itemNovo = new Item()
            {
                nome = itemBase.nome,
                nomeReal = itemBase.nomeReal,
                tipoItem = itemBase.tipoItem,
                IDLocal = itemBase.IDLocal,
                IDGeral = itemBase.IDGeral,
                prefab = itemBase.prefab,
                imagem = itemBase.imagem,
                quantidade = quantidade,
                corpoACorpo = itemBase.corpoACorpo,
                tipoMunicao = itemBase.tipoMunicao
            };

            colocarInventarioRapido = InventarioRapido.Instance.AdicionarItem(itemNovo, this);
        }
        else
            colocarInventarioRapido = InventarioRapido.Instance.AdicionarItem(Itens.Instance.Item(item), this);

        if (colocarInventarioRapido) //se conseguir colocar no inventario rapido  (armas)
        {
            Destroy(gameObject);
        }
        else
        {
            bool colocarInventarioMochila = false;
            if (itemBase.tipoItem == TipoItem.Recurso)
                colocarInventarioMochila = InventarioMochila.Instance.AdicionarItem(itemNovo, this);
            else
                colocarInventarioMochila = InventarioMochila.Instance.AdicionarItem(Itens.Instance.Item(item), this);

            //print("COLOCOU " + colocarInventarioMochila + " E É RECURSO? " + (itemBase.tipoItem == TipoItem.Recurso));
            if (colocarInventarioMochila)
            {
                Destroy(gameObject);
            }
        }
    }

    public void AtualizarItem(ItemNome? itemNovo)
    {
        itemNullable = itemNovo;
        if (itemNovo == null) item = 0;
        else item = (ItemNome)(int)itemNovo;
    }

    public ItemNome ItemNome()
    {
        return item;
    }

    public Item Item()
    {
        return Itens.Instance.itensDicionario[(int)item];
    }

    public bool Vazio()
    {
        if (itemNullable == null)
            return true;
        else
            return false;
    }

    public Sprite Imagem()
    {
        return Itens.Instance.ItemSprite(item);
    }

    public string Nome()
    {
        return Itens.Instance.ItemNome(item);
    }
}
 