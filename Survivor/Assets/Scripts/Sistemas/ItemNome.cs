
using UnityEngine;
[System.Serializable]
public class ItemNomeQuantidade
{
	[SerializeField] public ItemNome itemnome;
	[SerializeField] public int quantidade;
}
public enum ItemNome
{
	Desert,
	USPS,
	AK,
	M4A4,
	G35G1,
	BastaoUm,
	BastaoDois,
	BastaoTres,
	BoloChocolate,
	FatiaPizza,
	MacaVerde,
	MacaVermelha,
	Sanduiche,
	Bandagem,
	PrimeirosSocorros,
	MunicaoLeve,
	MunicaoMedia,
	Madeira,
	Ferro,
	Lingote,
	MolaComum,
	MolaTornado,
	Metal,
	Chumbo,
	Cano,
	Parafuso,
	Tecido,
	Prego,
	MesaCraft,
	ArmadilhaUm,
	ArmadilhaDois	
}
