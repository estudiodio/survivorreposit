﻿using UnityEngine;
using UnityEngine.UI;

public class ConstrucaoGame : Destrutivel //obra acabada, construcao pronta
{
    //[Range(0, 1)]
    [SerializeField] float repararProgresso = 0;
    [Range(0, 20)]
    [SerializeField] float tempoRepararProgresso = 0;
    [Range(0, 20)]
    [SerializeField] float quantidadeReparar = 0;
    public ObraGame obra;
    [HideInInspector] public ObraConstrucaoGame obraConstrucaoGame;

    public void Reparando()
    {
        repararProgresso += 0.02f;
        if (Progresso() >= 1)
        {
            repararProgresso = 0;
            RecuperarVida(quantidadeReparar);
        }
        vidaImgFill.fillAmount = VidaPorcentagem();
    }

    public float Progresso()
    {
        return repararProgresso / tempoRepararProgresso;
    }

   /* public void ObraPronta()
    {
        //if (obra) obra.GetComponent<ObraGame>().obraProgresso = 1;
        RecuperarVida(VidaMax());
        if(obra) obra.SetActive(false);
        construcao.SetActive(true);
        this.gameObject.SetActive(false);
    }*/

    protected override void Awake()
    {
        base.Awake();
        if(obra)
        if (!obra.gameObject.activeSelf || obra.Progresso() >= 1 || gameObject.activeSelf) obra.ObraPronta();
    }
}
