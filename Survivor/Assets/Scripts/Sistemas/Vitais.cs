using UnityEngine;

[System.Serializable]
public class Vitais
{
	[SerializeField] public float vida;
	[SerializeField] public float energia;
	[SerializeField] public float fome;
	[SerializeField] public float sede;
}
