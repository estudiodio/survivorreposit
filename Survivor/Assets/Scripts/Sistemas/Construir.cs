﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Construir : MonoBehaviour
{
    [SerializeField] public static GameObject itemConstruir;
    [SerializeField] public static GameObject itemConstruirInstance;
    [Range(1,20)]
    public float distanciaMaxPosicionar = 10; //e um clone da de cima, mas esse ta sendo usado
    public static Construir Instance;
    public Vector3 offSet;
    public LayerMask myLayerMask;


    public void Iniciar()
    {
        #region Instance
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);
        #endregion

        Enable(false);
    }

    // Update is called once per frame
    void Update()
    {
            PosicionarConstrucao();
        RotacionarConstrucao();

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            CriarConstrucao();
            return;
        }

        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            CancelarConstrucao();
            return;
        }
    }

    public static void SwitchEnable()
    {
        Instance.gameObject.SetActive(Instance.gameObject.activeSelf);

        if (!Enabled())
        {
            itemConstruir = null;
            itemConstruirInstance = null;
        }
    }

    public static void Enable(bool enabled)
    {
        Instance.gameObject.SetActive(enabled);

        if (!Enabled())
        {
            itemConstruir = null;
            itemConstruirInstance = null;
        }
    }

    public static bool Enabled()
    {
        return Instance.gameObject.activeSelf;
    }

    public static void SetConstrucao(GameObject itemObj)
    {
        //print("setar Construcao");
        if (itemObj == null) return;
        if (Itens.Instance.Item(itemObj.GetComponent<ItemGame>().ItemNome()).tipoItem != TipoItem.Construcao) return;
        itemConstruir = itemObj;

        itemConstruirInstance = Instantiate(itemConstruir, Camera.main.transform.position + Instance.offSet, itemConstruir.transform.rotation);
        Enable(true);
    }

    public static void PosicionarConstrucao()
    {
        if (itemConstruirInstance == null) return;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hitInfo;

        if (Physics.Raycast(ray, out hitInfo, Instance.distanciaMaxPosicionar, Instance.myLayerMask, QueryTriggerInteraction.Ignore))
        {
            //print("COLIDIU " + hitInfo.collider.gameObject.name);
            itemConstruirInstance.transform.position = hitInfo.point;
            //itemConstruirInstance.transform.rotation = Quaternion.FromToRotation(Vector3.up, hitInfo.normal); setar rotacao de acordo com o rayhit
        }
        else
        {
            Ray ray2 = new Ray(Camera.main.transform.forward + Instance.offSet, -Vector3.up);
            RaycastHit hitInfo2;

            if (Physics.Raycast(ray2, out hitInfo2, 3f, Instance.myLayerMask, QueryTriggerInteraction.Ignore))
            {
                itemConstruirInstance.transform.position = Camera.main.transform.position + Instance.offSet;
            }
        }
    }

    static float mouseRotation = 0;
    public static void RotacionarConstrucao()
    {
        if (itemConstruirInstance == null) return;

        //print(mouseRotation);
        mouseRotation += Input.mouseScrollDelta.y;
        itemConstruirInstance.transform.eulerAngles = Vector3.up * mouseRotation * 10f;
    }

    public static void CriarConstrucao()
    {
        if (itemConstruirInstance == null) return;
        if (Itens.Instance.Item(itemConstruirInstance.GetComponent<ItemGame>().ItemNome()).tipoItem != TipoItem.Construcao) return;

        if (itemConstruirInstance.GetComponent<Armadilha_Urso>())
        {
            itemConstruirInstance.GetComponent<Armadilha_Urso>().enabled = true;
            itemConstruirInstance.GetComponent<Rigidbody>().isKinematic = false;
            itemConstruirInstance.GetComponent<SphereCollider>().enabled = true;
        }

        if (itemConstruirInstance.GetComponent<ArmadilhaEstacas>())
        {
            itemConstruirInstance.GetComponent<ArmadilhaEstacas>().enabled = true;
            BoxCollider[] cols = itemConstruirInstance.GetComponents<BoxCollider>();
            for (int i = 0; i < cols.Length; i++)
            {
                cols[i].enabled = true;
            }
        }
        Inventario.TerminouConstrucao(true);

        Enable(false);
    }

    public static void CancelarConstrucao()
    {
        if (itemConstruirInstance == null) return;
        if (Itens.Instance.Item(itemConstruirInstance.GetComponent<ItemGame>().ItemNome()).tipoItem != TipoItem.Construcao) return;

        Destroy(itemConstruirInstance);
        Inventario.TerminouConstrucao(false);
        Enable(false);
    }
}
