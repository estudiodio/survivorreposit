﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObraGame : Destrutivel //obra inacabada
{
    [Space (20)]

    [SerializeField] float obraProgresso = 0;
    [Range(0, 20)]
    [SerializeField] float tempoObraProgresso = 0;
    [SerializeField] float quantidade = 0;
    [Range(0, 10)]
    [SerializeField] float quantidadeProgressoMax = 0;
    public ConstrucaoGame construcao;
    [HideInInspector] public ObraConstrucaoGame obraConstrucaoGame;

    public void Construindo()
    {
        obraProgresso += 0.02f; // é pra ser chamado no fixed update, q tem 50 update por seg
        //obraImageFill.fillAmount = ProgressoPorcentagem();
        if (Progresso() >= 1)
        {
            quantidade++;
            obraProgresso = 0;
            if ((quantidade / quantidadeProgressoMax) >= 1)
            {
                ObraPronta();
            }
        }
    }

    public float Progresso()
    {
        return obraProgresso / tempoObraProgresso;
    }

    public void ObraPronta()
    {
        RecuperarVida(VidaMax());
        gameObject.SetActive(false);
        construcao.gameObject.SetActive(true);
        this.gameObject.SetActive(false);
        obraProgresso = tempoObraProgresso;
        quantidade = quantidadeProgressoMax;
    }

    protected override void Awake()
    {
        base.Awake();
        if (!gameObject.activeSelf || Progresso() >= 1 || construcao.gameObject.activeSelf) ObraPronta();
    }
}
