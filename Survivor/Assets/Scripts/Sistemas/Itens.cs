﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public enum TipoItem { Ferramenta, Comestivel, Recurso, Material, Construcao }
//public enum TipoFerramenta { Pistola, Rifle, Bastao}
public enum TipoMunicao { Leve, Media, Pesada }
[System.Serializable]
public class Item
{
    public string nome;
    public string nomeReal;
    public TipoItem tipoItem;
    [HideInInspector] public int IDLocal;// { get { return ID; } private set { ID = value; } }
    [HideInInspector] public int IDGeral;// { get { return ID; } private set { ID = value; } }
    public GameObject prefab;
    public Sprite imagem;
    [Range(0, 100)]
    public int quantidade; //só é usado quando é (ferramenta/arma = DANO) ou (recurso = QUANTIDADE)
    public bool corpoACorpo = false; //só é usado quando é (ferramenta/arma = DANO)
    public TipoMunicao tipoMunicao; //só é usado qnd é RECURSO / MUNICAO && arma/ferramenta correspondente
    public Vitais vitais; //só é usado qnd é COMESTIVEL
    public List<ItemNomeQuantidade> ingredientes; //
} // ak, pistola, faca, bazooka, machado

[System.Serializable]
public class Ferramenta
{
    public string nome;
    public string nomeReal;
    [HideInInspector] public int IDLocal;// { get { return ID; } private set { ID = value; } }
    [HideInInspector] public int IDGeral;// { get { return ID; } private set { ID = value; } }
    public GameObject prefab;
    public Sprite imagem;
    //[Range(0,10)]
    //public int qntMunicao;
    //public bool corpoACorpo = false; //só é usado quando é (ferramenta/arma = DANO)
    public TipoMunicao tipoMunicao; //só é usado qnd é RECURSO / MUNICAO
    public List<ItemNomeQuantidade> ingredientes; //
} // ak, pistola, faca, bazooka, machado

[System.Serializable]
public class Comestivel
{
    public string nome;
    public string nomeReal;
    [HideInInspector] public int IDLocal;// { get { return ID; } private set { ID = value; } }
    [HideInInspector] public int IDGeral;// { get { return ID; } private set { ID = value; } }
    public GameObject prefab;
    public Sprite imagem;
    public Vitais vitais;
    public List<ItemNomeQuantidade> ingredientes; //
} // carne, peixe, fruta, coco, agua, agua suja

[System.Serializable]
public class Recurso //aka municao
{
    public string nome;
    public string nomeReal;
    [HideInInspector] public int IDLocal;// { get { return ID; } private set { ID = value; } }
    [HideInInspector] public int IDGeral;// { get { return ID; } private set { ID = value; } }
    public GameObject prefab;
    public int quantidade;
    public Sprite imagem;
    public TipoMunicao tipoMunicao; //só é usado qnd é RECURSO / MUNICAO
    //public List<ItemNomeQuantidade> ingredientes; //
} // bala

[System.Serializable]
public class Material
{
    public string nome;
    public string nomeReal;
    [HideInInspector] public int IDLocal;// { get { return ID; } private set { ID = value; } }
    [HideInInspector] public int IDGeral;// { get { return ID; } private set { ID = value; } }
    public GameObject prefab;
    public Sprite imagem;
    public List<ItemNomeQuantidade> ingredientes; //
} // madeira, metal

[System.Serializable]
public class Vital
{
    public string nome;
    public string nomeReal;
    [HideInInspector] public int ID;// { get { return ID; } private set { ID = value; } }
    public Sprite imagem;
} // vida, energia, fome, sede

[System.Serializable]
public class Construcao
{
    public string nome;
    public string nomeReal;
    [HideInInspector] public int IDLocal;// { get { return ID; } private set { ID = value; } }
    [HideInInspector] public int IDGeral;// { get { return ID; } private set { ID = value; } }
    public GameObject prefab;
    public Sprite imagem;
    public List<ItemNomeQuantidade> ingredientes; //
} // oq da pra botar no chao


[System.Serializable]
public class Mesa
{
    public string nome;
    public string nomeReal;
    [HideInInspector] public int IDLocal;// { get { return ID; } private set { ID = value; } }
    [HideInInspector] public int IDGeral;// { get { return ID; } private set { ID = value; } }
    public GameObject prefab;
    public Sprite imagem;
    public List<ItemNomeQuantidade> ingredientes; //
} // mesa craft

public class Itens : MonoBehaviour
{
    public static Itens Instance;

    [Header("ITENS")]
    [HideInInspector] public Item[] itens;
    public Ferramenta[] ferramentas; //armas
    public Comestivel[] comestiveis; //mudam vida,energia,fome,sede
    public Recurso[] recursos; // municao
    public int quantidadeMaxBalasSlot;
    public Material[] materiais; // madeira, ferro e pa

    [Header("CONSTRUÇÕES")]
    public Construcao[] construcoes;

    //[Header("MESA CRAFT")]
    //public Mesa[] mesas;

    [Header("VITAIS")]
    public Vital[] vitais; //vida,energia,fome,sede

    [Space(20)]
    public Sprite iconeInteracaoItem;
    public Sprite iconeInteracaoCraft;
    public Sprite iconeInteracaoLootbox;

    [Space(20)]
    [Header("Atualizar")]

    public Dictionary<int, Item> itensDicionario;

    public Dictionary<int, Ferramenta> ferramentasDicionario;
    public  Dictionary<int, Comestivel> comestiveisDicionario;
    public  Dictionary<int, Recurso> recursosDicionario;
    public Dictionary<int, Material> materiaisDicionario;
    public Dictionary<int, Construcao> construcoesDicionario;
    //public Dictionary<int, Mesa> mesasDicionario;

    public  Dictionary<int, Vital> vitaisDicionario;

    public void Preencher()
    {
        #region Instance
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);
        #endregion

        List<string> listaItens = new List<string>();

        List<string> listaFerramentas = new List<string>();
        List<string> listaComestiveis = new List<string>();
        List<string> listaRecursos = new List<string>();
        List<string> listaMateriais = new List<string>();
        List<string> listaConstrucoes = new List<string>();
        //List<string> listaMesas = new List<string>();

        itensDicionario = new Dictionary<int, Item>();
        itensDicionario.Clear();

        for (int i = 0; i < ferramentas.Length; i++)
        {
            ferramentas[i].IDLocal = i;
            ferramentas[i].IDGeral = listaItens.Count;

            /*
            print(i + "  nome " + ferramentas[i].nome);
            print(i + "  nomeReal " + ferramentas[i].nomeReal);
            //print(i + "  tipoItem " + ferramentas[i].tipoItem);
            print(i + "  IDLocal " + ferramentas[i].IDLocal);
            print(i + "  IDGeral " + ferramentas[i].IDGeral);
            print(i + "  prefab " + ferramentas[i].prefab.name);
            print(i + "  imagem " + ferramentas[i].imagem);
            print(i + "  tipoMunicao " + ferramentas[i].tipoMunicao);
            print(i + "  nome " + ferramentas[i].nome);*/

            Item item = new Item()
            {
                nome = ferramentas[i].nome,
                nomeReal = ferramentas[i].nomeReal,
                tipoItem = TipoItem.Ferramenta,
                IDLocal = ferramentas[i].IDLocal,
                IDGeral = ferramentas[i].IDGeral,
                prefab = ferramentas[i].prefab,
                imagem = ferramentas[i].imagem,
                //corpoACorpo = ferramentas[i].corpoACorpo,
                tipoMunicao = ferramentas[i].tipoMunicao,
                //quantidade = ferramentas[i].qntMunicao,
                ingredientes = ferramentas[i].ingredientes
            };
            itensDicionario.Add(itensDicionario.Count, item);
            listaItens.Add(item.nome);
        }

        for (int i = 0; i < comestiveis.Length; i++)
        {
            comestiveis[i].IDLocal = i;
            comestiveis[i].IDGeral = listaItens.Count;

            Item item = new Item()
            {
                nome = comestiveis[i].nome,
                nomeReal = comestiveis[i].nomeReal,
                tipoItem = TipoItem.Comestivel,
                IDLocal = comestiveis[i].IDLocal,
                IDGeral = comestiveis[i].IDGeral,
                prefab = comestiveis[i].prefab,
                imagem = comestiveis[i].imagem,
                vitais = comestiveis[i].vitais,
                ingredientes = comestiveis[i].ingredientes
            };

            itensDicionario.Add(itensDicionario.Count, item);
            listaItens.Add(item.nome);
        }

        for (int i = 0; i < recursos.Length; i++)
        {
            recursos[i].IDLocal = i;
            recursos[i].IDGeral = listaItens.Count;

            Item item = new Item()
            {
                nome = recursos[i].nome,
                nomeReal = recursos[i].nomeReal,
                tipoItem = TipoItem.Recurso,
                IDLocal = recursos[i].IDLocal,
                IDGeral = recursos[i].IDGeral,
                prefab = recursos[i].prefab,
                imagem = recursos[i].imagem,
                quantidade = recursos[i].quantidade,
                tipoMunicao = recursos[i].tipoMunicao
                //ingredientes = recursos[i].ingredientes
            };

            itensDicionario.Add(itensDicionario.Count, item);
            listaItens.Add(item.nome);
        }

        for (int i = 0; i < materiais.Length; i++)
        {
            materiais[i].IDLocal = i;
            materiais[i].IDGeral = listaItens.Count;

            Item item = new Item()
            {
                nome = materiais[i].nome,
                nomeReal = materiais[i].nomeReal,
                tipoItem = TipoItem.Material,
                IDLocal = materiais[i].IDLocal,
                IDGeral = materiais[i].IDGeral,
                prefab = materiais[i].prefab,
                imagem = materiais[i].imagem,
                ingredientes = materiais[i].ingredientes
            };

            itensDicionario.Add(itensDicionario.Count, item);
            listaItens.Add(item.nome);
        }

        for (int i = 0; i < construcoes.Length; i++)
        {
            construcoes[i].IDLocal = i;
            construcoes[i].IDGeral = listaItens.Count;

            Item item = new Item()
            {
                nome = construcoes[i].nome,
                nomeReal = construcoes[i].nomeReal,
                tipoItem = TipoItem.Construcao,
                IDLocal = construcoes[i].IDLocal,
                IDGeral = construcoes[i].IDGeral,
                prefab = construcoes[i].prefab,
                imagem = construcoes[i].imagem,
                ingredientes = construcoes[i].ingredientes
            };

            itensDicionario.Add(itensDicionario.Count, item);
            listaItens.Add(item.nome);
        }

        /*for (int i = 0; i < mesas.Length; i++)
        {
            mesas[i].IDLocal = i;
            mesas[i].IDGeral = listaItens.Count;

            Item item = new Item()
            {
                nome = mesas[i].nome,
                nomeReal = mesas[i].nomeReal,
                tipoItem = TipoItem.MesaCraft,
                IDLocal = mesas[i].IDLocal,
                IDGeral = mesas[i].IDGeral,
                prefab = mesas[i].prefab,
                imagem = mesas[i].imagem,
                ingredientes = mesas[i].ingredientes
            };

            itensDicionario.Add(itensDicionario.Count, item);
            listaItens.Add(item.nome);
        }*/

        #region SetAntigo
        /*
        for (int i = 0; i < itens.Length; i++)
        {
            itens[i].IDGeral = i;
            itensDicionario.Add(i, itens[i]);
            listaItens.Add(itens[i].nome);

            switch (itens[i].tipoItem)
            {
                case TipoItem.Ferramenta:
                    itens[i].IDLocal = ferramentasList.Count;
                    Ferramenta ferramenta = new Ferramenta()
                    {
                        nome = itens[i].nome,
                        nomeReal = itens[i].nomeReal,
                        IDLocal = ferramentasList.Count,
                        IDGeral = i,
                        imagem = itens[i].imagem,
                        dano = itens[i].quantidade,
                        //interacoes = itens[i].interacoes
                    };
                    ferramentasList.Add(ferramenta);
                    break;
                case TipoItem.Comestivel:
                    itens[i].IDLocal = comestiveisList.Count;
                    Comestivel comestivel = new Comestivel()
                    {
                        nome = itens[i].nome,
                        nomeReal = itens[i].nomeReal,
                        IDLocal = comestiveisList.Count,
                        IDGeral = i,
                        imagem = itens[i].imagem,
                        vitais = itens[i].vitais
                    };
                    comestiveisList.Add(comestivel);
                    break;
                case TipoItem.Recurso:
                    itens[i].IDLocal = recursosList.Count;
                    Recurso recurso = new Recurso()
                    {
                        nome = itens[i].nome,
                        nomeReal = itens[i].nomeReal,
                        IDLocal = recursosList.Count,
                        IDGeral = i,
                        quantidade = itens[i].quantidade,
                        imagem = itens[i].imagem
                    };
                    recursosList.Add(recurso);
                    break;
                case TipoItem.Material:
                    itens[i].IDLocal = materiaisList.Count;
                    Material material = new Material()
                    {
                        nome = itens[i].nome,
                        nomeReal = itens[i].nomeReal,
                        IDLocal = materiaisList.Count,
                        IDGeral = i,
                        imagem = itens[i].imagem
                    };
                    materiaisList.Add(material);
                    break;
                default:
                    break;
            }

            itens[i].IDLocal = i;
        }
        */
        #endregion

        #region Ferramentas
        // PREENCHER FERRAMENTAS
        ferramentasDicionario = new Dictionary<int, Ferramenta>();
        ferramentasDicionario.Clear();
        for (int i = 0; i < ferramentas.Length; i++)
        {
            ferramentasDicionario.Add(i, ferramentas[i]);
            listaFerramentas.Add(ferramentas[i].nome);
        }
        #endregion

        #region Comestiveis
        // PREENCHER COMESTIVEIS
        comestiveisDicionario = new Dictionary<int, Comestivel>();
        comestiveisDicionario.Clear();
        for (int i = 0; i < comestiveis.Length; i++)
        {
            comestiveisDicionario.Add(i, comestiveis[i]);
            listaComestiveis.Add(comestiveis[i].nome);
        }
        #endregion

        #region Recursos
        // PREENCHER RECURSOS
        recursosDicionario = new Dictionary<int, Recurso>();
        recursosDicionario.Clear();
        for (int i = 0; i < recursos.Length; i++)
        {
            recursosDicionario.Add(i, recursos[i]);
            listaRecursos.Add(recursos[i].nome);
        }
        #endregion

        #region Materiais
        // PREENCHER MATERIAIS
        materiaisDicionario = new Dictionary<int, Material>();
        materiaisDicionario.Clear();
        for (int i = 0; i < materiais.Length; i++)
        {
            materiaisDicionario.Add(i, materiais[i]);
            listaMateriais.Add(materiais[i].nome);
        }
        #endregion

        #region Construcoes
        // PREENCHER CONSTRUCOES
        construcoesDicionario = new Dictionary<int, Construcao>();
        construcoesDicionario.Clear();
        for (int i = 0; i < construcoes.Length; i++)
        {
            construcoesDicionario.Add(i, construcoes[i]);
            listaConstrucoes.Add(construcoes[i].nome);
        }
        #endregion

        /*#region Mesas
        // PREENCHER FERRAMENTAS
        mesasDicionario = new Dictionary<int, Mesa>();
        mesasDicionario.Clear();
        for (int i = 0; i < mesas.Length; i++)
        {
            mesasDicionario.Add(i, mesas[i]);
            listaMesas.Add(mesas[i].nome);
        }
        #endregion*/

        GenerateEnum.Generate(listaFerramentas, "FerramentaNome", false); // faz bool false pq a bool true ta cagando a situacao
        GenerateEnum.Generate(listaComestiveis, "ComestivelNome", false);
        GenerateEnum.Generate(listaRecursos, "RecursoNome", false);
        GenerateEnum.Generate(listaMateriais, "MaterialNome", false);
        GenerateEnum.Generate(listaConstrucoes, "ConstrucaoNome", false);
        //GenerateEnum.Generate(listaMesas, "MesaNome", false);

        GenerateEnum.Generate(listaItens, "ItemNome", true);

        #region Vitais
        // PREENCHER FERRAMENTAS
        vitaisDicionario = new Dictionary<int, Vital>();
        vitaisDicionario.Clear();
        List<string> listaVitais = new List<string>();
        for (int i = 0; i < vitais.Length; i++)
        {
            vitais[i].ID = i;
            vitaisDicionario.Add(i, vitais[i]);
            listaVitais.Add(vitais[i].nome);
        }
        #endregion

        GenerateClass.Generate(listaVitais, "Vitais", true);
        GenerateClass.Generate(listaMateriais, "Materiais",false);
    }

        /*public void PrintarArmas()
        {
            if (tipoArmasDicionario == null)
            {
                Debug.LogError("O DICIONARIO ESTÁ VAZIO. ATUALIZE-O");
                return;
            }
            Debug.Log("TOTAL DE " + armasDicionario.Count + " ARMAS");

            for (int i = 0; i < tipoArmasDicionario.Count; i++)
            {
                var tipo = tipoArmasDicionario[i];
                Debug.Log("[" + i + "] " + tipo.nome);

                for (int j = 0; j < tipo.armas.Length; j++)
                {
                    var arma = tipo.armas[j];
                    Debug.Log("[" + tipo.nome + "] " + arma.nome + " [ID " + arma.ID + "]");
                }
            }
        }*/

    public void Awake()
    {
        Preencher();
    }

    public Sprite ItemSprite(ItemNome? item)
    {
        if (item == null) return null;
        return itensDicionario[(int)item].imagem;
    }
    public string ItemNome(ItemNome? item)
    {
        if (item == null) return null;
        return itensDicionario[(int)item].nomeReal;
    }
    public Item Item(ItemNome? item)
    {
        if (item == null) return null;
        return itensDicionario[(int)item];
    }

    
    public Sprite FerramentaSprite(FerramentaNome ferramenta)
    {
        return ferramentasDicionario[(int)ferramenta].imagem;
    }
    public string FerramentaNome(FerramentaNome ferramenta)
    {
        return ferramentasDicionario[(int)ferramenta].nomeReal;
    }
    public Ferramenta Ferramenta(FerramentaNome ferramenta)
    {
        return ferramentasDicionario[(int)ferramenta];
    }

    /*
    public Sprite ComestivelSprite(ComestivelNome comestivel)
    {
        return comestiveisDicionario[(int)comestivel].imagem;
    }
    public string ComestivelNome(ComestivelNome comestivel)
    {
        return comestiveisDicionario[(int)comestivel].nomeReal;
    }
    public Comestivel Comestivel(ComestivelNome comestivel)
    {
        return comestiveisDicionario[(int)comestivel];
    }
    public Sprite RecursoSprite(RecursoNome recurso)
    {
        return recursosDicionario[(int)recurso].imagem;
    }
    public string RecursoNome(RecursoNome recurso)
    {
        return recursosDicionario[(int)recurso].nomeReal;
    }
    public Recurso Recurso(RecursoNome recurso)
    {
        return recursosDicionario[(int)recurso];
    }*/
}

public class GenerateEnum
{
    static List<string> listaBase;
    static string nomeEnumBase;
    static bool gerarEnumComClasse;
    public static void Generate(List<string> lista, string nomeEnum, bool gerarClasse)
    {
        listaBase = lista;
        nomeEnumBase = nomeEnum;
        gerarEnumComClasse = gerarClasse;
        GenerateEnumNow();
    }

    //[MenuItem("Tools/GenerateEnum")]
    public static void GenerateEnumNow()
    {
        string enumName = nomeEnumBase;
        string[] enumEntries = listaBase.ToArray();
        string filePathAndName = "Assets/Scripts/Sistemas/" + enumName + ".cs"; //The folder Assets/Scripts/Combate/ is expected to exist

        using (StreamWriter streamWriter = new StreamWriter(filePathAndName))
        {
            if (gerarEnumComClasse)
            {
                streamWriter.WriteLine("");
                streamWriter.WriteLine("using UnityEngine;");

                streamWriter.WriteLine("[System.Serializable]");
                streamWriter.WriteLine("public class " + enumName + "Quantidade");
                streamWriter.WriteLine("{");
                //for (int i = 0; i < enumEntries.Length; i++)
                //{
                    streamWriter.WriteLine("\t" + "[SerializeField] public " + enumName + " " + enumName.Replace(" ", string.Empty).ToLower() + ";");
                    streamWriter.WriteLine("\t" + "[SerializeField] public int quantidade;");
                //}
                streamWriter.WriteLine("}");
            }

            streamWriter.WriteLine("public enum " + enumName);
            streamWriter.WriteLine("{");
            //streamWriter.Write("\t" + "Nenhum,");

            for (int i = 0; i < enumEntries.Length; i++)
            {
                streamWriter.Write("\t" + enumEntries[i].Replace(" ", string.Empty));

                if (i < enumEntries.Length - 1)
                    streamWriter.WriteLine(",");
                else
                    streamWriter.WriteLine("\t");
            }
            streamWriter.WriteLine("}");

        }
       // AssetDatabase.Refresh();
    }  
}
public class GenerateClass
{
    static List<string> listaBase;
    static string nomeClasseBase;
    static bool usarFloat;
    public static void Generate(List<string> lista, string nomeClasse, bool eFloat)
    {
        listaBase = lista;
        nomeClasseBase = nomeClasse;
        usarFloat = eFloat;
        GenerateEnumNow();
    }

   // [MenuItem("Tools/GenerateEnum")]
    public static void GenerateEnumNow()
    {
        string className = nomeClasseBase;
        string[] arrayEntries = listaBase.ToArray();
        string filePathAndName = "Assets/Scripts/Sistemas/" + className + ".cs"; //The folder Assets/Scripts/Combate/ is expected to exist

        using (StreamWriter streamWriter = new StreamWriter(filePathAndName))
        {
            streamWriter.WriteLine("using UnityEngine;");

            streamWriter.WriteLine("");

            streamWriter.WriteLine("[System.Serializable]");
            streamWriter.WriteLine("public class " + className);
            streamWriter.WriteLine("{");

            if(!usarFloat)
            for (int i = 0; i < arrayEntries.Length; i++)
            {
                string nameVar = arrayEntries[i].Replace(" ", string.Empty).ToLower();
                streamWriter.Write("\t" + "[SerializeField] public int " + nameVar);
                streamWriter.WriteLine(";");
            }
            else
                for (int i = 0; i < arrayEntries.Length; i++)
                {
                    string nameVar = arrayEntries[i].Replace(" ", string.Empty).ToLower();
                    streamWriter.Write("\t" + "[SerializeField] public float " + nameVar);
                    streamWriter.WriteLine(";");
                }

            streamWriter.WriteLine("}");
        }
        //AssetDatabase.Refresh();
    }
}

//[CustomEditor(typeof(Itens))]
/*public class ObjectBuilderEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        Itens itens = (Itens)target;

        GUILayout.Space(30);
        GUILayout.Label("ATUALIZAR DICIONARIO", EditorStyles.boldLabel);
        GUILayout.Space(5);

        if (GUILayout.Button("ATUALIZAR"))
        {
            itens.Preencher();
            Debug.Log("DICIONÁRIO ATUALIZADO");
        }
    }
}*/