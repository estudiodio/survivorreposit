﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Ciclo24H : MonoBehaviour
{
    public static Ciclo24H Instance;
    [Range(1, 30)]
    public float duracaoDoDiaMinutos; //1 min = 60 seg
    float duracaoFixedUpdate;
    public Light sol;
    public Light lua;
    public GameObject parentSolLua;
    public Text diaCountT;
    public bool dia;
    public int diaCount = 0;

    private void Awake()
    {
        Instance = this;
        duracaoFixedUpdate = 360 / (duracaoDoDiaMinutos * 60) / 50;
        StartCoroutine(DiaPassou());
    }

    IEnumerator DiaPassou()
    {
        yield return new WaitForSeconds(duracaoDoDiaMinutos * 60);
        diaCount++;
        diaCountT.text = "Dia " + diaCount;
        StartCoroutine(DiaPassou());
        yield return null;
    }
    void FixedUpdate()
    {
        parentSolLua.transform.RotateAround(Vector3.zero, Vector3.right, duracaoFixedUpdate);
        float rotation = parentSolLua.transform.localEulerAngles.x;
        dia = (rotation > 180 && rotation < 350);
        sol.transform.LookAt(Vector3.zero);
        lua.transform.LookAt(Vector3.zero);
    }
}
