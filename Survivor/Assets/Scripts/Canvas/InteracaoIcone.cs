﻿using UnityEngine;
using UnityEngine.UI;

public class InteracaoIcone : MonoBehaviour
{
    public static InteracaoIcone Instance;
    public Image icone;
    public Vector3 offset;
    //Player player;

    void Awake()
    {
        #region Instance
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);
        #endregion
        //player = Player.Instance;
    }

    public void SetInteracao(Sprite imagem, Vector3 posicao)
    {
        icone.sprite = imagem;
        transform.position = posicao;

        if (imagem != null) gameObject.SetActive(true);
        else gameObject.SetActive(false);
    }
}