﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

//[RequireComponent(typeof(OnMouseOverGUI))]
public class ItemCanvas : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler, IPointerUpHandler
{
    [HideInInspector] public Inventario inventario; //inventario do qual ele faz parte
    public Item item;
    public Text nome;
    public Image fundoImagem;
    public Image highlightImagem;
    public Image highlightImagem2;
    public Image imagem;
    public Image quantidadeBack;
    public Text quantidade;
    public Image imagemTipoMunicao;

    public void Iniciar()
    {
        if (inventario.resetarNoInicio)
        {
            item = null; //se for ter save de inventario, remove essa linha
        }

        Atualizar();

        HighlightMouseOver(false);
    }

    public void AtualizarNullFake() //qnd vc tenta arrastar um item de um slot pro outro, ele vai fakear que apagou o item do slot q vc pegou, mas só visualmente
    {
        SetNome(null);
        SetQuantidade(null);
        SetIcone(null, null);
        SetIconeMunicao(null, null);
    } // finge que removeu o item

    public void SetNome(string text)
    {
        if (nome)
        {
            nome.text = text;
            if (text != null)
                nome.enabled = true;
            else
                nome.enabled = false;
        }
    }
    public void SetQuantidade(int? qnt)
    {
        if (quantidade)
        {
            quantidade.text = qnt.ToString();
            if (qnt != null)
                quantidade.enabled = true;
            else
                quantidade.enabled = false;
        }

        if (quantidadeBack)
        {
            if (qnt != null)
                quantidadeBack.enabled = true;
            else
                quantidadeBack.enabled = false;
        }
    }
    public void SetIcone(Sprite sprite, Color? cor)
    {
        if (cor == null) cor = Color.white;
        if (imagem)
        {
            imagem.sprite = sprite;
            imagem.color = cor ?? Color.white;

            if (sprite != null)
                imagem.enabled = true;
            else
                imagem.enabled = false;
        }
    }

    public void SetIconeMunicao(Sprite sprite, Color? cor)
    {
        if (cor == null) cor = Color.white;
        if (imagemTipoMunicao)
        {
            imagemTipoMunicao.sprite = sprite;
            imagemTipoMunicao.color = cor ?? Color.white;

            if (sprite != null)
            {
                imagemTipoMunicao.enabled = true;
                imagemTipoMunicao.gameObject.SetActive(true);
            }
            else
            {
                imagemTipoMunicao.enabled = false;
                imagemTipoMunicao.gameObject.SetActive(false);
            }
        }
    }
    public void Atualizar()
    {
        /*if (itemNovo == null) print("nenhum Item");
        else print(itemNovo.nome);*/
        item = inventario.objetosInventario[gameObject.transform.GetSiblingIndex()];

        if (item != null)
        {
            SetIcone(item.imagem, null);
            SetNome(item.nomeReal);
            if (item.tipoItem == TipoItem.Recurso)
                SetQuantidade(item.quantidade);
            else
                SetQuantidade(null);

            if (item.tipoItem == TipoItem.Ferramenta && !item.corpoACorpo)
                SetIconeMunicao(Itens.Instance.recursosDicionario[(int)item.tipoMunicao].imagem, null);
            else
                SetIconeMunicao(null, null);
        }
        else
        {
            SetIcone(null, null);
            SetNome(null);
            SetQuantidade(null);
            SetIconeMunicao(null, null);
        }
    }

    public void AtualizarFake(Item item) //atualiza de acordo com item mostrado e nao o real oficial do inventario
    {
        //print("atualizarFake");
        if (item != null)
        {
            SetIcone(item.imagem, InventarioCraftResultado.Instance.corArmaPreview);
            SetNome(item.nomeReal);
            if (item.tipoItem == TipoItem.Recurso)
                SetQuantidade(item.quantidade);
            else
                SetQuantidade(null);

            if (item.tipoItem == TipoItem.Ferramenta && !item.corpoACorpo)
                SetIconeMunicao(Itens.Instance.recursosDicionario[(int)item.tipoMunicao].imagem, InventarioCraftResultado.Instance.corArmaPreview);
            else
                SetIconeMunicao(null, null);
        }
        else
        {
            SetIcone(null, null);
            SetNome(null);
            SetQuantidade(null);
            SetIconeMunicao(null, null);
        }
    } // inventario craft resultado. finge que criou o item

    public void HighlightMouseOver(bool enabled)
    {
        highlightImagem.enabled = enabled;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        Inventario.SelecionarSlotDrop(this);
        HighlightMouseOver(true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        Inventario.SelecionarSlotDrop(null);
        HighlightMouseOver(false);
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
             Inventario.Selecionar(inventario, transform.GetSiblingIndex());
    }
    public void OnPointerUp(PointerEventData eventData)
    {
        Inventario.Soltar();
    }
}
