﻿using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class AtributoCanvas
{
    public string nome;
    public Image fill;
    public Text text;
    public Text textMax;
}
// vida, energia, fome, sede, dano

public class CanvasManager : MonoBehaviour
{
    public static CanvasManager Instance;
    public HUD hud;
    public Pause pause;
    public GameObject morte;
    public Image mira;
    public GameObject iconeInteracao;
    public Image interagindoFill;
    public GameObject Detect;
    public GameObject TelaDeDano;

    void Awake()
    {
        //print("iniciar");
        Instance = this;
        hud.Iniciar();
        pause.Iniciar();
        iconeInteracao.gameObject.SetActive(false);
        Player.Instance.iconeInteracao = iconeInteracao;
        Player.Instance.tempoInteragindoFill = interagindoFill;
    } 
}
