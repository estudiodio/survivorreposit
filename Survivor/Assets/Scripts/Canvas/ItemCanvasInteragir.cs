﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class ItemCanvasInteragir : MonoBehaviour, IPointerEnterHandler, IPointerDownHandler, IPointerExitHandler
{
    public Image highlightImagem;
    public Text textInteracao;

    public void HighlightMouseOver(bool enabled)
    {
        highlightImagem.enabled = enabled;
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
        HighlightMouseOver(true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        HighlightMouseOver(false);
        GetComponent<Image>().enabled = false;
        textInteracao.enabled = false;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            Inventario.ConsumirComestivel();
            Inventario.PosicionarConstrucao();
        }
    }
}
