﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ItemCanvasDrop : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public void OnPointerEnter(PointerEventData eventData)
    {
        Inventario.SelecionarSlotDropChao(true);
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        Inventario.SelecionarSlotDropChao(false);
    }
}
