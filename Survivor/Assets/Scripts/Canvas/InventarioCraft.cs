﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventarioCraft : Inventario
{
    public static InventarioCraft Instance;
    public List<List<Item>> receitas = new List<List<Item>>();
    public List<Item> receitasResultado = new List<Item>();

    Item resultante = null;

    public override void Iniciar()
    {
        #region Instance
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);
        #endregion
        base.Iniciar();

        PreencherReceitas();

        /*for (int i = 0; i < receitas.Count; i++)
        {
            string receita = "";
            for (int j = 0; j < receitas[i].Count; j++)
            {
                if (receitas[i][j] != null)
                    receita += " " +receitas[i][j].nome;
                else
                    receita += " null";
            }
            print(i + "    " + receita);
        }*/

        //ReceitaCanvas.Instance.Iniciar();
    }

    /*void PreencherReceitas()
    {
        for (int i = 0; i < Itens.Instance.itensDicionario.Count; i++)//roda todos os itens do game
        {
            if (Itens.Instance.itensDicionario[i].ingredientes == null || Itens.Instance.itensDicionario[i].ingredientes.Count == 0)
                continue; //se o item nao tiver receita, nem precisa

            List<Item> itensReceita = new List<Item>();//se tiver receita, fazer a listinha de itens da receita
            for (int j = 0; j < Itens.Instance.itensDicionario[i].ingredientes.Count; j++)
            {
                for (int k = 0; k < Itens.Instance.itensDicionario[i].ingredientes[j].quantidade; k++)
                {
                    itensReceita.Add(Itens.Instance.Item(Itens.Instance.itensDicionario[i].ingredientes[j].itemnome));
                }
            }
            receitas.Add(itensReceita);
            receitasResultado.Add(Itens.Instance.itensDicionario[i]);
        }
    }*/


    public override void SwitchEnable()
    {
        parentInventario.SetActive(!parentInventario.activeSelf);

        if (!Enabled())
        {
            ResetarSlotsSelecionados();
            DesligarHighlightsSlots();
            ReceitaCanvas.Instance.Enable(false);
        }

    }
    public override void Enable(bool enabled)
    {
        parentInventario.SetActive(enabled);
        if (!Enabled())
        {
            ResetarSlotsSelecionados();
            DesligarHighlightsSlots();
            ReceitaCanvas.Instance.Enable(false);
        }
    }
    void PreencherReceitas()
    {
        for (int i = 0; i < Itens.Instance.itensDicionario.Count; i++)//roda todos os itens do game
        {
            if (Itens.Instance.itensDicionario[i].ingredientes == null || Itens.Instance.itensDicionario[i].ingredientes.Count == 0)
                continue; //se o item nao tiver receita, nem precisa

            List<Item> itensReceita = new List<Item>();//se tiver receita, fazer a listinha de itens da receita
            for (int j = 0; j < Itens.Instance.itensDicionario[i].ingredientes.Count; j++)
            {
                //print(Itens.Instance.itensDicionario[i].nome +" num ingredientes " + Itens.Instance.itensDicionario[i].ingredientes[j].itemnome.ToString() + " x" + Itens.Instance.itensDicionario[i].ingredientes[j].quantidade.ToString());

                if (Itens.Instance.itensDicionario[i].ingredientes[j].quantidade != 0)
                {
                    //print("Item " + Itens.Instance.Item(Itens.Instance.itensDicionario[i].ingredientes[j].itemnome).nomeReal);
                    itensReceita.Add(Itens.Instance.Item(Itens.Instance.itensDicionario[i].ingredientes[j].itemnome));
                }
                else
                {
                    //print("Item null");
                    itensReceita.Add(null);
                }
            }
            string itens = "";
            for (int k = 0; k < itensReceita.Count; k++)
            {
                if (itensReceita[k] != null)
                    itens += "[" + k + "] " + itensReceita[k].nome;
                else
                    itens += "[" + k + "] " + "null";
            }
            //print("[" + i + "] " + itens);

            receitas.Add(itensReceita);
            receitasResultado.Add(Itens.Instance.itensDicionario[i]);

        }
        //print(receitas);
    }
    void AtualizarItemResultante(Item item)
    {
        if (InventarioCraftResultado.Instance.objetosInventario[0] != null) return;
        //else
        //if (InventarioCraftResultado.Instance.objetosInventario[0].nome != null) return; //se ja tiver item la, nem mexe
        //print("atualizarResultante");

        resultante = ChecarReceita(objetosInventarioList());

        Item[] itemArray = new Item[1];
        itemArray[0] = item;
        if (item != null)
            resultante = item;
        else resultante = null;
        InventarioCraftResultado.Instance.SetInventarioFake(itemArray);
    }

    public List<Item> objetosInventarioList()
    {
        List<Item> itensInventario = new List<Item>();
        string itens = "";
        for (int i = 0; i < objetosInventario.Length; i++)
        {
                itensInventario.Add(objetosInventario[i]);
            if (objetosInventario[i] != null)
                itens += " -- [" + i + "]" + " " + objetosInventario[i].nome;
            else
                itens += " -- [" + i + "]" + " " + "null";
            //print(itens[i]);
        }
        print(itens);
        return itensInventario;
    }
    /*public List<Item> objetosInventarioList()
    {
        List<Item> itensInventario = new List<Item>();
        string itens = "";
        for (int i = 0; i < objetosInventario.Length; i++)
        {
            if (objetosInventario[i] != null)
            {
                itensInventario.Add(objetosInventario[i]);
                itens += " -- [" + i + "]" + " " + objetosInventario[i].nome;
                //print("[" + i + "]" + " " + objetosInventario[i].nome);
            }
        }
        //print(itens);
        return itensInventario;
    }*/
    Item ChecarReceita(List<Item> listaItens)
    {
        List<Item> atual = objetosInventarioList();
        for (int i = 0; i < receitas.Count; i++)
        {
            for (int j = 0; j < receitas[i].Count; j++)
            {
                if (receitas[i][j] == null && atual[j] == null)
                {
                    if (j == receitas[i].Count - 1)
                    {
                        return receitasResultado[i];
                    }

                    continue;
                }
                if (receitas[i][j] == null && atual[j] != null) break;
                if (receitas[i][j] != null && atual[j] == null) break;
                if (receitas[i][j].nome != atual[j].nome) break;

                if (j == receitas[i].Count - 1)
                {
                    return receitasResultado[i];
                }
            }
        }

        return null;
        /*for (int i = 0; i < receitas.Count; i++)
        {
            print("CHECANDO " + receitasResultado[i].nome);

            print("ATUAL " + "  =========  " + receita);
            string RECEITA2 = "";
            for (int j = 0; j < receitas[i].Count; j++)
            {

                    if (receitas[i][j] != null)
                        RECEITA2 += " " + receitas[i][j].nome;
                else
                    RECEITA2 += " null";
            }
            print("RECEITA DE " + receitasResultado[i].nome + "  =========  " + RECEITA2);


            if (atual == receitas[i])
            {
                print("TODOS OS INGREDIENTES PRONTOS PARA CRIAR " + receitasResultado[i].nome);
                return receitasResultado[i];
            }
            else
                print("FALTOU ITEM PRA " + receitasResultado[i].nome);
        }*/
    }
    /*Item ChecarReceita(List<Item> listaItens)
    {
        for (int i = 0; i < receitas.Count; i++)
        {
            for (int j = 0; j < receitas[i].Count; j++)
            {
                var itemReceita = receitas[i].FindAll(s => s.Equals(receitas[i][j]));
                var itemInventario = listaItens.FindAll(s => s.Equals(receitas[i][j]));

                //print("LOOP   " + j + "/ " + receitas[i].Count);
                //print("[ RECEITA ]   " + "[" + i + "]" + "[" + j + "]" + " " + receitas[i][j].nome + " x" + itemReceita.Count);
                //print("[ INVENTARIO ]   " + receitas[i][j].nome + " x" + itemInventario.Count);

                if (itemInventario.Count < itemReceita.Count) //se o inventario n tiver o num min de itens daquele
                {
                    //print("FALHOU EM " + receitas[i][j].nome + " x" + itemInventario.Count);
                    break;
                }
                if(j >= receitas[i].Count-1)
                {
                    //print("TODOS OS INGREDIENTES PRONTOS PARA CRIAR " + receitasResultado[i].nome);
                    return receitasResultado[i];
                }
            }
        }
        return null;
    }*/

   /* List<int> IndexesApagar(List<Item> listaItens)
    {
        if (resultante == null) return null;

        for (int i = 0; i < receitas.Count; i++)
        {
            if (receitasResultado[i] != resultante) continue; //procura a receita do item q deveria ser resultante

            List<int> indexesTemp = new List<int> ();

            for (int j = 0; j < receitas[i].Count; j++)
            {
                var itemReceita = receitas[i].FindAll(s => s.Equals(receitas[i][j]));
                var itemInventario = listaItens.FindAll(s => s.Equals(receitas[i][j]));

                if (itemInventario.Count < itemReceita.Count) return null;

                for (int k = 0; k < objetosInventario.Length; k++)
                {
                    if(objetosInventario[k] != null)
                    if (objetosInventario[k] == receitas[i][j] && !indexesTemp.Contains(k))
                    {
                        //print("INDEX [" + k + "] " + objetosInventario[k].nome + " == " + receitas[i][j].nome);
                        indexesTemp.Add(k);
                            break;
                    }
                }

                if (j >= receitas[i].Count - 1)
                    return indexesTemp;
            }
        }
        return null;
    } // nao ta sendo usado*/

    public void CriarItemResultante() //chamado no botao de criar
    {
        if (resultante == null || resultante.nome == "") return;
        if (InventarioCraftResultado.Instance.objetosInventario[0] != null) return;
        //List<int> itensApagar = IndexesApagar(objetosInventarioList());
        //if (itensApagar == null) return;

        Item[] itemArray = new Item[1];
        itemArray[0] = resultante;
        InventarioCraftResultado.Instance.SetInventario(itemArray);

        //for (int i = 0; i < itensApagar.Count; i++)
        //{
        //    ApagarItem(itensApagar[i]);
        //}

        for (int i = 0; i < objetosInventario.Length; i++)
        {
            ApagarItem(i);
        }

        AtualizarItemResultante(ChecarReceita(objetosInventarioList()));
    }



    public override void SetInventario(Item[] itens)
    {
        objetosInventario = new Item[transform.childCount];

        for (int i = 0; i < objetosInventario.Length; i++)
        {
            if (i < itens.Length)
                if (CheckInventario(this, itens[i]) == false) // se o inventario nao suporta esse item, continua o baile
                    continue;
                else //se ele aceita, preenche
                {
                    if (i < itens.Length)
                        objetosInventario[i] = itens[i];
                    else
                        objetosInventario[i] = null;

                    canvasInventario[i].Atualizar();
                    AtualizarItemResultante(ChecarReceita(objetosInventarioList()));
                }
        }
    }

    public override bool AdicionarItem(Item novoItem, ItemGame sendoUsado)
    {
        if (CheckInventario(this, novoItem) == false)
            return false;

        for (int i = 0; i < objetosInventario.Length; i++)
        {
            if (objetosInventario[i] == null)
            {
                objetosInventario[i] = novoItem;
                canvasInventario[i].Atualizar();
                AtualizarItemResultante(ChecarReceita(objetosInventarioList()));
                return true;
            }
        }

        return false;
    }

    public override bool AdicionarItem(Item novoItem, int index, bool ignorarNull)
    {
        if (novoItem != null)
            if (CheckInventario(this, novoItem) == false) return false;

        if (!ignorarNull && objetosInventario[index] == null)
        {
            return false;
        }

        objetosInventario[index] = novoItem;
        canvasInventario[index].Atualizar();
        AtualizarItemResultante(ChecarReceita(objetosInventarioList()));
        return true;
    }

    public override void ApagarItem(int indexItem) //evitar dar errado pq tem 2 itens iguais por ex
    {
        objetosInventario[indexItem] = null;
        canvasInventario[indexItem].Atualizar();
        AtualizarItemResultante(ChecarReceita(objetosInventarioList()));
    }

}