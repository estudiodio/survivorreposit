﻿using UnityEngine;

public class InventarioLoot : Inventario
{
    public static InventarioLoot Instance;
    public static LootBox atual; //o loot box que ele tem aberto

    public override void Iniciar()
    {
        base.Iniciar();
        #region Instance
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);
        #endregion
    }

    public override bool AdicionarItem(Item novoItem, ItemGame sendoUsado)
    {
        if (CheckInventario(this, novoItem) == false)
            return false;

        for (int i = 0; i < objetosInventario.Length; i++)
        {
            if (objetosInventario[i] == null)
            {
                objetosInventario[i] = novoItem;
                    canvasInventario[i].Atualizar();
                atual.SetItens(objetosInventario);
                return true;
            }
        }

        return false;
    }

    public override bool AdicionarItem(Item novoItem, int index, bool ignorarNull)
    {
        if (novoItem != null)
            if (CheckInventario(this, novoItem) == false) return false;

        if (!ignorarNull && objetosInventario[index] == null)
        {
            return false;
        }

        objetosInventario[index] = novoItem;
            canvasInventario[index].Atualizar();
        atual.SetItens(objetosInventario);
        return true;
    }

    public override void ApagarItem(int indexItem) //evitar dar errado pq tem 2 itens iguais por ex
    {
        objetosInventario[indexItem] = null;
        canvasInventario[indexItem].Atualizar();
        atual.SetItens(objetosInventario);
    }
}