﻿using System.Collections;
using UnityEngine;

public class InventarioRapido : Inventario
{
    public static InventarioRapido Instance;
    [SerializeField] protected Item selecionadoUso;
    public float tempoFecharSozinho = 0;
    private int? slotAgora = 0; //nao aplicavel para ordem dos gameObjets de arma, apenas para os slots
    [SerializeField] Sprite marteloSprite;
    [SerializeField] Sprite chaveInglesaSprite;

    public override void Iniciar()
    {
        #region Instance
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);
        #endregion
        base.Iniciar();

        for (int i = 0; i < canvasInventario.Length; i++) // começa com nenhum ligado
        {
            canvasInventario[i].highlightImagem2.enabled = false; 
        }
    }

    private void OnEnable()
    {
        tempoFecharSozinho = 5;
    }

    private void FixedUpdate()
    {
        if (InventarioMochila.Instance.Enabled() && tempoFecharSozinho == 5)
            return;

        if (InventarioMochila.Instance.Enabled() && tempoFecharSozinho != 5)
        {
            tempoFecharSozinho = 5;
            return;
        }

        tempoFecharSozinho -= (0.02f);

        if (tempoFecharSozinho <= 0)
        {
            Enable(false);
        }
    }

    public override void SwitchEnable()
    {
        base.SwitchEnable();
        if (!Enabled())
            tempoFecharSozinho = 5;

    }
    public override void Enable(bool enabled)
    {
        base.Enable(enabled);

        if (!Enabled())
            tempoFecharSozinho = 5;
    }

    public override bool AdicionarItem(Item novoItem, ItemGame sendoUsado)
    {
        if (CheckInventario(this, novoItem) == false)
            return false;

        for (int i = 0; i < objetosInventario.Length; i++)
        {
            if (objetosInventario[i] == null)
            {
                objetosInventario[i] = novoItem;
                canvasInventario[i].Atualizar();
                Instance.Enable(true);
                return true;
            }
        }

        return false;
    }

    public override bool AdicionarItem(Item novoItem, int index, bool ignorarNull)
    {
        if (novoItem != null)
            if (CheckInventario(this, novoItem) == false) return false;

        if (!ignorarNull && objetosInventario[index] == null)
        {
            return false;
        }

        objetosInventario[index] = novoItem;
        canvasInventario[index].Atualizar();
        Instance.Enable(true);
        return true;
    }

    public override void ApagarItem(int indexItem)
    {
        base.ApagarItem(indexItem);
        if (indexItem == slotAgora) //se apagar o item q ele ta segurando
        {
            SelecionarRapido(null, true); // atualiza arma na mao para ''nada''
        }
    }
    public void SelecionarRapido(int? slotFuturo, bool forcar)
    {
        tempoFecharSozinho = 5;

        if (!gameObject.activeSelf)
            gameObject.SetActive(true); // liga o inventario pro player ve oq ta fazendo

        if (!forcar) // troca de arma normal
        {
            if(objetosInventario[slotFuturo.GetValueOrDefault()] != null) // se ele tem arma onde ta clicando
            {
                canvasInventario[slotAgora.GetValueOrDefault()].highlightImagem2.enabled = false;
                canvasInventario[slotFuturo.GetValueOrDefault()].highlightImagem2.enabled = true;

                AtualizarArma(objetosInventario[slotFuturo.GetValueOrDefault()]); //tirar do comment qnd tiver arma
               
                slotAgora = slotFuturo;
            }
            else // se nao tema arma, ele nao troca de arma
            {
                return;
            }
        }
        else //se ele soltou a arma q tinha selecionado, ja q ele é um idiota
        {
            canvasInventario[slotAgora.GetValueOrDefault()].highlightImagem2.enabled = false;

            if (slotFuturo != null)//se tiver arma pra ele selecionar
            {
                canvasInventario[slotFuturo.GetValueOrDefault()].highlightImagem2.enabled = true;
                AtualizarArma(objetosInventario[slotFuturo.GetValueOrDefault()]);
            }
            else
                AtualizarSemArma();

            slotAgora = slotFuturo;
            //AtualizarArma(objetosInventario[slotFuturo.GetValueOrDefault()].IDLocal); aqui fala pra selecionar "arma nenhuma"
        }
    }

    public Item ferramentaAtual = null;

    void AtualizarArma(Item arma)
    {
        //print(arma.nome + " " + arma.quantidade);
        if(arma == null)
        {
            ferramentaAtual = null;
            CG_Vitto.Instance.armaAtual = 0;
            CG_Vitto.Instance.AtualizarArma();
            HUD.Instance.armaNome.text = null;
            HUD.Instance.armaIcone.enabled = false;
            HUD.Instance.armaIcone.sprite = null;
            return;
        }

        ferramentaAtual = arma;

        /*sem arma 0, pistola 1, rifle 2, bastao 3

        //BastaoAtual = TipoBastao.Bastao_1;
        //PistolaAtual = TipoPistola.Desert;
        // RifleAtual = TipoRifle.Ak_47;
        //FerramentaAtual = TipoFerramenta.Martelo;
        //-----------------------------------------------
        //TipoPistola { Desert, USPS }
        //TipoRifle { Ak_47, M4A4, G35G }
        //TipoBastao { Bastao_1, Bastao_2, Bastao_3 }
        //TipoFerramenta { Martelo, ChaveIng }*/

        switch (ferramentaAtual.IDLocal)
        {
            case 0: //Desert
                CG_Vitto.Instance.armaAtual = 1;
                CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola_V>().municao = arma.quantidade;
               // print("SETANDO " + InventarioMochila.Instance.TotalMunicao(arma.tipoMunicao));
                CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola_V>().municaoRestante = InventarioMochila.Instance.totalMunicaoLeve;
              //  print("SETOU " + InventarioMochila.Instance.TotalMunicao(arma.tipoMunicao));
                CG_Vitto.Instance.PistolaAtual = TipoPistola.Desert;
                break;
            case 1: //USPS
                CG_Vitto.Instance.armaAtual = 1;
                CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola_V>().municao = arma.quantidade;
               // print("SETANDO " + InventarioMochila.Instance.TotalMunicao(arma.tipoMunicao));
                CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola_V>().municaoRestante = InventarioMochila.Instance.totalMunicaoLeve;
               // print("SETOU " + InventarioMochila.Instance.TotalMunicao(arma.tipoMunicao));
                CG_Vitto.Instance.PistolaAtual = TipoPistola.USPS;
                break;
            case 2://Ak_47
                CG_Vitto.Instance.armaAtual = 2;
                CG_Vitto.Instance.Rifle.transform.GetChild(1).GetComponent<Rifle_V>().municao = arma.quantidade;
                CG_Vitto.Instance.Rifle.transform.GetChild(1).GetComponent<Rifle_V>().municaoRestante = InventarioMochila.Instance.totalMunicaoMedia;
                CG_Vitto.Instance.RifleAtual = TipoRifle.Ak_47;
                break;
            case 3://M4A4
                CG_Vitto.Instance.armaAtual = 2;
                CG_Vitto.Instance.Rifle.transform.GetChild(1).GetComponent<Rifle_V>().municao = arma.quantidade;
                CG_Vitto.Instance.Rifle.transform.GetChild(1).GetComponent<Rifle_V>().municaoRestante = InventarioMochila.Instance.totalMunicaoMedia;
                CG_Vitto.Instance.RifleAtual = TipoRifle.M4A4;
                break;
            case 4://G35G
                CG_Vitto.Instance.armaAtual = 2;
                CG_Vitto.Instance.Rifle.transform.GetChild(1).GetComponent<Rifle_V>().municao = arma.quantidade;
                CG_Vitto.Instance.Rifle.transform.GetChild(1).GetComponent<Rifle_V>().municaoRestante = InventarioMochila.Instance.totalMunicaoMedia;
                CG_Vitto.Instance.RifleAtual = TipoRifle.G35G;
                break;
            case 5://Bastao_1
                CG_Vitto.Instance.armaAtual = 3;
                //CG_Vitto.Instance.Pistola.GetComponent<Rifle>().Mun = arma.quantidade;
                //CG_Vitto.Instance.Pistola.GetComponent<Pistola>().MunRestante = InventarioMochila.Instance.TotalMunicao(arma.tipoMunicao);
                CG_Vitto.Instance.BastaoAtual = TipoBastao.Bastao_1;
                break;
            case 6://Bastao_2
                CG_Vitto.Instance.armaAtual = 3;
                //CG_Vitto.Instance.Pistola.GetComponent<Rifle>().Mun = arma.quantidade;
                //CG_Vitto.Instance.Pistola.GetComponent<Pistola>().MunRestante = InventarioMochila.Instance.TotalMunicao(arma.tipoMunicao);
                CG_Vitto.Instance.BastaoAtual = TipoBastao.Bastao_2;
                break;
            case 7://Bastao_3
                CG_Vitto.Instance.armaAtual = 3;
                //CG_Vitto.Instance.Pistola.GetComponent<Rifle>().Mun = arma.quantidade;
                //CG_Vitto.Instance.Pistola.GetComponent<Pistola>().MunRestante = InventarioMochila.Instance.TotalMunicao(arma.tipoMunicao);
                CG_Vitto.Instance.BastaoAtual = TipoBastao.Bastao_3;
                break;
            default:
                break;
        }

        CG_Vitto.Instance.AtualizarArma();
        HUD.Instance.armaNome.text = ferramentaAtual.nomeReal;
        HUD.Instance.armaIcone.enabled = true;
        HUD.Instance.armaIcone.sprite = ferramentaAtual.imagem;
        //public Image ImagemAtualArma;
        //public Text currentWeaponText;
        //aqui vai ter escrito pra desligar a arma atual e ligar a arma nova
    }

    public void AtualizarFerramentaConstrucao(int ferramenta)
    {

        //TipoFerramenta { Martelo, ChaveIng }*/ == 4

        switch (ferramentaAtual.IDLocal)
        {
            case 0: //Desert
                CG_Vitto.Instance.armaAtual = 4;
                CG_Vitto.Instance.FerramentaAtual = TipoFerramenta.Martelo;
                HUD.Instance.armaNome.text = "Martelo";
                HUD.Instance.armaIcone.sprite = marteloSprite;
                break;
            case 1: //USPS
                CG_Vitto.Instance.armaAtual = 4;
                CG_Vitto.Instance.FerramentaAtual = TipoFerramenta.ChaveIng;
                HUD.Instance.armaNome.text = "Chave Inglesa";
                HUD.Instance.armaIcone.sprite = chaveInglesaSprite;
                break;            
            default:
                break;
        }

        CG_Vitto.Instance.AtualizarArma();
        HUD.Instance.armaIcone.enabled = true;
    }

    public void AtualizarSemArma()
    {
        CG_Vitto.Instance.armaAtual = 0;
        CG_Vitto.Instance.AtualizarArma();
    }
    public void Atirou()
    {
        ferramentaAtual.quantidade--;
    }
    //armas de fogo
    //direito mira, esquerdo atira, r recarrega, e bota arma noku, f olhar a arma
}