﻿using UnityEngine;

public class InventarioMochila : Inventario
{
    public static InventarioMochila Instance;
    public int totalMunicaoLeve;
    public int totalMunicaoMedia;
    public int totalMunicaoPesada;

    //modificar pra ja ter um int salvo pra cada recurso (os 3 tipos de bala)
    public override void Iniciar()
    {
        #region Instance
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);
        #endregion
        base.Iniciar();

        totalMunicaoLeve = CalcularMunicao(TipoMunicao.Leve);
        totalMunicaoMedia = CalcularMunicao(TipoMunicao.Media);
        totalMunicaoPesada = CalcularMunicao(TipoMunicao.Pesada);

        for (int i = 0; i < objetosInventario.Length; i++)
        {
            objetosInventario[i] = null;
        }
    }

    /*void FixedUpdate()
    {
        print(CalcularMunicao(TipoMunicao.Leve));
    }*/
    public override void SwitchEnable()
    {
        parentInventario.SetActive(!parentInventario.activeSelf);

        if (!Enabled())
        {
            ResetarSlotsSelecionados();
            DesligarHighlightsSlots();
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

    }
    public override void Enable(bool enabled)
    {
        parentInventario.SetActive(enabled);
        if (!Enabled())
        {
            ResetarSlotsSelecionados();
            DesligarHighlightsSlots();
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    public override bool Enabled()
    {
        return parentInventario.activeSelf;
    }

    public int ProcurarMunicao(TipoMunicao tipo, int quantidadePedida)
    {
        int municaoDisponivel = 0;
        //print("MUNICAO RESTANTE TESTE");
        for (int i = 0; i < objetosInventario.Length; i++)
        {
            Item item = objetosInventario[i];
            if (item != null)
                if (item.tipoItem == TipoItem.Recurso && item.tipoMunicao == tipo)
                {
                    if (objetosInventario[i].quantidade <= quantidadePedida) // se o slot procurado vai dar td oq tem disponivel
                    {
                        municaoDisponivel += objetosInventario[i].quantidade;
                        quantidadePedida -= objetosInventario[i].quantidade;
                        objetosInventario[i].quantidade -= municaoDisponivel;
                        //canvasInventario[i].Atualizar(); //atualizar num de balas
                        ApagarItem(i);

                        if (quantidadePedida == 0)
                        {
                            switch (tipo)
                            {
                                case TipoMunicao.Leve:
                                    totalMunicaoLeve -= municaoDisponivel;
                                    break;
                                case TipoMunicao.Media:
                                    totalMunicaoMedia -= municaoDisponivel;
                                    break;
                                case TipoMunicao.Pesada:
                                    totalMunicaoPesada -= municaoDisponivel;
                                    break;
                                default:
                                    break;
                            }
                            //print("municao leve " + municaoDisponivel);
                            return municaoDisponivel;
                        }
                    }
                    else //se ele vai dar oq tem e ainda vai sobrar dele
                    {
                        municaoDisponivel += quantidadePedida;
                        quantidadePedida -= quantidadePedida;
                        objetosInventario[i].quantidade -= municaoDisponivel;
                        canvasInventario[i].Atualizar(); //atualizar num de balas
                        if (quantidadePedida == 0)
                        {
                            switch (tipo)
                            {
                                case TipoMunicao.Leve:
                                    totalMunicaoLeve -= municaoDisponivel;
                                    break;
                                case TipoMunicao.Media:
                                    totalMunicaoMedia -= municaoDisponivel;
                                    break;
                                case TipoMunicao.Pesada:
                                    totalMunicaoPesada -= municaoDisponivel;
                                    break;
                                default:
                                    break;
                            }
                            //print("municao leve " + municaoDisponivel);
                            return municaoDisponivel;
                        }

                    }
                }
        }
        switch (tipo)
        {
            case TipoMunicao.Leve:
                totalMunicaoLeve -= municaoDisponivel;
                break;
            case TipoMunicao.Media:
                totalMunicaoMedia -= municaoDisponivel;
                break;
            case TipoMunicao.Pesada:
                totalMunicaoPesada -= municaoDisponivel;
                break;
            default:
                break;
        }
        //print("municao leve " + municaoDisponivel);
        return municaoDisponivel;
    }

    public int CalcularMunicao(TipoMunicao tipo)
    {
        int municaoDisponivel = 0;
        for (int i = 0; i < objetosInventario.Length; i++)
        {
            Item item = objetosInventario[i];
            if (item != null)
                if (item.tipoItem == TipoItem.Recurso && item.tipoMunicao == tipo)
                {
                    municaoDisponivel += objetosInventario[i].quantidade;
                }
        }
        //print(municaoDisponivel);
        return municaoDisponivel;
    }

    public int TotalMunicao(TipoMunicao tipo)
    {
        switch (tipo)
        {
            case TipoMunicao.Leve:
                return totalMunicaoLeve;
            case TipoMunicao.Media:
                return totalMunicaoMedia;
            case TipoMunicao.Pesada:
                return totalMunicaoPesada;
            default:
                break;
        }
        return 0;
    }

    public override bool AdicionarItem(Item novoItem, ItemGame sendoUsado)
    {
        //print(novoItem.nome);
        if (CheckInventario(this, novoItem) == false)
            return false;

        //print(novoItem.nome + " -- " + novoItem.tipoItem + " -- " + novoItem.quantidade);
        if (novoItem.tipoItem == TipoItem.Recurso) //tenta colocar municao onde ja tem
        {
            for (int i = 0; i < objetosInventario.Length; i++)
            {
                if (novoItem.quantidade <= 0) return true;

                if (objetosInventario[i] != null)
                if (objetosInventario[i] != null || objetosInventario[i].nome != null)
                    if (objetosInventario[i].tipoItem == TipoItem.Recurso)
                        if (objetosInventario[i].tipoMunicao == novoItem.tipoMunicao) // se o tipo de municao for o msm
                        {
                            if (objetosInventario[i].quantidade < Itens.Instance.quantidadeMaxBalasSlot) //se tem menos quantidade do que o maximo
                            {

                                //print("c");
                                int quantidadeQuePodeReceber = Itens.Instance.quantidadeMaxBalasSlot - objetosInventario[i].quantidade;
                                if (novoItem.quantidade <= quantidadeQuePodeReceber)
                                {
                                    objetosInventario[i].quantidade += novoItem.quantidade;
                                    canvasInventario[i].Atualizar();

                                    if (novoItem.tipoItem == TipoItem.Recurso)
                                        switch (novoItem.tipoMunicao)
                                        {
                                            case TipoMunicao.Leve:
                                                totalMunicaoLeve += novoItem.quantidade;
                                                CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola_V>().municaoRestante = totalMunicaoLeve;
                                                break;
                                            case TipoMunicao.Media:
                                                totalMunicaoMedia += novoItem.quantidade;
                                                CG_Vitto.Instance.Rifle.transform.GetChild(1).GetComponent<Rifle_V>().municaoRestante = totalMunicaoMedia;
                                                break;
                                            case TipoMunicao.Pesada:
                                                totalMunicaoPesada += novoItem.quantidade;
                                                //CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola>().municaoRestante = totalMunicaoPesada;
                                                break;
                                            default:
                                                break;
                                        }

                                   // print("d");
                                    return true;
                                }
                                else
                                {
                                    objetosInventario[i].quantidade += quantidadeQuePodeReceber;
                                    canvasInventario[i].Atualizar();

                                    novoItem.quantidade -= quantidadeQuePodeReceber;
                                }
                            }
                        }
            }
        }

        //print("ainda tem muniççao " + novoItem.quantidade);
        for (int i = 0; i < objetosInventario.Length; i++)
        {
            if (objetosInventario[i] == null)
            if (objetosInventario[i] == null  || objetosInventario[i].nome == null)
            {
                objetosInventario[i] = novoItem;
                canvasInventario[i].Atualizar();

               // print("add " + novoItem.tipoItem);
                if (novoItem.tipoItem == TipoItem.Recurso)
                    switch (novoItem.tipoMunicao)
                    {
                        case TipoMunicao.Leve:
                            totalMunicaoLeve += novoItem.quantidade;
                            CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola_V>().municaoRestante = totalMunicaoLeve;
                            break;
                        case TipoMunicao.Media:
                            totalMunicaoMedia += novoItem.quantidade;
                            CG_Vitto.Instance.Rifle.transform.GetChild(1).GetComponent<Rifle_V>().municaoRestante = totalMunicaoMedia;
                            break;
                        case TipoMunicao.Pesada:
                            totalMunicaoPesada += novoItem.quantidade;
                            //CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola>().municaoRestante = totalMunicaoPesada;
                            break;
                        default:
                            break;
                    }
                return true;
            }
        }

        if (novoItem.tipoItem == TipoItem.Recurso) //chegou no final, nao tem mais a onde botar e pa, ele já deve ter colocado municao em outros slots mas sobrou umas entao atualiza o balor da q ta no chao
            sendoUsado.SetQuantidade(novoItem.quantidade);

        return false;
    }

    public override bool AdicionarItem(Item novoItem, int index, bool ignorarNull)
    {
        //print(novoItem.nome + " -- " + novoItem.tipoItem + " -- " + novoItem.quantidade);

        if (novoItem != null)
            if (CheckInventario(this, novoItem) == false) return false;

        if (!ignorarNull && objetosInventario[index] == null)
        {
            return false;
        }

        if(novoItem != null)
        if (novoItem.tipoItem == TipoItem.Recurso)
            switch (novoItem.tipoMunicao)
            {
                case TipoMunicao.Leve:
                    totalMunicaoLeve += novoItem.quantidade;
                    CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola_V>().municaoRestante = totalMunicaoLeve;
                    break;
                case TipoMunicao.Media:
                    totalMunicaoMedia += novoItem.quantidade;
                    CG_Vitto.Instance.Rifle.transform.GetChild(1).GetComponent<Rifle_V>().municaoRestante = totalMunicaoMedia;
                    break;
                case TipoMunicao.Pesada:
                    totalMunicaoPesada += novoItem.quantidade;
                    //CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola>().municaoRestante = totalMunicaoPesada;
                    break;
                default:
                    break;
            }

        objetosInventario[index] = novoItem;
        canvasInventario[index].Atualizar();
        return true;
    }

    public override void ApagarItem(int indexItem) //evitar dar errado pq tem 2 itens iguais por ex
    {
        if (objetosInventario[indexItem].tipoItem == TipoItem.Recurso)
            switch (objetosInventario[indexItem].tipoMunicao)
            {
                case TipoMunicao.Leve:
                    totalMunicaoLeve -= objetosInventario[indexItem].quantidade;
                    CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola_V>().municaoRestante = totalMunicaoLeve;
                    break;
                case TipoMunicao.Media:
                    totalMunicaoMedia -= objetosInventario[indexItem].quantidade;
                    CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola_V>().municaoRestante = totalMunicaoMedia;
                    break;
                case TipoMunicao.Pesada:
                    totalMunicaoPesada -= objetosInventario[indexItem].quantidade;
                    CG_Vitto.Instance.Pistola.transform.GetChild(1).GetComponent<Pistola_V>().municaoRestante = totalMunicaoPesada;
                    break;
                default:
                    break;
            }

        objetosInventario[indexItem] = null;
        canvasInventario[indexItem].Atualizar();
    }

}