﻿using UnityEngine;
using UnityEngine.UI;

public class HUD : MonoBehaviour
{
    Player player;
    CG_Vitto playerV;
    public InventarioRapido inventarioRapido;
    public InventarioMochila inventarioMochila;
    public InventarioLoot inventarioLoot;
    public InventarioCraft inventarioCraft;
    public InventarioCraftResultado inventarioCraftResultado;
    public Construir construir;
    public ReceitaCanvas receitaCanvas;
    public static HUD Instance;
    public AtributoCanvas[] atributosVitais; //vida, energia, fome, sede; ATRIBUTOS DO PLAYER sao 4
    public Text armaNome; //img icone arma
    public Image armaIcone; //img icone arma
    public GameObject municaoParent; //municao
    public Text municao; //municao
    public Text municaoMax; //municao max
    public Text granada; //municao max
    public Legenda legenda;

    public void Iniciar()
    {
        Instance = this;
        player = Player.Instance;
        playerV = player.gameObject.GetComponent<CG_Vitto>();

        AtualizarVitais();

        inventarioMochila.Iniciar();
        inventarioRapido.Iniciar();
        inventarioLoot.Iniciar();
        inventarioCraft.Iniciar();
        inventarioCraftResultado.Iniciar();
        construir.Iniciar();
        receitaCanvas.Iniciar();
        legenda.Iniciar();
    }

    public void AtualizarVitais() // chamado no player.VitaisAdicionar e player.VitaisSetar
    {
        if (atributosVitais[0].fill) atributosVitais[0].fill.fillAmount = (float)playerV.vida / 100f;
        if (atributosVitais[1].fill) atributosVitais[1].fill.fillAmount = (float)player.vitais.energia / 100f;
        if (atributosVitais[2].fill) atributosVitais[2].fill.fillAmount = (float)player.vitais.fome / 100f;
        if (atributosVitais[3].fill) atributosVitais[3].fill.fillAmount = (float)player.vitais.sede / 100f;

        if (atributosVitais[0].textMax) atributosVitais[0].textMax.text = 100f.ToString();
        if (atributosVitais[1].textMax) atributosVitais[1].textMax.text = 100f.ToString();
        if (atributosVitais[2].textMax) atributosVitais[2].textMax.text = 100f.ToString();
        if (atributosVitais[3].textMax) atributosVitais[3].textMax.text = 100f.ToString();

        if (atributosVitais[0].text) atributosVitais[0].text.text = ((int)playerV.vida).ToString();
        if (atributosVitais[1].text) atributosVitais[1].text.text = ((int)player.vitais.energia).ToString();
        if (atributosVitais[2].text) atributosVitais[2].text.text = ((int)player.vitais.fome).ToString();
        if (atributosVitais[3].text) atributosVitais[3].text.text = ((int)player.vitais.sede).ToString();
    }
}
