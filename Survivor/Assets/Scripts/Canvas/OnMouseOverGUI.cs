﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class OnMouseOverGUI : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IPointerExitHandler
{
    public UnityEvent onMouseEnter;
    public UnityEvent onMouseClick;
    public UnityEvent onMouseDown;
    public UnityEvent onMouseUp;
    public UnityEvent onMouseExit;

    public void OnPointerEnter(PointerEventData eventData)
    {
        onMouseEnter.Invoke();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        onMouseClick.Invoke();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        onMouseDown.Invoke();
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        onMouseUp.Invoke();
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        onMouseExit.Invoke();
    }

}
