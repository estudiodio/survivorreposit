﻿using UnityEngine;

public class ObraConstrucaoGame : MonoBehaviour
{
    [SerializeField] ObraGame obra;
    [SerializeField] ConstrucaoGame construcao;

    protected void Awake()
    {
        obra.construcao = construcao;
        construcao.obra = obra;

        obra.obraConstrucaoGame = this;
        construcao.obraConstrucaoGame = this;
    }

    public void Interagindo()
    {
        if (ObraOuConstrucao() == 0)
        {
            InventarioRapido.Instance.AtualizarFerramentaConstrucao(0);
            obra.Construindo();
        }
        else
        {
            construcao.Reparando();
            InventarioRapido.Instance.AtualizarFerramentaConstrucao(1);
        }
    }

    public float Progresso() //0 a 1
    {
        if (ObraOuConstrucao() == 0)
        {
            return obra.Progresso();
        }
        else
        {
            return construcao.Progresso();
        }
    }

    public int ObraOuConstrucao() // 0= em obra> tem q construir, 1 = construido> tem q repararar
    {
        if (obra)
        {
            if (obra.Progresso() < 1)
                return 0;
            else
                return 1;
        }
        else
            return 1;
    }
}
