﻿using UnityEngine;
using UnityEngine.UI;

public class Inventario : MonoBehaviour
{
    [SerializeField] bool ferramentas = false;
    [SerializeField] bool recursos = false;
    [SerializeField] bool comestiveis = false;
    [SerializeField] bool materiais = false;
    //[SerializeField] bool mesa = false;
    [SerializeField] bool construcoes = false;
    [SerializeField] public Item[] objetosInventario;// { protected set { objetosInventario = value; } get { return objetosInventario; } }
    [SerializeField] protected ItemCanvas[] canvasInventario;
    [SerializeField] public bool resetarNoInicio = true;
    [SerializeField] protected static ItemCanvas selecionadoSlotAntes;
    [SerializeField] protected static ItemCanvas selecionadoSlotEmPotencial;
    [SerializeField] protected static ItemCanvas selecionadoSlotComer;
    [SerializeField] public static ItemCanvas selecionadoSlotConstruir;
    [SerializeField] protected static bool selecionadoSlotEmPotencialChao; //se o player quiser jogar item no chao
    [SerializeField] public static Image selecionadoImg;
    [SerializeField] public static ItemCanvasInteragir interacaoComestivelConstrucaoImg;
    [SerializeField] public static Image foraImg; //se o player quiser jogar item no chao
    public GameObject parentInventario;

    public virtual void Iniciar()
    {
        if (resetarNoInicio)
        {
            objetosInventario = new Item[transform.childCount];
        }

        canvasInventario = new ItemCanvas[transform.childCount];
        for (int i = 0; i < canvasInventario.Length; i++)
        {
            canvasInventario[i] = transform.GetChild(i).gameObject.GetComponent<ItemCanvas>();
            canvasInventario[i].inventario = this;
            canvasInventario[i].Iniciar();
        }

        selecionadoImg = HUD.Instance.transform.Find("[Inventario] Item Selecionado").GetComponent<Image>();
        interacaoComestivelConstrucaoImg = HUD.Instance.transform.Find("[Inventario] Interagir Comestivel").GetComponent<ItemCanvasInteragir>();
        foraImg = HUD.Instance.transform.Find("[Inventario] Back").GetComponent<Image>();

        ImgItemSelecionado(null);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse1))
        {
            //print(selecionadoSlotAntes);

            if (selecionadoSlotAntes) return;
            if (selecionadoSlotEmPotencial == null) return;
            if (selecionadoSlotEmPotencial.item == null) return;

            if (selecionadoSlotEmPotencial && selecionadoSlotEmPotencial.item.tipoItem == TipoItem.Comestivel) //se ele nao estiver arrastando && se o item q ele ta com o mouse em cima for comestivel
            {
                interacaoComestivelConstrucaoImg.gameObject.transform.position = selecionadoSlotEmPotencial.gameObject.transform.position;
                interacaoComestivelConstrucaoImg.GetComponent<Image>().enabled = true;
                interacaoComestivelConstrucaoImg.textInteracao.text = "COMER";
                interacaoComestivelConstrucaoImg.textInteracao.enabled = true;
                selecionadoSlotComer = selecionadoSlotEmPotencial;
            }
            else
            if (selecionadoSlotEmPotencial && selecionadoSlotEmPotencial.item.tipoItem == TipoItem.Construcao) //se ele nao estiver arrastando && se o item q ele ta com o mouse em cima for comestivel
            {
                interacaoComestivelConstrucaoImg.gameObject.transform.position = selecionadoSlotEmPotencial.gameObject.transform.position;
                interacaoComestivelConstrucaoImg.GetComponent<Image>().enabled = true;
                interacaoComestivelConstrucaoImg.textInteracao.text = "CRIAR";
                interacaoComestivelConstrucaoImg.textInteracao.enabled = true;
                selecionadoSlotConstruir = selecionadoSlotEmPotencial;
            }
        }
    }

    public static void ConsumirComestivel()
    {
        if (!selecionadoSlotComer) return;
        if (selecionadoSlotComer.item == null) return;
        if (selecionadoSlotComer.item.tipoItem != TipoItem.Comestivel) return;

        Player.Instance.VitaisAdicionar(selecionadoSlotComer.item.vitais);
        selecionadoSlotComer.inventario.ApagarItem(selecionadoSlotComer.gameObject.transform.GetSiblingIndex());

        interacaoComestivelConstrucaoImg.GetComponent<Image>().enabled = false;
        interacaoComestivelConstrucaoImg.highlightImagem.enabled = false;
        interacaoComestivelConstrucaoImg.textInteracao.enabled = false;

        selecionadoSlotComer = null;
    }

    public static void PosicionarConstrucao()
    {
        //print("construir " + selecionadoSlotConstruir.item.nome);
        if (!selecionadoSlotConstruir) return;
        if (selecionadoSlotConstruir.item == null) return;
        if (selecionadoSlotConstruir.item.tipoItem != TipoItem.Construcao) return;

        interacaoComestivelConstrucaoImg.GetComponent<Image>().enabled = false;
        interacaoComestivelConstrucaoImg.highlightImagem.enabled = false;
        interacaoComestivelConstrucaoImg.textInteracao.enabled = false;

        Construir.SetConstrucao(selecionadoSlotConstruir.item.prefab);
        InventarioCraft.Instance.Enable(false);
        InventarioLoot.Instance.Enable(false);
        InventarioMochila.Instance.Enable(false);
        InventarioRapido.Instance.Enable(false);
    }

    public static void TerminouConstrucao(bool construiu)
    {
        if (!selecionadoSlotConstruir) return;
        if (selecionadoSlotConstruir.item == null) return;
        if (selecionadoSlotConstruir.item.tipoItem != TipoItem.Construcao) return;

        if (construiu)
        {
            selecionadoSlotConstruir.inventario.ApagarItem(selecionadoSlotConstruir.gameObject.transform.GetSiblingIndex());
            InventarioCraft.Instance.Enable(false);
            InventarioLoot.Instance.Enable(false);
            InventarioMochila.Instance.Enable(false);
            InventarioRapido.Instance.Enable(false);
        }
        else
        {
            selecionadoSlotConstruir.Atualizar();
            InventarioCraft.Instance.Enable(false);
            InventarioLoot.Instance.Enable(false);
            InventarioMochila.Instance.Enable(true);
            InventarioRapido.Instance.Enable(true);
        }

        interacaoComestivelConstrucaoImg.GetComponent<Image>().enabled = false;
        interacaoComestivelConstrucaoImg.highlightImagem.enabled = false;
        interacaoComestivelConstrucaoImg.textInteracao.enabled = false;

        selecionadoSlotConstruir = null;
        ResetarSlotsSelecionados();
    }
    public virtual void SwitchEnable()
    {
        parentInventario.SetActive(!parentInventario.activeSelf);

        if (!Enabled())
        {
            ResetarSlotsSelecionados();
            DesligarHighlightsSlots();
        }

    }
    public virtual void Enable(bool enabled)
    {
        parentInventario.SetActive(enabled);
        if (!Enabled())
        {
            ResetarSlotsSelecionados();
            DesligarHighlightsSlots();
        }
    }

    public virtual bool Enabled()
    {
        return parentInventario.activeSelf;
    }

    public virtual void SetInventario(Item[] itens)
    {
        objetosInventario = new Item[transform.childCount];

        for (int i = 0; i < objetosInventario.Length; i++)
        {
            if (i < itens.Length)
                if (CheckInventario(this, itens[i]) == false) // se o inventario nao suporta esse item, continua o baile
                    continue;
                else //se ele aceita, preenche
                {
                    if (i < itens.Length)
                        objetosInventario[i] = itens[i];
                    else
                        objetosInventario[i] = null;
                    canvasInventario[i].Atualizar();
                }
        }
        /*objetosInventario = new Item[transform.childCount];

        for (int i = 0; i < itens.Length; i++)
        {
            if (CheckInventario(this, itens[i]) == false) // se o inventario nao suporta esse item, continua o baile
                continue;
            else //se ele aceita, preenche
            {
                objetosInventario[i] = itens[i];
                canvasInventario[i].Atualizar();
            }
        }*/
    }

    public virtual bool AdicionarItem(Item novoItem, ItemGame sendoUsado)
    {
        if (CheckInventario(this, novoItem) == false)
            return false;

        //print("Item " + novoItem.nomeReal  +" e tipo " + novoItem.tipoItem);
        if (novoItem.tipoItem == TipoItem.Recurso) //tenta colocar municao onde ja tem
        {
            for (int i = 0; i < objetosInventario.Length; i++)
            {
                if (novoItem.quantidade <= 0) return true;

                /*if (objetosInventario[i] != null)
                    print("muniççao qnt " + novoItem.quantidade + " " + novoItem.tipoMunicao + " E no slot tem " + objetosInventario[i].tipoMunicao);
                else
                    print("muniççao qnt " + novoItem.quantidade + " " + novoItem.tipoMunicao + " E no slot VAZIO");*/

                if (objetosInventario[i] != null)
                    if (objetosInventario[i].tipoItem == TipoItem.Recurso)
                        if (objetosInventario[i].tipoMunicao == novoItem.tipoMunicao) // se o tipo de municao for o msm
                        {
                            //print("achou ms  tipo de municao nesse slot " + i + " && tem " + novoItem.quantidade + " balas pra tentar botar");
                            if (objetosInventario[i].quantidade < Itens.Instance.quantidadeMaxBalasSlot) //se tem menos quantidade do que o maximo
                            {
                                int quantidadeQuePodeReceber = Itens.Instance.quantidadeMaxBalasSlot - objetosInventario[i].quantidade;
                                if (novoItem.quantidade <= quantidadeQuePodeReceber)
                                {
                                    objetosInventario[i].quantidade += novoItem.quantidade;
                                    canvasInventario[i].Atualizar();
                                    return true;
                                }
                                else
                                {
                                    objetosInventario[i].quantidade += quantidadeQuePodeReceber;
                                    canvasInventario[i].Atualizar();

                                    novoItem.quantidade -= quantidadeQuePodeReceber;
                                }
                            }
                        }
            }
        }

        //print("ainda tem muniççao " + novoItem.quantidade);
        for (int i = 0; i < objetosInventario.Length; i++)
        {
            if (objetosInventario[i] == null)
            {
                    objetosInventario[i] = novoItem;
                    canvasInventario[i].Atualizar();
                    return true;
            }
        }

        if (novoItem.tipoItem == TipoItem.Recurso) //chegou no final, nao tem mais a onde botar e pa, ele já deve ter colocado municao em outros slots mas sobrou umas entao atualiza o balor da q ta no chao
            sendoUsado.SetQuantidade(novoItem.quantidade);

        return false;
    }

    public virtual bool AdicionarItem(Item novoItem, int index, bool ignorarNull)
    {
        if (novoItem != null)
            if (CheckInventario(this, novoItem) == false) return false;

        if (!ignorarNull && objetosInventario[index] == null)
        {
            return false;
        }

        objetosInventario[index] = novoItem;
        canvasInventario[index].Atualizar();
        return true;
    }

    public virtual void ApagarItem(int indexItem) //evitar dar errado pq tem 2 itens iguais por ex
    {
        //print("apagando " + this.gameObject.name + " " + indexItem);
        objetosInventario[indexItem] = null;
        canvasInventario[indexItem].Atualizar();
    }

    public static bool TrocarPosItem(Inventario origem, int origemIndex, Inventario destino, int destinoIndex)
    {
        Item um = origem.objetosInventario[origemIndex];
        Item dois = destino.objetosInventario[destinoIndex];

        if (CheckInventario(destino, um) == false)
            return false;

        if (dois != null)
            if (CheckInventario(origem, dois) == false)
                return false;

        origem.AdicionarItem(dois, origemIndex, true);
        destino.AdicionarItem(um, destinoIndex, true);
        return true;
    }

    public static bool CheckInventario(Inventario inventario, Item item) //checar se o tal inventario aceita o tipo de item especifico
    {
        if(selecionadoSlotEmPotencial)
        if (inventario == InventarioCraftResultado.Instance && selecionadoSlotEmPotencial.inventario == InventarioCraftResultado.Instance) return false;
        if (item == null) return true;

        switch (item.tipoItem)
        {
            case TipoItem.Ferramenta:
                if (!inventario.ferramentas)
                    return false;
                break;
            case TipoItem.Comestivel:
                if (!inventario.comestiveis)
                    return false;
                break;
            case TipoItem.Recurso:
                if (!inventario.recursos)
                    return false;
                break;
            case TipoItem.Material:
                if (!inventario.materiais)
                    return false;
                break;

            /*case TipoItem.MesaCraft:
                if (!inventario.mesa)
                    return false;
                break;*/

            case TipoItem.Construcao:
                if (!inventario.construcoes)
                    return false;
                break;
            default:
                break;
        }

        return true;
    }

    public static void Selecionar(Inventario inventario, int indexItem)
    {
        if (inventario.objetosInventario[indexItem] == null) return;

                inventario.canvasInventario[indexItem].AtualizarNullFake();
            selecionadoSlotAntes = inventario.canvasInventario[indexItem];

        ImgItemSelecionado(inventario.objetosInventario[indexItem].imagem);
    }

    public static void SelecionarSlotDrop(ItemCanvas itemCanvas) //passou mouse em um slot
    {
        selecionadoSlotEmPotencial = itemCanvas;
    }

    public static void SelecionarSlotDropChao(bool derrubar) // passou mouse do chao
    {
        selecionadoSlotEmPotencialChao = derrubar;
    }

    protected static void ResetarSlotsSelecionados() //fala q ele nao escolheu de onde tirar o item nem onde por
    {
        SelecionarSlotDrop(null);
        selecionadoSlotAntes = null;
        selecionadoSlotComer = null;
        selecionadoSlotEmPotencialChao = false;
        //selecionadoSlotConstruir = null;
    }

    protected void DesligarHighlightsSlots()
    {
        for (int i = 0; i < canvasInventario.Length; i++)
        {
            canvasInventario[i].HighlightMouseOver(false);
        }
    }

    public static void Soltar()
    {
        ImgItemSelecionado(null);

        if (selecionadoSlotAntes == null) return; //se ele nao tinha clicado antes em nd, n tem oq fazer

        if (selecionadoSlotEmPotencialChao) //se for pra derrubar
        {
            if (selecionadoSlotAntes.item.tipoItem == TipoItem.Construcao)
            {
                selecionadoSlotConstruir = selecionadoSlotAntes;
                PosicionarConstrucao();
                return;
            }
            GameObject prefab = selecionadoSlotAntes.item.prefab;
            GameObject objeto = null;
            if (prefab != null)
                objeto = Instantiate(prefab, Player.Instance.transform.position + (Player.Instance.transform.forward * 2), prefab.transform.rotation);

            if (selecionadoSlotAntes.item.tipoItem == TipoItem.Recurso || selecionadoSlotAntes.item.tipoItem == TipoItem.Ferramenta)
                objeto.GetComponent<ItemGame>().SetQuantidade(selecionadoSlotAntes.item.quantidade);
            //if (selecionadoSlotAntes.item.quantidade != Itens.Instance.Item((ItemNome)selecionadoSlotAntes.item.IDGeral).quantidade)


            //derrubar item, instanciar na frente do player
            selecionadoSlotAntes.inventario.ApagarItem(selecionadoSlotAntes.transform.GetSiblingIndex());
            ResetarSlotsSelecionados();
            return;
        }

        if (selecionadoSlotEmPotencial == null) //se ele nao escolheu um novo slot pra botar o item
        {
            selecionadoSlotAntes.Atualizar();
            ResetarSlotsSelecionados();
            return;
        }

        bool trocado = TrocarPosItem(selecionadoSlotAntes.inventario, selecionadoSlotAntes.transform.GetSiblingIndex(), selecionadoSlotEmPotencial.inventario, selecionadoSlotEmPotencial.transform.GetSiblingIndex());
        //se a troca deu certo

        if (trocado == false) // se a troca deu errado
        {
            selecionadoSlotAntes.Atualizar();
            ResetarSlotsSelecionados();
            return;
        }

        //ResetarSlotsSelecionados();
        //SelecionarSlotDrop(null);
        selecionadoSlotAntes = null;
        selecionadoSlotComer = null;
        selecionadoSlotEmPotencialChao = false;
        selecionadoSlotConstruir = null;
    }

    public static void ImgItemSelecionado(Sprite img)
    {
        selecionadoImg.sprite = img;
        if (img) selecionadoImg.enabled = true;
        else selecionadoImg.enabled = false;
    }

}
