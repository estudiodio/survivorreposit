﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pause : MonoBehaviour
{
    public static Pause Instance;

    public void Iniciar()
    {
        #region Instance
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);
        #endregion
    }
    public void SwitchEnable()
    {
        gameObject.SetActive(!gameObject.activeSelf);

        if (Enabled())
        {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            Time.timeScale = 1;
            if (!InventarioMochila.Instance.Enabled())
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }
        CG_Vitto.Instance.pausado = Enabled();
    }
    public void Enable(bool enabled)
    {
        gameObject.SetActive(enabled);

        if (Enabled())
        {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        else
        {
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        CG_Vitto.Instance.pausado = Enabled();
    }

    public virtual bool Enabled()
    {
        return gameObject.activeSelf;
    }
}
