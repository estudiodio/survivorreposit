﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasWorld : MonoBehaviour
{
    public int distBaseScale = 6;
    public bool disablePlayerFar = false;
    public int disablePlayerFarDistance = 6;
    public bool lookAtPlayer;
    public bool sameScale;

    void FixedUpdate()
    {
        float dist = Vector3.Distance(transform.position, Camera.main.transform.position);
        if (disablePlayerFar)
        {
            if (dist > disablePlayerFarDistance) GetComponent<Canvas>().enabled = false;
            else GetComponent<Canvas>().enabled = true;
        }

        if (!GetComponent<Canvas>().enabled) return;
        if (lookAtPlayer)
        transform.LookAt(2* transform.position - Camera.main.transform.position);
        if (sameScale)
        transform.localScale = Vector3.one * dist / distBaseScale;
    }
}
