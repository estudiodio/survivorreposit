﻿using UnityEngine;

public class InventarioCraftResultado : Inventario
{
    public static InventarioCraftResultado Instance;

    public Color corArmaPreview;

    public override void Iniciar()
    {
        base.Iniciar();
        #region Instance
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);
        #endregion
        objetosInventario[0] = null;
    }

    public void SetInventarioFake(Item[] itens) //preenche só a imagem
    {
        objetosInventario = new Item[transform.childCount];
        //print("SET INVENTARIO FAKE");

        for (int i = 0; i < objetosInventario.Length; i++)
        {
            //print((i < itens.Length) + " " + CheckInventario(this, itens[i]));
            if (i < itens.Length)
            if (CheckInventario(this, itens[i]) == false) // se o inventario nao suporta esse item, continua o baile
                continue;
            else //se ele aceita, preenche
            {
                canvasInventario[i].AtualizarFake(itens[i]);
            }
        }
    }
}