﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReceitaCanvas : MonoBehaviour
{
    public static ReceitaCanvas Instance;
    [HideInInspector] public GameObject parentInventario;
    public List<List<Item>> receitas = new List<List<Item>>();
    public List<Item> receitasResultado = new List<Item>();
    public Image resultado;
    public Image[] ingredientes;
    public int numReceita = 0;

    public virtual void SwitchEnable()
    {
        parentInventario.SetActive(!parentInventario.activeSelf);

        /*if (!Enabled())
        {

        }*/

    }
    public virtual void Enable(bool enabled)
    {
        parentInventario.SetActive(enabled);
        /*if (!Enabled())
        {

        }*/
    }

    public virtual bool Enabled()
    {
        return parentInventario.activeSelf;
    }


    public void ReceitaProxima()
    {
        if (numReceita == receitas.Count - 1) numReceita = 0;
        else numReceita++;
        SetReceita(receitas[numReceita], receitasResultado[numReceita]);
    }

    public void ReceitaAnterior()
    {
        if (numReceita == 0) numReceita = receitas.Count - 1;
        else numReceita--;
        SetReceita(receitas[numReceita], receitasResultado[numReceita]);
    }

    public void Iniciar()
    {
        Instance = this;
        parentInventario = this.gameObject;
        receitas = InventarioCraft.Instance.receitas;
        receitasResultado = InventarioCraft.Instance.receitasResultado;
        //print(receitasResultado.Count + " receitas");
        for (int i = 0; i < receitas.Count; i++)
        {
            if (receitas[i].Count != 8) print("receita de " + receitasResultado[i].nome + " deve ta errada pq n tem 8 componente");
        }
        SetReceita(receitas[0], receitasResultado[0]);
    }

    public void SetReceita(List<Item> ingredientesReceita, Item resultadoReceita)
    {
        List<Sprite> itensIngredientes = new List<Sprite>();
        //print(ingredientesReceita.Count);

        for (int i = 0; i< ingredientesReceita.Count; i++)
		{
            //print(ingredientesReceita[i].nome + " x " + ingredientesReceita[i].quantidade );
            //for (int q = 0; q < ingredientesReceita[i].quantidade; q++)
            //{
            if (ingredientesReceita[i] != null)
                itensIngredientes.Add(ingredientesReceita[i].imagem);
            else
                itensIngredientes.Add(null);
            //}
        }

        for (int j = 0; j < 8; j++)
        { 
            //if (j < itensIngredientes.Count)
            if (itensIngredientes[j] != null)
            {
                ingredientes[j].sprite = itensIngredientes[j];
                ingredientes[j].enabled = true;
            }
            else
            {
                ingredientes[j].sprite = null;
                ingredientes[j].enabled = false;
            }
        }

        resultado.sprite = resultadoReceita.imagem;
    }
}
