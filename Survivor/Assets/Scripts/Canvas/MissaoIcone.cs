﻿using UnityEngine;
using UnityEngine.UI;

public class MissaoIcone : MonoBehaviour
{
    public static MissaoIcone Instance;
    public Image icone;

    void Awake()
    {
        #region Instance
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);
        #endregion
        //player = Player.Instance;
    }

    public void SetInteracao(Sprite imagem, Vector3 posicao)
    {
        icone.sprite = imagem;
        transform.position = posicao;

        if (imagem != null) gameObject.SetActive(true);
        else gameObject.SetActive(false);
    }
}