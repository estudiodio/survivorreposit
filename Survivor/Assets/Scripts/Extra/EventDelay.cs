﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum Call
{
    None,
    OnAwake,
    OnEnable,
    OnStart,
    OnDisable,
    OnDestroy
}
[System.Serializable]
public class EventSecondsDelay
{
    public Call callOn;
    public float seconds;
    public UnityEvent events;
}
public class EventDelay : MonoBehaviour
{
    public EventSecondsDelay[] events;
    List<EventSecondsDelay> onAwake = new List<EventSecondsDelay>();
    List<EventSecondsDelay> onEnable = new List<EventSecondsDelay>();
    List<EventSecondsDelay> onStart = new List<EventSecondsDelay>();
    List<EventSecondsDelay> onDisable = new List<EventSecondsDelay>();
    List<EventSecondsDelay> onDestroy = new List<EventSecondsDelay>();

    void ListEvents()
    {
        for (int i = 0; i < events.Length; i++)
        {
            switch (events[i].callOn)
            {
                case Call.None:
                    break;
                case Call.OnAwake:
                    onAwake.Add(events[i]);
                    break;
                case Call.OnEnable:
                    onEnable.Add(events[i]);
                    break;
                case Call.OnStart:
                    onStart.Add(events[i]);
                    break;
                case Call.OnDisable:
                    onDisable.Add(events[i]);
                    break;
                case Call.OnDestroy:
                    onDestroy.Add(events[i]);
                    break;
                default:
                    break;
            }
        }
    }

    void Awake()
    {
        ListEvents();

        for (int i = 0; i < onAwake.Count; i++)
        {
            if (gameObject.activeInHierarchy)
                StartCoroutine(CallEventInSecs(onAwake[i].events, onAwake[i].seconds));
        }
    }

    void OnEnable()
    {
        for (int i = 0; i < onEnable.Count; i++)
        {
            if (gameObject.activeInHierarchy)
                StartCoroutine(CallEventInSecs(onEnable[i].events, onEnable[i].seconds));
        }
    }
    void Start()
    {
        for (int i = 0; i < onStart.Count; i++)
        {
            if (gameObject.activeInHierarchy)
                StartCoroutine(CallEventInSecs(onStart[i].events, onStart[i].seconds));
        }
    }

    void OnDisable()
    {
        for (int i = 0; i < onDisable.Count; i++)
        {
            if (gameObject.activeInHierarchy)
            StartCoroutine(CallEventInSecs(onDisable[i].events, onDisable[i].seconds));
        }
    }

    void OnDestroy()
    {
        for (int i = 0; i < onDestroy.Count; i++)
        {
            if (gameObject.activeInHierarchy)
                StartCoroutine(CallEventInSecs(onDestroy[i].events, onDestroy[i].seconds));
        }
    }

    public void EventCall(int numIndexArray)
    {
        events[numIndexArray].events.Invoke();
    }

    public void EventCallDelayed (int numIndexArray)
    {
        StartCoroutine(CallEventInSecs(events[numIndexArray].events, events[numIndexArray].seconds));
    }

    IEnumerator CallEventInSecs(UnityEvent eventToInvoke, float secs)
    {
        yield return new WaitForSeconds(secs);
        eventToInvoke.Invoke();
    }
}
