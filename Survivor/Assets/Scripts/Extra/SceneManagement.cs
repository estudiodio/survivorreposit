﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Events;

public class SceneManagement : MonoBehaviour
{
    public UnityEvent ASyncDone;
    public void LoadScene(int num)
    {
        SceneManager.LoadScene(num);
    }

    public void QuitApplication()
    {
        Application.Quit();
    }
    public void TimeScale(int time)
    {
        Time.timeScale = time;
    }

    public void LoadSceneAsync(int num)
    {
        Cursor.visible = false;
        Time.timeScale = 1;
        StartCoroutine(CarregarJogo(num));
    }

    AsyncOperation async;
    public Image asyncProgress;
    IEnumerator CarregarJogo(int num) //libera cena com enter
    {
        yield return new WaitForSeconds(5);
        yield return null;

        async = SceneManager.LoadSceneAsync(num);
        async.allowSceneActivation = false;
        while (!async.isDone)
        {
            if (async.progress < 0.9f)
                asyncProgress.fillAmount = async.progress;
            else
                asyncProgress.fillAmount = 1;
            if (async.progress >= 0.9f)
            {
                ASyncDone.Invoke();

                if (Input.GetKeyDown(KeyCode.Return))
                    AllowASync();
                //loading.transform.GetChild(0).gameObject.GetComponent<Text>().text = "Pressione ENTER para continuar";
                //if (Input.GetKeyDown(KeyCode.Space))
                //async.allowSceneActivation = true;
            }
            yield return null;
        }
    }

    public void AllowASync()
    {
        async.allowSceneActivation = true;
    }
}
