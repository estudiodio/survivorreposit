﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections.Generic;

public enum CallOn { StateEnter, StateExit, StateUpdate, StateMove, StateIK, StateMachineEnter, StateMachineExit }
// OnStateEnter is called before OnStateEnter is called on any state inside this state machine
// OnStateExit is called before OnStateExit is called on any state inside this state machine
// OnStateUpdate is called before OnStateUpdate is called on any state inside this state machine
// OnStateMove is called before OnStateMove is called on any state inside this state machine
// OnStateIK is called before OnStateIK is called on any state inside this state machine
// OnStateMachineEnter is called when entering a state machine via its Entry Node
// OnStateMachineExit is called when exiting a state machine via its Exit Node

[System.Serializable]
public class EventCall
{
    public CallOn call;
    public UnityEvent evento;
}

public class animatorBehaviour : StateMachineBehaviour
{
    public EventCall[] Eventos;

    List<UnityEvent> StateEnter = new List<UnityEvent>();
    List<UnityEvent> StateExit = new List<UnityEvent>();
    List<UnityEvent> StateUpdate = new List<UnityEvent>();
    List<UnityEvent> StateMove = new List<UnityEvent>();
    List<UnityEvent> StateIK = new List<UnityEvent>();
    List<UnityEvent> StateMachineEnter = new List<UnityEvent>();
    List<UnityEvent> StateMachineExit = new List<UnityEvent>();

    private void Awake()
    {
        foreach (var evento in Eventos)
        {
            switch (evento.call)
            {
                case CallOn.StateEnter:
                    StateEnter.Add(evento.evento);
                    break;
                case CallOn.StateExit:
                    StateExit.Add(evento.evento);
                    break;
                case CallOn.StateUpdate:
                    StateUpdate.Add(evento.evento);
                    break;
                case CallOn.StateMove:
                    StateMove.Add(evento.evento);
                    break;
                case CallOn.StateIK:
                    StateIK.Add(evento.evento);
                    break;
                case CallOn.StateMachineEnter:
                    StateMachineEnter.Add(evento.evento);
                    break;
                case CallOn.StateMachineExit:
                    StateMachineExit.Add(evento.evento);
                    break;
                default:
                    break;
            }
        }
    }

    public override void OnStateEnter(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        foreach (var evento in StateEnter)
        {
                evento.Invoke();
        }
    }
    public override void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        foreach (var evento in StateExit)
        {
            evento.Invoke();
        }
    }
    public override void OnStateUpdate(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        foreach (var evento in StateUpdate)
        {
            evento.Invoke();
        }
    }
    public override void OnStateMove(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        foreach (var evento in StateMove)
        {
            evento.Invoke();
        }
    }
    public override void OnStateIK(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex)
    {
        foreach (var evento in StateIK)
        {
            evento.Invoke();
        }
    }
    public override void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
    {
        foreach (var evento in StateMachineEnter)
        {
            evento.Invoke();
        }
    }
    public override void OnStateMachineExit(Animator animator, int stateMachinePathHash)
    {
        foreach (var evento in StateMachineExit)
        {
            evento.Invoke();
        }
    }
}
