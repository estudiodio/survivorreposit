﻿using UnityEngine;

public enum DebugType { Log, Warning, Error }
public class QuickDebug : MonoBehaviour
{
    public DebugType type;
    public void DebugNow(string line)
    {
        switch (type)
        {
            case DebugType.Log:
                Debug.Log(line);
                break;
            case DebugType.Warning:
                Debug.LogWarning(line);
                break;
            case DebugType.Error:
                Debug.LogError(line);
                break;
            default:
                break;
        }
    }
}
