﻿using UnityEngine;
using UnityEngine.Events;

public class OnMouseOverCollider : MonoBehaviour
{
    public UnityEvent onMouseEnter;
    public UnityEvent onMouseOver;
    public UnityEvent onMouseExit;

    void OnMouseEnter()
    {
        onMouseEnter.Invoke();
    }

    void OnMouseOver()
    {
        onMouseOver.Invoke();
    }

    void OnMouseExit()
    {
        onMouseExit.Invoke();
    }
}
