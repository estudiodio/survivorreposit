﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColisorDano : MonoBehaviour
{
    [HideInInspector] public Combatente combatenteOrigem;
    public int dano;
    private void OnTriggerEnter(Collider other)
    {
        Destrutivel colidiu = other.GetComponent<Destrutivel>();
        if (colidiu != null)
        {
            //print("dano de " + combatenteOrigem + " em " + other.gameObject.name + " VIDA " + colidiu.Vida() + "/" + colidiu.VidaMax());
            colidiu.ReceberDano(dano * combatenteOrigem.forca); //dar dano no inimigo
            combatenteOrigem.RecuperarVida(5); //quem deu o dano recupera vida (sistema de vigor)
        }
    }
}
