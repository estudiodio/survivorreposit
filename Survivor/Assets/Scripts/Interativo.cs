﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class Interativo : MonoBehaviour
{
    public Legendas legendas;
    public UnityEvent eventos;

    [SerializeField] public float interagirProgresso = 0;
    [Range(0, 20)]
    [SerializeField] float tempoInteragirProgresso = 2;

    public bool interagiu;
    public bool interacaoUnica;
    public bool travarPlayer;

    public void SetLegendas()
    {
        Legenda.Instance.SetLegendas(legendas);
    }
    public bool PodeInteragir()
    {
        if (interacaoUnica) return !interagiu;
        else return true;
    }

    public void Interagindo()
    {
        interagirProgresso += 0.02f;
        if (Progresso() >= 1)
        {
            if (interacaoUnica) interagirProgresso = 1;
            else interagirProgresso = 0;

            interagiu = true;
            eventos.Invoke();
        }
    }

    public float Progresso()
    {
        return interagirProgresso / tempoInteragirProgresso;
    }
}