﻿using UnityEngine;
using UnityEngine.UI;

public class Legenda : MonoBehaviour
{
    public static Legenda Instance;
    [SerializeField] Legendas legendas;
    [SerializeField] float falaProgresso;
    [SerializeField] int falaNum;
    [SerializeField] float intervalo;
    Text text;
    AudioSource audioSourceInicio;
    AudioSource audioSourceFinal;

    public void Iniciar()
    {
        Instance = this;
        legendas = null;
        text = GetComponent<Text>();
        audioSourceInicio = GetComponents<AudioSource>()[0];
        audioSourceFinal = GetComponents<AudioSource>()[1];
    }

    public void SetLegendas(Legendas legendasNova)
    {
        legendas = legendasNova;
        falaProgresso = 0;
        falaNum = 0;
        intervalo = 0;

        SetText();
    }

    private void FixedUpdate()
    {
        if (legendas == null) return;
        if (legendas.falas == null) return;
        if (legendas.falas.Length == 0) return;

        falaProgresso += 0.02f;

        if (intervalo != 0)
        {
            text.text = "";

            intervalo += 0.02f;
            if (intervalo >= legendas.falas[falaNum].intervaloProxFala)
            {
                falaNum++;
                intervalo = 0;
                falaProgresso = 0;

                if (falaNum >= legendas.falas.Length)
                    SetLegendas(null);
                else
                    SetText();
            }
            return;
        }

        if(falaProgresso >= legendas.falas[falaNum].duracao)
        {
            audioSourceFinal.clip = legendas.falas[falaNum].somFim;
            audioSourceFinal.Play();
            falaProgresso = legendas.falas[falaNum].duracao;
            intervalo = 0.02f;
        }        

    }

    void SetText()
    {
        Fala fala = null;

        if (legendas != null) fala = legendas.falas[falaNum];

        if (fala != null)
        {
            audioSourceInicio.clip = fala.somInicio;
            audioSourceInicio.Play();
            text.text = fala.fala;
            text.color = fala.cor;
        }
        else
        {
            //audioSourceInicio.clip = null;
            //audioSourceInicio.Stop();
            text.text = "";
            text.color = Color.white;
        }
    }
}

[System.Serializable]
public class Legendas
{
    public Fala[] falas;
}

[System.Serializable]
public class Fala
{
    [TextArea]
    public string fala;
    [Range(0, 20)]
    public float duracao;
    public Color cor = Color.white;
    public AudioClip somInicio;
    public AudioClip somFim;
    [Range(0, 3)]
    public float intervaloProxFala;
}
