﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ZunbiMortoExplosao : MonoBehaviour
{
    [SerializeField]
    public GameObject Jogador;
    Rigidbody rb;
    int i;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        Jogador = GameObject.FindWithTag("Player");
    }

    // Start is called before the first frame update
    void Start()
    {

        i = Random.Range(0, 3);

        if(i == 0) rb.AddForce(Jogador.GetComponent<Mover>().cameraFPS.transform.up * 5000);

        if (i == 1)
        {
            rb.AddForce(Jogador.GetComponent<Mover>().cameraFPS.transform.up * 5000);
            rb.AddForce(transform.right * 2000);
        }

        if (i == 2)
        {
            rb.AddForce(Jogador.GetComponent<Mover>().cameraFPS.transform.up * 5000);
            rb.AddForce(-transform.right * 2000);
        }


    }

}
