﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.AI;
using UnityEngine;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class Default_IA : MonoBehaviour
{
    [HideInInspector]
    public NavMeshAgent agent;
    protected GameObject Jogador;
    public Transform target;
    protected Animator anim;

    public bool sobrevivente;

    public GameObject ZumbiMortoPrefab;
    public GameObject SobreviventeMortoPrefab; // faz todo sentido


    public float vida;

    [HideInInspector]
    public bool vendoPlayer;

    void Start()
    {

    }

    public void TomaDano(float dano)
    {
        vida -= dano;
        if (vida <= 0)
        {
            if(!sobrevivente) Instantiate(ZumbiMortoPrefab, transform.position, transform.rotation);
            if(sobrevivente) Instantiate(SobreviventeMortoPrefab, transform.position, transform.rotation);
           
            Destroy(gameObject);
        }
    }
}

