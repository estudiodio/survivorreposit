﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyEmT : MonoBehaviour
{

    public float tempoParaDestruir = 8;

    void Start()
    {
        Destroy(gameObject, tempoParaDestruir);
    }

}
