﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fov_Zombie : MonoBehaviour
{
    public Transform target;

    public Transform cabecaInimigo;
    public LayerMask layersDosInimigos;
    public List<Transform> inimigosVisiveis = new List<Transform>();
    List<Transform> listaTemporariaDeColisoes = new List<Transform>();
    [Space]
    
    [Range(1, 50)] public float distanciaDeVisao = 14;
    [Range(5, 180)]public float anguloDeVisao = 121;
    [Range(1, 50)] public float tempoDesistencia = 10;
    public bool desenharEsfera;
   
    float timerChecagem = 0;
    public float tempoDesistenciaAtual;
    bool contandoTempo;
    public  int i;

    void Start()
    {
        cabecaInimigo = gameObject.transform;

        timerChecagem = 0;
    }

    // Update is called once per frame
    void Update()
    {
        timerChecagem += Time.deltaTime;
        if (timerChecagem >= 0.1f)
        {
            timerChecagem = 0;
            ChecarInimigos();
        }

        if(inimigosVisiveis.Count > 0) target = inimigosVisiveis[0];

        if (contandoTempo) tempoDesistenciaAtual += Time.deltaTime;
    }


    private void ChecarInimigos()
    {

        Collider[] alvosNoRaioDeAlcance = Physics.OverlapSphere(cabecaInimigo.position, distanciaDeVisao, layersDosInimigos);
        foreach (Collider targetCollider in alvosNoRaioDeAlcance)
        {
            Transform alvo = targetCollider.transform;
            Vector3 direcaoDoAlvo = (alvo.position - cabecaInimigo.position).normalized;
            if (Vector3.Angle(cabecaInimigo.forward, direcaoDoAlvo) < (anguloDeVisao / 2.0f))
            {
                float distanciaDoAlvo = Vector3.Distance(transform.position, alvo.position);
                if (!Physics.Raycast(cabecaInimigo.position, direcaoDoAlvo, distanciaDoAlvo, ~layersDosInimigos))
                {
                    tempoDesistenciaAtual = 0;
                    contandoTempo = false;


                    if (!alvo.transform.IsChildOf(cabecaInimigo.root))
                    {
                        if (!listaTemporariaDeColisoes.Contains(alvo))
                        {
                            listaTemporariaDeColisoes.Add(alvo);
                        }
                        if (!inimigosVisiveis.Contains(alvo))
                        {
                            inimigosVisiveis.Add(alvo);

                            if (i == 0)
                            {
                                if (GetComponent<NewZombie>())
                                {
                                    GetComponent<AudioSource>().Play();
                                    Ve();
                                }

                                i = 1;
                            }

                            contandoTempo = false;
                            tempoDesistenciaAtual = 0;

                        }
                    }
                }
            }

        }
       /* for (int x = 0; x < inimigosVisiveis.Count; x++)
        {
             Debug.DrawLine(cabecaInimigo.position, inimigosVisiveis[x].position, Color.red);
        }*/

        //remove da lista inimigos que não estão visiveis
        for (int x = 0; x < inimigosVisiveis.Count; x++)
        {
            if (!listaTemporariaDeColisoes.Contains(inimigosVisiveis[x]))
            {
                contandoTempo = true;
               // 
                if (tempoDesistenciaAtual >= tempoDesistencia)
                {
                    inimigosVisiveis.Remove(inimigosVisiveis[x]);
                    contandoTempo = false;
                    tempoDesistenciaAtual = 0;
                    target = null;
                    if(i == 1)
                    {
                        NaoVe();
                        i = 0;
                    }

                }
            }
        }
        listaTemporariaDeColisoes.Clear();

    }

    public void Ve()
    {
        CG_Vitto.Instance.vistoPor += 1;
    }

    public void NaoVe()
    {
        CG_Vitto.Instance.vistoPor -= 1;
    }


#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {

        if (desenharEsfera)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawWireSphere(cabecaInimigo.position, distanciaDeVisao);


            Gizmos.color = Color.green;
            float angleToRay1 = (180.0f - anguloDeVisao) * 0.5f;
            float angleToRay2 = anguloDeVisao + (180.0f - anguloDeVisao) * 0.5f;
            Vector3 rayDirection1 = Quaternion.AngleAxis(angleToRay1, cabecaInimigo.up) * (-transform.right);
            Vector3 rayDirection2 = Quaternion.AngleAxis(angleToRay2, cabecaInimigo.up) * (-transform.right);
            Gizmos.DrawRay(cabecaInimigo.position, rayDirection1 * distanciaDeVisao);
            Gizmos.DrawRay(cabecaInimigo.position, rayDirection2 * distanciaDeVisao);
            //
            UnityEditor.Handles.color = Color.green;
            float angle = Vector3.Angle(transform.forward, rayDirection1);
            Vector3 pos = cabecaInimigo.position + (cabecaInimigo.forward * distanciaDeVisao * Mathf.Cos(angle * Mathf.Deg2Rad));
            UnityEditor.Handles.DrawWireDisc(pos, cabecaInimigo.transform.forward, distanciaDeVisao * Mathf.Sin(angle * Mathf.Deg2Rad));
        }
    }
#endif
}
