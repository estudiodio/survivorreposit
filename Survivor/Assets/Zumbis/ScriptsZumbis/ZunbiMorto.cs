﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ZunbiMorto : MonoBehaviour
{
    [SerializeField]
    public GameObject Jogador;
    Rigidbody rb;
    int i;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        Jogador = GameObject.FindWithTag("Player");
    }

    // Start is called before the first frame update
    void Start()
    {

        i = Random.Range(0, 3);

        if(i == 0) rb.AddForce(Jogador.GetComponent<Mover>().cameraFPS.transform.forward * 1700);

        if (i == 1)
        {
            rb.AddForce(Jogador.GetComponent<Mover>().cameraFPS.transform.forward * 1700);
            rb.AddForce(transform.right * 400);
        }

        if (i == 2)
        {
            rb.AddForce(Jogador.GetComponent<Mover>().cameraFPS.transform.forward * 1700);
            rb.AddForce(-transform.right * 400);
        }


    }

}
