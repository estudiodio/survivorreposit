﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public enum TipoPistola {Desert, USPS }
public enum TipoRifle {Ak_47, M4A4, G35G}
public enum TipoBastao {Bastao_1, Bastao_2, Bastao_3 }
public enum TipoFerramenta {Martelo, ChaveIng}

public class CG_Vitto : MonoBehaviour
{

    public int vida;

    [Header("Armas")]
    public GameObject SemArmas;   //
    public GameObject Rifle;   //
    public GameObject Pistola;//
    public GameObject Bastao;//
    public GameObject Ferramenta;//

    [Header("Arma Atual")]
    public TipoBastao BastaoAtual = TipoBastao.Bastao_1;
    public TipoPistola PistolaAtual = TipoPistola.Desert;
    public TipoRifle RifleAtual = TipoRifle.Ak_47;
    public TipoFerramenta FerramentaAtual = TipoFerramenta.Martelo;

    [Header("UI")]
    public Text mostra_Grandas;
    public GameObject Detectado;
    public GameObject TelaDano;
    public GameObject mira;
    public GameObject morte;
    public int N_Granadas = 3;
    public bool comInventario;
    public bool pausado;
    public bool sobAtaquePlayer;
    public int vistoPor = 0;
    int Pray;

    [Header("Volume")]
    [Range(0, 1)]
    public float sfxVolume;
    [Range(0, 1)]
    public float musicaVolume;

    public AudioSource CameraFps;
    public AudioClip MusicaBase;
    public AudioClip MusicaCombate;

    [Header("Mouse")]
    public Slider sencibilidade;
    public Text mostraSS;
    public Toggle inverte;



    [Header("Pause")]
    public GameObject TelaOpcoes;
    public Slider Sfx;
    public Slider music;


    [Header("Pontos")]
    [Tooltip("Pontos dentro do Jogador a ser seguido pelos Zumbis")]
    public Transform[] pontos;

    public int armaAtual = 1;

    public bool possuiPistola;
    public bool possuiRifle;
    public bool possuiBastao;
    public bool possuiFerramenta;

    public static CG_Vitto Instance;

    private void Awake()
    {
        Instance = this;
        Rifle.gameObject.transform.GetChild(1).GetComponent<Rifle_V>().SetInstance();
        Pistola.gameObject.transform.GetChild(1).GetComponent<Pistola_V>().SetInstance();
        mostra_Grandas = HUD.Instance.granada;

    }

    void Start()
    {
        vida = 100;
        morte.SetActive(false);
        Pause.Instance.Enable(false);
        pausado = false;
        HUD.Instance.AtualizarVitais();

        Detectado = CanvasManager.Instance.Detect;
        Detectado.SetActive(false);

        TelaDano = CanvasManager.Instance.TelaDeDano;
        TelaDano.SetActive(false);

        Sfx.value = 0.2f;
        music.value = 1;

        if (Rifle.activeInHierarchy) Rifle.gameObject.transform.GetChild(1).GetComponent<Rifle_V>().QRifle();
        if (Pistola.activeInHierarchy) Pistola.gameObject.transform.GetChild(1).GetComponent<Pistola_V>().QPistola();
        if (Bastao.activeInHierarchy) Bastao.gameObject.transform.GetChild(1).GetComponent<Corpo_Corpo>().QTaco();

        if (armaAtual == 0)
        {
            Pistola.SetActive(false);
            Rifle.SetActive(false);
            Bastao.SetActive(false);
            SemArmas.SetActive(true);
            armaAtual = -1;
        }
    }
    // Update is called once per frame
    void Update()
    {
        if (transform.position.y < -20) StartCoroutine("PraCima");

        comInventario = InventarioMochila.Instance.Enabled();

        UI();
        Pauser();
    }

    public void AtualizarArma()
    {
        switch (armaAtual)
        {
            case 0:
                SemArmas.SetActive(true);
                Pistola.SetActive(false);
                Rifle.SetActive(false);
                Bastao.SetActive(false);
                Ferramenta.SetActive(false);

                break;

            case 1:
                SemArmas.SetActive(false);
                Pistola.SetActive(true);
                Rifle.SetActive(false);
                Bastao.SetActive(false);
                Ferramenta.SetActive(false);
                Pistola.gameObject.transform.GetChild(1).GetComponent<Pistola_V>().QPistola();
                break;

            case 2:
                SemArmas.SetActive(false);
                Pistola.SetActive(false);
                Rifle.SetActive(true);
                Bastao.SetActive(false);
                Ferramenta.SetActive(false);
                Rifle.gameObject.transform.GetChild(1).GetComponent<Rifle_V>().QRifle();
                break;

            case 3:
                SemArmas.SetActive(false);
                Pistola.SetActive(false);
                Rifle.SetActive(false);
                Bastao.SetActive(true);
                Ferramenta.SetActive(false);
                Bastao.gameObject.transform.GetChild(1).GetComponent<Corpo_Corpo>().QTaco();
                break;

            case 4:
                SemArmas.SetActive(false);
                Pistola.SetActive(false);
                Rifle.SetActive(false);
                Bastao.SetActive(false);
                Ferramenta.SetActive(true);
                break;
        }
    }

    public void RecebeDano(int dano)
    {
        vida -= dano;
        
        HUD.Instance.AtualizarVitais();

        StartCoroutine("DesDano");

        if (vida <= 0) MorrePlayer();
    }
    void UI()
    {
        mostra_Grandas.text = "x" + N_Granadas;

        if (!comInventario)
            mira.SetActive(true);
        else
            mira.SetActive(false);

        mostraSS.text = "" + sencibilidade.value;

        if (vistoPor == 0) Detectado.SetActive(false);
        else Detectado.SetActive(true);
    }

    void MorrePlayer()
    {    
        vida = 0;
        morte.SetActive(true);
        Pause.Instance.Enable(true);
        pausado = true;
    }

    void Pauser()
    {
        if (Input.GetKeyDown(Mover.Instance.Pausar) && vida > 0)
        {
            Pause.Instance.SwitchEnable();
            if (TelaOpcoes.activeInHierarchy) TelaOpcoes.SetActive(false);
        }
    }

    public void Configurar()
    {
        TelaOpcoes.SetActive(true);
    }

    public void Volta()
    {
        TelaOpcoes.SetActive(false);
    }

    IEnumerator PraCima()
    {
        GetComponent<CharacterController>().enabled = false;
        transform.position = new Vector3(0, 2f, 122);
        yield return new WaitForSeconds(0.05f);
        GetComponent<CharacterController>().enabled = true;
    }

    IEnumerator DesDano()
    {
        TelaDano.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        TelaDano.SetActive(false);
    }
}