﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Corpo_Corpo : MonoBehaviour
{

    Animator anim;
    GameObject Jogador;

    [Header("Camera Da Arma")]
    public Camera gunCamera;

    [Header("Lista de Tacos")]
    public GameObject Taco_1;
    public GameObject Taco_2;
    public GameObject Taco_3;

    [Tooltip("Nome da arma atual, mostrada na interface do jogo.")]
    public string nome_Arma;
    private string nome_ArmaSalva;

    [Tooltip("Alterne o balanço da arma.")]
    bool oscilacaoDaArma = true;
    float intencidade_oscilacao = 0.02f;
    float max_oscilacao = 0.06f;
    float suavidade = 4.0f;

    private Vector3 initialSwayPosition;
    [Space]

    [Header("Configurações da Arma ")]
    float lastFired = 1;
    public float fireRate;
    public int   dano = 10;
    public bool automatica = false;

    bool correndo;
    bool inspecionando;
    [Space]

    public float inpactForceCaC = 16;
    [Space]

    public GameObject colisor;
    public bool batendo;
    public bool ferramenta = false;

    [Header("Configurações de Grenada")]
    public float grenadeSpawnDelay = 0.35f;

    [Header("Audio Source")]
    public AudioSource shootAudioSource;

    [Header("UI Components")]
    //public Image ImagemAtualArma;
    //public Text currentWeaponText;
    public Text TextoAtualMunicao;
    public Text totalAmmoText;
    public float tempoMira = 0.8f;

    [Tooltip("Objeto Mira")]
    public Image miraP;
    [Tooltip("Mira Padrão da Arma")]
    public Sprite SpriteMiraNeutra;

    [Tooltip("Sprite de construção. Só para Ferramentas.")]
    public Sprite SpriteConstroi;
    [Tooltip("Sprite de Destrução. Só para Ferramentas.")]
    public Sprite SpriteDestroi;



    [System.Serializable]
    public class prefabs
    {
        [Header("Prefabs")]
        public Transform grenadePrefab;
    }
    public prefabs Prefabs;

    [System.Serializable]
    public class spawnpoints
    {
        [Header("Spawnpoints")]
        public Transform grenadeSpawnPoint;
    }
    public spawnpoints Spawnpoints;

    [System.Serializable]
    public class soundClips
    {
        public AudioClip shootSound;
        public AudioClip takeOutSound;
        public AudioClip holsterSound;
    }
    public soundClips SoundClips;

    private void Awake()
    {
        miraP = CanvasManager.Instance.mira;
        //ImagemAtualArma = HUD.Instance.armaIcone;
        TextoAtualMunicao = HUD.Instance.municao;
        totalAmmoText = HUD.Instance.municaoMax;
        //currentWeaponText = HUD.Instance.armaNome;
        anim = GetComponent<Animator>();
        Jogador = GameObject.FindWithTag("Player");
    }

    private void OnEnable()
    {
        HUD.Instance.armaIcone.enabled = true;
        HUD.Instance.municaoParent.SetActive(false);
        miraP.GetComponent<Image>().sprite = SpriteMiraNeutra;
        miraP.GetComponent<Image>().color = new Color(0, 1, 0, 0.6f);
        if (ferramenta) miraP.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f, 0.6f);
    }

    private void Start()
    {
        nome_ArmaSalva = nome_Arma;
        //if (currentWeaponText)
            //currentWeaponText.text = nome_Arma;

        initialSwayPosition = transform.localPosition;

        shootAudioSource.clip = SoundClips.shootSound;

        miraP.GetComponent<Image>().sprite = SpriteMiraNeutra;
        miraP.GetComponent<Image>().color = new Color(0, 1, 0, 0.6f);
        if (ferramenta) miraP.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f, 0.6f);
    }

    private void LateUpdate()
    {
        if (oscilacaoDaArma == true)
        {
            float movementX = -Input.GetAxis("Mouse X") * intencidade_oscilacao;
            float movementY = -Input.GetAxis("Mouse Y") * intencidade_oscilacao;

            movementX = Mathf.Clamp
                (movementX, -max_oscilacao, max_oscilacao);
            movementY = Mathf.Clamp
                (movementY, -max_oscilacao, max_oscilacao);

            Vector3 finalSwayPosition = new Vector3
                (movementX, movementY, 0);
            transform.localPosition = Vector3.Lerp
                (transform.localPosition, finalSwayPosition +
                initialSwayPosition, Time.deltaTime * suavidade);
        }
    }

    private void Update()
    {
        if (!Jogador.GetComponent<CG_Vitto>().pausado)
        {
            anim.enabled = !Jogador.GetComponent<CG_Vitto>().comInventario;

            AnimationCheck();

            if (!Jogador.GetComponent<CG_Vitto>().comInventario)
            {

                if (Input.GetMouseButton(0) && !inspecionando && !correndo && automatica || Input.GetMouseButtonDown(0) && !inspecionando && !correndo && !automatica)
                {
                    if (Time.time - lastFired > 1 / fireRate)
                    {
                        lastFired = Time.time;

                        anim.Play("Fire", 0, 0f);

                        colisor.GetComponent<CuboDeDano>().danoCb = dano;
                        colisor.GetComponent<CuboDeDano>().inpactForce = inpactForceCaC;

                        if (!ferramenta) miraP.GetComponent<Image>().color = new Color(1, 0, 0, 0.6f);

                        if (ferramenta)
                        {
                            miraP.GetComponent<Image>().sprite = SpriteDestroi;
                            miraP.GetComponent<Image>().color = new Color(1, 0, 0, 0.6f);
                        }
                    }
                }
                else
                {
                    if (Input.GetMouseButtonUp(1))
                    {
                        miraP.GetComponent<Image>().sprite = SpriteMiraNeutra;
                        if (ferramenta) miraP.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f, 0.6f);
                    }
                    if (Input.GetMouseButtonUp(0) && !ferramenta) StartCoroutine("VoltaCor");
                }

                if (Input.GetMouseButton(1) && !inspecionando && !correndo && automatica || Input.GetMouseButtonDown(1) && !inspecionando && !correndo && !automatica)
                {
                    if (Time.time - lastFired > 1 / fireRate)
                    {
                        lastFired = Time.time;

                        anim.Play("Fire", 0, 0f);

                        colisor.GetComponent<CuboDeDano>().danoCb = 0;

                        if (ferramenta)
                        {
                            miraP.GetComponent<Image>().sprite = SpriteConstroi;
                            miraP.GetComponent<Image>().color = new Color(0, 1, 0, 0.6f);
                        }
                    }
                }
                else if (Input.GetMouseButtonUp(0) && ferramenta)
                {
                    miraP.GetComponent<Image>().sprite = SpriteMiraNeutra;
                    miraP.GetComponent<Image>().color = new Color(0.5f, 0.5f, 0.5f, 0.6f);
                }

                colisor.SetActive(batendo);

                if (Input.GetKeyDown(Jogador.GetComponent<Mover>().Inspecionar)) anim.SetTrigger("Inspect"); // Inspecionar


                if (Input.GetKeyDown(Jogador.GetComponent<Mover>().Granada) && Jogador.GetComponent<CG_Vitto>().N_Granadas > 0 && !inspecionando)
                {
                    StartCoroutine(GrenadeSpawnDelay());
                    anim.Play("GrenadeThrow", 0, 0.0f);
                    Jogador.GetComponent<CG_Vitto>().N_Granadas -= 1;
                    Debug.Log("Jogou");
                }

                if (Input.GetKey(Jogador.GetComponent<Mover>().Frente) && !correndo ||
                    Input.GetKey(Jogador.GetComponent<Mover>().Esquerda) && !correndo ||
                    Input.GetKey(Jogador.GetComponent<Mover>().Tras) && !correndo ||
                    Input.GetKey(Jogador.GetComponent<Mover>().Direita) && !correndo)
                {
                    anim.SetBool("Walk", true);
                }
                else
                {
                    anim.SetBool("Walk", false);
                }

                //Running when pressing down W and Left Shift key
                if ((Input.GetKey(Jogador.GetComponent<Mover>().Frente) && Input.GetKey(Jogador.GetComponent<Mover>().Correr)))
                {
                    correndo = true;
                }
                else
                {
                    correndo = false;
                }

                anim.SetBool("Run", correndo);

            }

            shootAudioSource.volume = Jogador.GetComponent<CG_Vitto>().sfxVolume;
        }
    }

    private IEnumerator GrenadeSpawnDelay()
    {
        yield return new WaitForSeconds(grenadeSpawnDelay);
        Instantiate(Prefabs.grenadePrefab,
            Spawnpoints.grenadeSpawnPoint.transform.position,
            Spawnpoints.grenadeSpawnPoint.transform.rotation);
    }

    private void AnimationCheck()
    {

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Inspect"))
        {
            inspecionando = true;
        }
        else
        {
            inspecionando = false;
        }
    }

    IEnumerator VoltaCor()
    {
        yield return new WaitForSeconds(tempoMira);
        miraP.GetComponent<Image>().color = new Color(0, 1, 0, 0.6f);
    }

    public void QTaco()
    {

        if (gameObject.activeInHierarchy) anim.Play("Draw", 0, 0f);

        if (Jogador.GetComponent<CG_Vitto>().BastaoAtual == TipoBastao.Bastao_1)
        {
            Taco_1.SetActive(true);
            Taco_2.SetActive(false);
            Taco_3.SetActive(false);

            Definicoes_Taco1();
        }

        if (Jogador.GetComponent<CG_Vitto>().BastaoAtual == TipoBastao.Bastao_2)
        {
            Taco_1.SetActive(false);
            Taco_2.SetActive(true);
            Taco_3.SetActive(false);

            Definicoes_Taco2();
        }

        if (Jogador.GetComponent<CG_Vitto>().BastaoAtual == TipoBastao.Bastao_3)
        {
            Taco_1.SetActive(false);
            Taco_2.SetActive(false);
            Taco_3.SetActive(true);

            Definicoes_Taco3();
        }
    }

    public void Definicoes_Taco1()
    {
        dano = 25;
        inpactForceCaC = 11;
    }
    public void Definicoes_Taco2()
    {
        dano = 30;
        inpactForceCaC = 12;
    }
    public void Definicoes_Taco3()
    {
        dano = 40;
        inpactForceCaC = 14;
    }

    public void Bater()
    {
        batendo = true;
    }

    public void Desbater()
    {
        batendo = false;
    }
}
