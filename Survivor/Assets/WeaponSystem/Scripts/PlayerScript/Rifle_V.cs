﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Rifle_V : MonoBehaviour
{
    public static Rifle_V Instance;

    #region VariaveisClass
    [Header("Configurações da Arma ")]//______________________Configurações da Arma________________________________
    [Range(0, 100)] public float impactForce = 3.5f;
    [Range(0, 100)] public float range = 30;
    [Range(0, 100)] public int dano = 30;
    public float fireRate = 0.1f;
    float currentRate;
    float sliderBackTimer = 1.58f;
    bool hasStartedSliderBack;
    [Space]

    [Header("Camera Da Arma")]//__________________________________Camera da Arma___________________________________
    public Camera gunCamera;
    [Range(0, 100)] public float fovSpeed = 15.0f;
    [Range(0, 100)] public float fov = 40.0f;
    [Range(0, 100)] public float aimFov = 15.0f;
    [Space]

    [Header("Opçoes de Munição")]//_____________________________Opçoes de Munição___________________________________
    public int municao;
    public int municaoRestante = 0;
    public int maxDeMunicaoPente;
    [Space]

    [Header("Configurações de Grenada")]//___________________Configurações de Grenada________________________________
    public float grenadeSpawnDelay = 0.35f;
    public Transform grenadeSpawnPoint;
    [Space]

    [Header("Configurações do Muzzleflash")]//_____________Configurações do Muzzleflash______________________________
    public GameObject muzzleParticles;
    public ParticleSystem sparkParticles;
    [Space]

    [Header("Configurações Muzzleflash Light")]//________Configurações Muzzleflash Light_____________________________
    public Light muzzleflashLight;
    public float lightDuration = 0.02f;
    [Space]

    [Header("Layers")]//____________________________________________Layers___________________________________________
    public LayerMask PlayerMask;
    public LayerMask layer_Zumbi;
    public LayerMask layer_Sobrevivente;
    [Space]

    [Header("Lista de  Armas")]//________________________________Lista de Rifles____________________________________
    public GameObject ak_47;
    public GameObject g35g;
    public GameObject m4a4;
    [Space]

    [Header("Ponto De Particula")]//______________________________ParticlePoint_______________________________________
    public Transform AkPoint;
    public Transform M4Point;
    public Transform g3Point;
    [Space]

    [Header("Oscilação da Arma")]//___________________________Oscilação da Arma_________-__________________________
    public float intencidade_oscilacao = 0.02f;
    public float max_oscilacao = 0.06f;
    public float suavidade = 4.0f;
    Vector3 initialSwayPosition;
    [Space]

    [Header("UI Componentes")]//___________________________________UI Components______________________________________
    public float tempoMira = 0.8f;
    public Sprite SpriteMiraNeutra;
    Text municaoAtualText;
    Text totalAmmoText;
    Image miraP;

    [Space]

    [Header("Prefabs")]//______________________________________________Prfabas_______________________________________
    public Transform grenadePrefab;
    public GameObject impactBlood;
    [Space]

    [Header("Audio Source")]//______________________________________Audio Source_____________________________________
    public AudioSource mainAudioSource;
    public AudioSource shootAudioSource;
    [Space]

    [Header("Sound Clips")]//_________________________________________soudClips______________________________________
    public AudioClip shootSound;
    public AudioClip takeOutSound;
    public AudioClip holsterSound;
    public AudioClip reloadSoundOutOfAmmo;
    public AudioClip reloadSoundAmmoLeft;
    public AudioClip aimSound;
    [Space]

    //[Header("Debugs")]//_________________________________ ____________Debugs________________________________________
    bool recarregando;
    bool correndo;
    bool mirando;
    bool inspecionando;
    bool aliado;
    bool podeAtirar;
    bool soundHasPlayed = false;

    Animator anim;
    #endregion

    public void SetInstance()
    {
        Instance = this;
    }

    void Awake()
    {
        miraP = CanvasManager.Instance.mira;
        municaoAtualText = HUD.Instance.municao;
        totalAmmoText = HUD.Instance.municaoMax;

        anim = GetComponent<Animator>();

        if (muzzleflashLight)
            muzzleflashLight.enabled = false;
    }

    void Start()
    {
        initialSwayPosition = transform.localPosition;
        shootAudioSource.clip = shootSound;

        muzzleParticles.SetActive(false);
        currentRate = fireRate;

        miraP.GetComponent<Image>().sprite = SpriteMiraNeutra;
        miraP.GetComponent<Image>().color = new Color(0, 1, 0, 0.6f);
    }

    void OnEnable()
    {
        HUD.Instance.armaIcone.enabled = true;
        HUD.Instance.municaoParent.SetActive(true);
        miraP.GetComponent<Image>().sprite = SpriteMiraNeutra;
        miraP.GetComponent<Image>().color = new Color(0, 1, 0, 0.6f);
    }

    void LateUpdate()
    {
        float movementX = -Input.GetAxis("Mouse X") * intencidade_oscilacao;
        float movementY = -Input.GetAxis("Mouse Y") * intencidade_oscilacao;

        movementX = Mathf.Clamp
            (movementX, -max_oscilacao, max_oscilacao);
        movementY = Mathf.Clamp
            (movementY, -max_oscilacao, max_oscilacao);

        Vector3 finalSwayPosition = new Vector3
            (movementX, movementY, 0);
        transform.localPosition = Vector3.Lerp
            (transform.localPosition, finalSwayPosition +
            initialSwayPosition, Time.deltaTime * suavidade);
    }

    void Update()
    {
        if (!CG_Vitto.Instance.pausado && !CG_Vitto.Instance.comInventario)
        {

            Hud();
            Atirar();
            Mirando();
            Animations();
            Recarregar();
            JogarGranada();
            AnimationCheck();
        }

        if (!CG_Vitto.Instance.pausado && !CG_Vitto.Instance.comInventario) anim.enabled = true;
        else anim.enabled = false;

        mainAudioSource.volume = CG_Vitto.Instance.sfxVolume;
        shootAudioSource.volume = CG_Vitto.Instance.sfxVolume;
    }



    void Hud()
    {
        municaoAtualText.text = municao.ToString();
        totalAmmoText.text = municaoRestante.ToString();
    }

    void Mirando()
    {
        if (Input.GetKey(Mover.Instance.M_2) && !recarregando && !correndo)
        {
            gunCamera.fieldOfView = Mathf.Lerp(gunCamera.fieldOfView, aimFov, fovSpeed * Time.deltaTime);

            mirando = true;

            if (!soundHasPlayed)
            {
                mainAudioSource.clip = aimSound;
                mainAudioSource.Play();

                soundHasPlayed = true;
            }

        }
        else
        {
            gunCamera.fieldOfView = Mathf.Lerp(gunCamera.fieldOfView, fov, fovSpeed * Time.deltaTime);

            mirando = false;
        }

        miraP.enabled = !mirando;
    }

    void Atirar()
    {

        if (!podeAtirar) currentRate += Time.deltaTime;

        if (currentRate >= fireRate) podeAtirar = true;

        if (Input.GetKey(Mover.Instance.M_1) && municao > 0 && !recarregando && !correndo && !aliado && podeAtirar)
        {
            if (mirando) anim.Play("Aim Fire", 0, 0f);
            else anim.Play("Fire", 0, 0f);

            currentRate = 0;
            podeAtirar = false;

            municao -= 1;
            InventarioRapido.Instance.Atirou();

            shootAudioSource.clip = shootSound;
            shootAudioSource.Play();

            StartCoroutine(MuzzleFlashLight());

            sparkParticles.Emit(10);

            if (Physics.Raycast(gunCamera.transform.position, gunCamera.transform.forward, out RaycastHit hit, range, ~PlayerMask))
            {
                if (hit.transform.CompareTag("Zombie"))
                {
                    hit.transform.GetComponent<Rigidbody>().velocity = transform.forward * impactForce;
                    Instantiate(impactBlood, hit.point, Quaternion.LookRotation(hit.normal), hit.transform.parent);

                    StartCoroutine("VoltaCor");
                }

                if (hit.transform.CompareTag("Morto"))
                {
                    hit.transform.GetComponent<Rigidbody>().velocity = transform.forward * impactForce;
                }

                if (hit.transform.name == "Granada(Clone)")
                {
                    hit.transform.GetComponent<GrenadeScript>().Explosao();
                    StartCoroutine("VoltaCor");
                }
            }
        }

        aliado = Physics.Raycast(gunCamera.transform.position, gunCamera.transform.forward, out RaycastHit hit2, range, layer_Sobrevivente);
        if (municaoRestante < 0) municaoRestante = 0;
    }

    void Animations()
    {
        anim.SetBool("Aim", mirando);

        if (Input.GetKeyDown(Mover.Instance.Inspecionar)) anim.SetTrigger("Inspect"); // Inspecionar

        if (Input.GetKey(Mover.Instance.Frente) && !correndo || Input.GetKey(Mover.Instance.Esquerda) && !correndo || Input.GetKey(Mover.Instance.Tras) && !correndo || Input.GetKey(Mover.Instance.Direita) && !correndo)
        {
            anim.SetBool("Walk", true);
        }
        else
        {
            anim.SetBool("Walk", false);
        }

        if (Input.GetKey(Mover.Instance.Frente) && Input.GetKey(Mover.Instance.Correr)) correndo = true;
        else correndo = false;

        anim.SetBool("Run", correndo);


        if (municao == 0)
        {

            anim.SetBool("Out Of Ammo Slider", true);
            anim.SetLayerWeight(1, 1.0f);
        }
        else anim.SetLayerWeight(1, 0.0f);

    }

    void Recarregar()
    {
        if (Input.GetKeyDown(Mover.Instance.Recarregar) && !recarregando)
        {
            if (!hasStartedSliderBack)
            {
                hasStartedSliderBack = true;
                StartCoroutine(HandgunSliderBackDelay());
            }

            if (municaoRestante == 0)
            {
                 anim.Play("Reload Out Of Ammo", 0, 0f);

                mainAudioSource.clip = reloadSoundOutOfAmmo;
                mainAudioSource.Play();
            }


            if (municaoRestante > 0 && municao < maxDeMunicaoPente)
            {
                anim.Play("Reload Ammo Left", 0, 0f);
                mainAudioSource.clip = reloadSoundAmmoLeft;
                mainAudioSource.Play();
            }


            int municaoPuxada = InventarioMochila.Instance.ProcurarMunicao(TipoMunicao.Media, Mathf.Clamp(maxDeMunicaoPente - municao, 0, maxDeMunicaoPente));
            municao += municaoPuxada;
            InventarioRapido.Instance.ferramentaAtual.quantidade += municaoPuxada;
            municaoRestante = InventarioMochila.Instance.totalMunicaoMedia;

        }
    }

    void JogarGranada()
    {
        if (Input.GetKeyDown(Mover.Instance.Granada) && CG_Vitto.Instance.N_Granadas > 0)
            StartCoroutine(GrenadeSpawnDelay());
    }

    void AnimationCheck()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Reload Out Of Ammo") ||
            anim.GetCurrentAnimatorStateInfo(0).IsName("Reload Ammo Left"))
        {
            recarregando = true;
        }
        else
        {
            recarregando = false;
        }

        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Inspect"))
        {
            inspecionando = true;
        }
        else
        {
            inspecionando = false;
        }
    }

    IEnumerator VoltaCor()
    {
        miraP.GetComponent<Image>().color = new Color(1, 0, 0, 0.6f);
        yield return new WaitForSeconds(tempoMira);
        miraP.GetComponent<Image>().color = new Color(0, 1, 0, 0.6f);
    }

    IEnumerator MuzzleFlashLight()
    {
        muzzleParticles.SetActive(true);
        muzzleParticles.GetComponent<ParticleSystem>().Play();
        muzzleParticles.GetComponent<ParticleSystem>().Emit(1);

        muzzleflashLight.enabled = true;
        yield return new WaitForSeconds(lightDuration);
        muzzleflashLight.enabled = false;
        muzzleParticles.SetActive(false);


    }

    IEnumerator GrenadeSpawnDelay()
    {
        anim.Play("GrenadeThrow", 0, 0.0f);
        CG_Vitto.Instance.N_Granadas -= 1;
        yield return new WaitForSeconds(grenadeSpawnDelay);
        Instantiate(grenadePrefab, grenadeSpawnPoint.transform.position, grenadeSpawnPoint.transform.rotation);
    }

    IEnumerator HandgunSliderBackDelay()
    {
        yield return new WaitForSeconds(sliderBackTimer);
        anim.SetBool("Out Of Ammo Slider", false);
        anim.SetLayerWeight(1, 0.0f);

        hasStartedSliderBack = false;
    }

  
    [HideInInspector] public int magAKmax = 30;
    public void Definicoes_Ak()
    {
        range = 50;
        dano = 20;
       // fireRate = 0.08f;
        maxDeMunicaoPente = magAKmax;
        impactForce = 3.5f;
        tempoMira = 0.03f;
    }

    [HideInInspector] public int magM4max = 30;
    public void Definicoes_M4()
    {
        range = 60;
        dano = 25;
       // fireRate = 0.08f;
        maxDeMunicaoPente = magM4max;
        impactForce = 3.2f;
        tempoMira = 0.04f;
    }

    [HideInInspector] public int magG3Smax = 25;
    public void Definicoes_G3S()
    {
        range = 100;
        dano = 50;
       // fireRate = 0.08f;
        maxDeMunicaoPente = magG3Smax;
        impactForce = 5f;
        tempoMira = 0.04f;
    } 
    

    public void QRifle()
    {

        if (gameObject.activeInHierarchy) anim.Play("Draw", 0, 0f);

        if (CG_Vitto.Instance.RifleAtual == TipoRifle.Ak_47)
        {
            ak_47.SetActive(true);
            g35g.SetActive(false);
            m4a4.SetActive(false);
            muzzleParticles.transform.position = AkPoint.position;

            Definicoes_Ak();
        }

        if (CG_Vitto.Instance.RifleAtual == TipoRifle.G35G)
        {
            ak_47.SetActive(false);
            g35g.SetActive(true);
            m4a4.SetActive(false);
            muzzleParticles.transform.position =  g3Point.position;

            Definicoes_G3S();
        }

        if (CG_Vitto.Instance.RifleAtual == TipoRifle.M4A4)
        {
            ak_47.SetActive(false);
            g35g.SetActive(false);
            m4a4.SetActive(true);
            muzzleParticles.transform.position = M4Point.position;

            Definicoes_M4();
        }

    }

}
