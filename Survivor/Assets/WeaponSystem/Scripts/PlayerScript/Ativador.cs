﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ativador : MonoBehaviour
{

    public LayerMask layer_Player; 

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(Mover.Instance.Interagir))
        {
            if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, 5, ~layer_Player))
            {
                if(hit.transform.CompareTag("Armadilha") && hit.transform.GetComponent<Armadilha_Urso>().presa)
                {
                    hit.transform.GetComponent<Armadilha_Urso>().Armar();
                }
            }
        }
    }
}
