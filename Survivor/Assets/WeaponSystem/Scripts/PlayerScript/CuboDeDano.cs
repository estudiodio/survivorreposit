﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CuboDeDano : MonoBehaviour
{

    public int danoCb;
    public float inpactForce;
    public bool zumbi = false;
    public bool sobrevivente = false;
    public bool stay = false;
    public AudioClip pancada;
    AudioSource _audio;


    private void Start()
    {
       if(GetComponent<AudioSource>()) _audio = GetComponent<AudioSource>();
    }

    private void OnTriggerEnter(Collider other)
    {

        if (GetComponent<AudioSource>()) _audio.volume = CG_Vitto.Instance.sfxVolume;

        if (!stay)
        {

            if (other.CompareTag("Zombie") && !zumbi)
            {
                _audio.clip = pancada;
                _audio.Play();

                if (!sobrevivente)
                {
                    other.GetComponent<Rigidbody>().velocity += -transform.right * 1;
                }
            }

            if  (zumbi)
            {
                if (other.CompareTag("Player"))
                {
                    other.GetComponent<CG_Vitto>().RecebeDano(danoCb);
                  //  Debug.Log("Tomou");
                }


            }
        }
    }
}
