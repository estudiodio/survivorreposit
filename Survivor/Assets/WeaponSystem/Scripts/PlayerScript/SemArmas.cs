﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SemArmas : MonoBehaviour
{

    Animator anim;
    GameObject Jogador;

    [Header("Camera Da Arma")]
    public Camera gunCamera;
    [Space]

    [Header("UI")]
    [Tooltip("Nome da arma atual, mostrada na interface do jogo.")]
    public string nome_Arma;
    private string nome_ArmaSalva;
    [Space]

    bool correndo;
    bool mirando;

    [Header("Configurações de Grenada")]
    public float grenadeSpawnDelay = 0.35f;

    [Header("UI Components")]
    //public Image ImagemAtualArma;
    //public Text TextoAtualMunicao;
    //public Text totalAmmoText;
    //public Text currentWeaponText;

    public Image miraP;
    [Tooltip("Mira Padrão da Arma")]
    public Sprite SpriteMiraNeutra;


    [System.Serializable]
    public class prefabs
    {
        [Header("Prefabs")]
        public Transform grenadePrefab;
    }
    public prefabs Prefabs;

    [System.Serializable]
    public class spawnpoints
    {
        public Transform grenadeSpawnPoint;
    }
    public spawnpoints Spawnpoints;

    private void Awake()
    {
        miraP = CanvasManager.Instance.mira;
        //ImagemAtualArma = HUD.Instance.armaIcone;
        //TextoAtualMunicao = HUD.Instance.municao;
        //totalAmmoText = HUD.Instance.municaoMax;
        //currentWeaponText = HUD.Instance.armaNome;
        anim = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        HUD.Instance.armaIcone.enabled = false;
        HUD.Instance.municaoParent.SetActive(false);
        miraP.GetComponent<Image>().sprite = SpriteMiraNeutra;
        miraP.GetComponent<Image>().color = new Color(0, 1, 0, 0.6f);
    }

    private void Start()
    {
        nome_ArmaSalva = nome_Arma;
        //if (currentWeaponText)
            //currentWeaponText.text = nome_Arma;

        Jogador = GameObject.FindWithTag("Player");

        miraP.GetComponent<Image>().sprite = SpriteMiraNeutra;
        miraP.GetComponent<Image>().color = new Color(0, 1, 0, 0.6f);

    }

    private void Update()
    {
        if (!Jogador.GetComponent<CG_Vitto>().pausado && !Jogador.GetComponent<CG_Vitto>().comInventario)
        {
            {
                anim.enabled = !Jogador.GetComponent<CG_Vitto>().comInventario;

                if (Input.GetKeyDown(KeyCode.G) && Jogador.GetComponent<CG_Vitto>().N_Granadas > 0) //&& !inspecionando)
                {
                    StartCoroutine(GrenadeSpawnDelay());
                    anim.Play("GrenadeThrow", 0, 0.0f);
                    Jogador.GetComponent<CG_Vitto>().N_Granadas -= 1;
                    //Debug.Log("Jogou");
                }

                if (Input.GetKey(KeyCode.W) && !correndo ||
                    Input.GetKey(KeyCode.A) && !correndo ||
                    Input.GetKey(KeyCode.S) && !correndo ||
                    Input.GetKey(KeyCode.D) && !correndo)
                {
                    anim.SetBool("Walk", true);
                }
                else
                {
                    anim.SetBool("Walk", false);
                }

                if ((Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift)))
                {
                    correndo = true;
                }
                else
                {
                    correndo = false;
                }

                anim.SetBool("Run", correndo);
            }

        }

    }

    private IEnumerator GrenadeSpawnDelay()
    {
        yield return new WaitForSeconds(grenadeSpawnDelay);
        Instantiate(Prefabs.grenadePrefab,
            Spawnpoints.grenadeSpawnPoint.transform.position,
            Spawnpoints.grenadeSpawnPoint.transform.rotation);
    }

}
