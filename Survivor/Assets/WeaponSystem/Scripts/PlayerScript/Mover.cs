﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(AudioSource))]
public class Mover : MonoBehaviour
{
    public static Mover Instance;

    public float speed;
    public float walkingSpeed = 6.0f;
    public float runningSpeed = 10;
    public float rotationSpeed = 3;
    public float JumpForce = 9;
    public GameObject cameraFPS;
    public GameObject modelo;
    public bool inverterCamera;
    int som;

    public AudioClip walkingSound;
    public AudioClip runningSound;

    public float barulho;
    public LayerMask layer_Zumbi;

    [Header("Inputs")]

    public KeyCode Frente = KeyCode.W;
    public KeyCode Tras = KeyCode.S;
    public KeyCode Esquerda = KeyCode.A;
    public KeyCode Direita = KeyCode.D;
    public KeyCode Correr = KeyCode.LeftShift;
    public KeyCode Agachar = KeyCode.LeftControl;
    public KeyCode Pular = KeyCode.Space;
    public KeyCode Recarregar = KeyCode.R;
    public KeyCode Inspecionar = KeyCode.F;
    public KeyCode Granada = KeyCode.G;
    public KeyCode M_1 = KeyCode.Mouse0;
    public KeyCode M_2 = KeyCode.Mouse1;

    public KeyCode PegarItem = KeyCode.E;
    public KeyCode InteragirMesaCraft = KeyCode.E;
    public KeyCode InteragirLootBox = KeyCode.E;
    public KeyCode Interagir = KeyCode.E;
    public KeyCode Espera = KeyCode.T;
    public KeyCode MandaAtacar = KeyCode.Y;
    public KeyCode Pausar = KeyCode.Escape;

    [HideInInspector] public float FrenteTras;
    [HideInInspector] public float DireitaEsquerda;
    float pushForce;

    Vector3 moveDirection = Vector3.zero;
    CharacterController controller;
    AudioSource _audioSource;
    float rotacaoX = 0.0f, rotacaoY = 0.0f;

    Player playerMilena;

    private void Awake()
    {
        #region Instance
        if (Instance == null)
        {
            Instance = this;
        }
        else
        if (Instance != this)
            DestroyImmediate(gameObject);
        #endregion
        playerMilena = Player.Instance;

        controller = GetComponent<CharacterController>();

        speed = walkingSpeed;

        _audioSource = GetComponent<AudioSource>();
        _audioSource.clip = walkingSound;
        _audioSource.loop = true;

        cameraFPS.GetComponent<AudioSource>().Play();
    }

    void Update()
    {
        if (!GetComponent<CG_Vitto>().pausado)
        {
            Vector3 direcaoFrente = new Vector3(cameraFPS.transform.forward.x, 0, cameraFPS.transform.forward.z);
            Vector3 direcaoLado = new Vector3(cameraFPS.transform.right.x, 0, cameraFPS.transform.right.z);
            direcaoFrente.Normalize();
            direcaoLado.Normalize();

            direcaoFrente *= FrenteTras;
            direcaoLado *= DireitaEsquerda;
            Vector3 direcFinal = direcaoFrente + direcaoLado;

            if (direcFinal.sqrMagnitude > 1) direcFinal.Normalize();

            if (!GetComponent<CG_Vitto>().comInventario)
            {
                if (controller.isGrounded)
                {
                    moveDirection = new Vector3(direcFinal.x, 0, direcFinal.z);
                    moveDirection *= speed;

                    AgacharV();
                    CorrerV();
                    PularV();
                }
            }

            if (GetComponent<CG_Vitto>().comInventario) moveDirection = new Vector3(0, 0, 0);

            moveDirection.y -= 20.0f * Time.deltaTime;
            controller.Move(moveDirection * Time.deltaTime);

            if (!GetComponent<CG_Vitto>().comInventario)
            {
                CameraPrimeiraPessoa();
            }

            MyVertical();
            MyHorizontal();
            PlayFootstepSounds();
        }

        _audioSource.volume = GetComponent<CG_Vitto>().sfxVolume;
        cameraFPS.GetComponent<AudioSource>().volume = GetComponent<CG_Vitto>().musicaVolume;

        rotationSpeed = GetComponent<CG_Vitto>().sencibilidade.value;
        inverterCamera = GetComponent<CG_Vitto>().inverte.isOn; 

    }

    void CameraPrimeiraPessoa()
    {
        rotacaoX += Input.GetAxis("Mouse X") * rotationSpeed;
        if (!inverterCamera) rotacaoY += Input.GetAxis("Mouse Y") * rotationSpeed;
        if (inverterCamera) rotacaoY += -Input.GetAxis("Mouse Y") * rotationSpeed;
        rotacaoX = ClampAngleFPS(rotacaoX, -360, 360);
        rotacaoY = ClampAngleFPS(rotacaoY, -75, 80);
        Quaternion xQuaternion = Quaternion.AngleAxis(rotacaoX, Vector3.up);
        Quaternion yQuaternion = Quaternion.AngleAxis(rotacaoY, -Vector3.right);
        Quaternion rotacFinal = Quaternion.identity * xQuaternion * yQuaternion;
        Quaternion rotacModelo = Quaternion.identity * xQuaternion;
        cameraFPS.transform.localRotation = rotacFinal; // Quaternion.Lerp(cameraFPS.transform.localRotation, rotacFinal, Time.deltaTime * 10.0f);
        modelo.transform.localRotation = rotacModelo; //Quaternion.Euler( new Vector3 (rotacFinal.x, rotacFinal.y, rotacFinal.z));

    }

    void PlayFootstepSounds()
    {
        if (controller.isGrounded && controller.velocity.sqrMagnitude > 0.1f)
        {
            _audioSource.clip = Input.GetKey(Correr) ? runningSound : walkingSound;
            if (!_audioSource.isPlaying)
            {
                _audioSource.Play();
            }
        }
        else
        {
            if (_audioSource.isPlaying)
            {
                _audioSource.Pause();
            }
        }
    }

    void AgacharV()
    {
        if (Input.GetKey(Agachar))      //INPUT
        {
            controller.height = 1;
            speed = 2;
            som = 1;
        }
        if (Input.GetKeyUp(Agachar))       //INPUT
        {
            speed = walkingSpeed;
            som = 2;
            moveDirection.y = 1;
        }
        if (controller.height < 2 && !Input.GetKey(Agachar))      //INPUT
        {
            controller.height = Mathf.MoveTowards(controller.height, 2, 5f * Time.deltaTime);
        }
    }


    Vitais CorrerVitais = new Vitais()
    {
        energia = -.6f,
        fome = -.012f,
        sede = -.01f,
    };


    Vitais AndarVitais = new Vitais()
    {
        energia = .25f,
        fome = .005f,
        sede = -.008f,
    };

    void CorrerV()
    {
        if (Input.GetKey(Correr) && playerMilena.vitais.energia > Mathf.Abs(CorrerVitais.energia))
        {
            speed = runningSpeed;
            som = 3;
            playerMilena.VitaisAdicionar(CorrerVitais);
        }
        else
            playerMilena.VitaisAdicionar(AndarVitais);

        if (Input.GetKeyUp(Correr) && speed == runningSpeed)
        {
            speed = walkingSpeed;
            som = 2;
        }
    }

    void PularV()
    {
        if (Input.GetKeyDown(Pular))
        {
            moveDirection.y = JumpForce;
        }
    }

    void MyVertical()
    {
        if (Input.GetKey(Frente) && !Input.GetKey(Tras)) FrenteTras = 1;
        if (Input.GetKey(Tras) && !Input.GetKey(Frente)) FrenteTras = -1;
        if (!Input.GetKey(Tras) && !Input.GetKey(Frente)) FrenteTras = 0;
    }

    void MyHorizontal()
    {
        if (Input.GetKey(Direita) && !Input.GetKey(Esquerda)) DireitaEsquerda = 0.4f;
        if (Input.GetKey(Esquerda) && !Input.GetKey(Direita)) DireitaEsquerda = -0.4f;
        if (!Input.GetKey(Esquerda) && !Input.GetKey(Direita)) DireitaEsquerda = 0;
    }

    float ClampAngleFPS(float angulo, float min, float max)
    {
        if (angulo < -360)
        {
            angulo += 360;
        }
        if (angulo > 360)
        {
            angulo -= 360;
        }
        return Mathf.Clamp(angulo, min, max);
    }

    public void TravaPlayer()
    {
        controller.enabled = !controller.enabled;

    }

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Rigidbody rbObistaculos = hit.collider.attachedRigidbody;

        if (rbObistaculos == null || rbObistaculos.isKinematic)
            return;

        if (hit.moveDirection.y < -0.3f)
            return;

        Vector3 pushDir = new Vector3(hit.moveDirection.x, 0, hit.moveDirection.z);

        if (rbObistaculos.mass < 1) pushForce = 5;
        if (rbObistaculos.mass == 1) pushForce = 3;
        if (rbObistaculos.mass > 1 && rbObistaculos.mass < 3) pushForce = 1;
        if (rbObistaculos.mass >= 3) pushForce = 0;

        rbObistaculos.velocity = pushDir * pushForce;

    }


    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, barulho);
    }
}
