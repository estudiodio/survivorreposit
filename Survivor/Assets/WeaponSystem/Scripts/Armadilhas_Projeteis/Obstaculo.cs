﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class Obstaculo : MonoBehaviour
{

    public int vida;
    public int danoRecebido = 5;
    public float fireRate = 1;
    [Space]

    public bool instancia;
    public GameObject InstanciarPrefab;


    float currentRate;


    // Start is called before the first frame update
    void Start()
    {
        currentRate = fireRate;
        vida = 100;
    }

    private void OnCollisionStay(Collision collision)
    {
        if(collision.transform.CompareTag("Zombie"))
        {
            currentRate += Time.deltaTime;

            if(currentRate >= fireRate)
            {
                Destruindo(danoRecebido);

                currentRate = 0;
            }
        }

    }

    public void Destruindo(int dano)
    {
        vida -= dano;

        if (vida <= 0) Destruida();
    }

    void Destruida()
    {
        if (instancia) Instantiate(InstanciarPrefab, transform.position, transform.rotation);

        Destroy(gameObject);
    }

}
