﻿using UnityEngine;
using System.Collections;

public class GrenadeScript : MonoBehaviour {

    [Header("Timer")]
    public float TempoExplosao = 5.0f;

    [Header("Explosion Prefabs")]
    public Transform explosionPrefab;

    [Header("Explosãp")]
    public float raio = 25.0F;
    public float forca = 350.0F;

    [Header("Força de Arremeço")]
    public float minimumForce = 1500.0f;
    public float maximumForce = 2500.0f;
    private float throwForce;

    int i;

    [Header("Audio")]
    public AudioSource impactSound;


    public GameObject Morto;
    int nZ;
    GameObject Jogador;

    private void Awake()
    {
        throwForce = Random.Range(minimumForce, maximumForce);


        GetComponent<Rigidbody>().AddRelativeTorque
           (Random.Range(500, 1500), //X Axis
            Random.Range(0, 0),          //Y Axis
            Random.Range(0, 0)           //Z Axis
            * Time.deltaTime * 5000);
    }

    private void Start()
    {
        GetComponent<Rigidbody>().AddForce(gameObject.transform.forward * throwForce);

        Jogador = GameObject.FindWithTag("Player");
        impactSound.volume = Jogador.GetComponent<CG_Vitto>().sfxVolume;

        StartCoroutine(ExplosionTimer());
        i = 0;
    }

    private void OnCollisionEnter(Collision collision)
    {
        impactSound.Play();
    }

    private IEnumerator ExplosionTimer()
    {
        yield return new WaitForSeconds(TempoExplosao);

        Explosao();

    }

    public void Explosao()
    {
        RaycastHit checkGround;
        if (Physics.Raycast(transform.position, Vector3.down, out checkGround, 50))
        {
            Instantiate(explosionPrefab, checkGround.point,
                Quaternion.FromToRotation(Vector3.forward, checkGround.normal));
        }

        Vector3 explosionPos = transform.position;

        Collider[] colliders = Physics.OverlapSphere(explosionPos, raio);
        Collider[] colliderSom = Physics.OverlapSphere(explosionPos, raio *2);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = hit.GetComponent<Rigidbody>();

            if (rb != null && hit.GetComponent<Collider>().tag != "Player")
                rb.AddExplosionForce(forca * 5, explosionPos, raio, 3.0F);

            if (hit.GetComponent<Collider>().CompareTag("Zombie"))
            {
                nZ += 1;

                hit.gameObject.SetActive(false);

                if (i < nZ)
                {
                     Instantiate(Morto, hit.transform.position, hit.transform.rotation);
                     i++;
                }

                Debug.Log(nZ);
            }



            if (hit.CompareTag("Player"))
            {
                //hit.GetComponent<Player>().Vida -= 20;
            }

        }

            Destroy(gameObject);

    }
}
