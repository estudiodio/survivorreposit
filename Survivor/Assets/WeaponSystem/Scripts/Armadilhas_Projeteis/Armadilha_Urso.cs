﻿using UnityEngine;

public class Armadilha_Urso : MonoBehaviour
{

    Animator anim;
    public bool presa;
    GameObject o_Pe;
 //   Rigidbody rb;

    void Start()
    {
        anim = GetComponent<Animator>();
       // rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        //print(other.gameObject);
        if(other.CompareTag("Zombie") && !presa)
        {
            presa = true;

            transform.parent = o_Pe.transform;
            transform.position = new Vector3(o_Pe.transform.position.x, transform.position.y, o_Pe.transform.position.z);
            transform.rotation = Quaternion.identity;

            anim.Play("Armadilha_Fechar", 0, 0f);

            //rb.isKinematic = true;
            GetComponent<BoxCollider>().enabled = false;

        }
    }

    public void Armar()
    {
        anim.Play("Armadilha_Abrir", 0, 0f);
        presa = false;
        transform.parent = null;
        transform.rotation = Quaternion.identity;
        //rb.isKinematic = false;
        GetComponent<BoxCollider>().enabled = true;
    }
}
