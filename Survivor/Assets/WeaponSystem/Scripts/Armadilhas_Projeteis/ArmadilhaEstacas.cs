﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArmadilhaEstacas : MonoBehaviour
{

    int DanoArmadilha = 5;
    int danoRecebido = 5;
    public float fireRate = 2;
    public Mesh construida;
    public Mesh destruida;
    public Mesh incompleta;
    public Collider box;

    float currentRate;
    public float vida;
    public bool _incompleta;

    // Start is called before the first frame update
    void Start()
    {
        vida = 100;
        currentRate = fireRate;

        if (_incompleta) Incompleta();
    }


    private void OnTriggerStay(Collider other)
    {
        if (other.transform.CompareTag("Zombie"))
        {
            currentRate += Time.deltaTime;

            if (currentRate >= fireRate)
            {
                vida -= danoRecebido;
                currentRate = 0;
                if (vida <= 0) Destruida();
            }
        }

        if (other.transform.CompareTag("Player"))
        {
            currentRate += Time.deltaTime;

            if (currentRate >= fireRate)
            {
                other.GetComponent<CG_Vitto>().RecebeDano(DanoArmadilha);
                vida -= danoRecebido;
                currentRate = 0;
                if (vida <= 0) Destruida();
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        currentRate = fireRate;
    }

    public void Incompleta()
    {
        GetComponent<MeshFilter>().mesh = incompleta;
        DanoArmadilha = 0;
        danoRecebido = 0;
        box.enabled = false;
        vida = 100;
    }

    public void Construida()
    {
        GetComponent<MeshFilter>().mesh = construida;
        DanoArmadilha = 15;
        danoRecebido = 5;
        box.enabled = true;
        vida = 100;
    }

    public void Destruida()
    {
        GetComponent<MeshFilter>().mesh = destruida;
        DanoArmadilha = 0;
        danoRecebido = 0;
        box.enabled = false;
        Destroy(gameObject,5);
    }
}
