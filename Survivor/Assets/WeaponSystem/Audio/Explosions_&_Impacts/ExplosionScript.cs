﻿using UnityEngine;
using System.Collections;

public class ExplosionScript : MonoBehaviour {

	[Header("Opiçoes")]
	public float despawnTime = 9.0f;
	public float lightDuration = 0.02f;

	[Header("Luz")]
	public Light lightFlash;

	[Header("Audio")]
	public AudioClip[] explosionSounds;
	public AudioSource audioSource;

    GameObject Jogador;

	private void Start () {

		StartCoroutine (DestroyTimer ());
		StartCoroutine (LightFlash ());

        Jogador = GameObject.FindWithTag("Player");

		audioSource.clip = explosionSounds
			[Random.Range(0, explosionSounds.Length)];

        audioSource.Play();

        audioSource.volume = Jogador.GetComponent<CG_Vitto>().sfxVolume;
    }

	private IEnumerator LightFlash () {

		lightFlash.GetComponent<Light>().enabled = true;

		yield return new WaitForSeconds (lightDuration);

		lightFlash.GetComponent<Light>().enabled = false;
	}

	private IEnumerator DestroyTimer () {
		yield return new WaitForSeconds (despawnTime);
		Destroy (gameObject);
	}
}
